let echarts = require("echarts");
import options from "../../chartOptions/MesLineFQCBaseOptions";
export default {
  data() {
    return {
      bar: "b-" + ~~(Math.random(10000, 100000) * 100000),
      pie: "p-" + ~~(Math.random(10000, 100000) * 100000),
      line: "l-" + ~~(Math.random(10000, 100000) * 100000),
      heigth: document.documentElement.clientHeight - 190,
      options: {}
    };
  },
  methods: {
    searchAfter(data) {
      let dataFormat = data.mesFQCValue
      dataFormat.push(data.yAxisValue)
      let temp = options(data.mesFQCBad.chartBadName, data.mesFQCXmodel.chartX, dataFormat)
      this.options = temp.line
      let $bar = echarts.init(document.getElementById(this.bar));
      $bar.setOption(this.options);

      let $line = echarts.init(document.getElementById(this.line));
      $line.setOption(this.options);
    }
  },
};
