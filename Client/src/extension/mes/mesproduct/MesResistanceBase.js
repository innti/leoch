
import stringUtils from "../../../utilities/stringUtils";
//author:jxx
//此处是对表单的方法，组件，权限操作按钮等进行任意扩展(方法扩展可参照SellOrder.js)
let extension = {
    components: {//动态扩充组件或组件路径
        //表单header、content、footer对应位置扩充的组件
        gridHeader:'',//{ template: "<div>扩展组xx件</div>" },
        gridBody: '',
        gridFooter: '',
        //弹出框(修改、编辑、查看)header、content、footer对应位置扩充的组件
        modelHeader: '',
        modelBody: '',
        modelFooter: ''
    },
    buttons: {view: [], box:[],  detail:[]},//扩展的按钮
    methods: {//事件扩展
       onInit() {

       },
       Report() {
        const begin = stringUtils.format(this.searchFormFields.SDate[0],'yyyy-MM-dd')  
        const end = stringUtils.format(this.searchFormFields.SDate[1],'yyyy-MM-dd')  
        
        if (stringUtils.isNullOrEmpty(end)) {
          return this.$error("请选择日期查询");
        }
        
        if (end.indexOf('NaN')==0) {
          return this.$error("请选择日期查询");
        }


        const Sortbe = this.searchFormFields.Sort
        if (stringUtils.isNullOrEmpty(Sortbe)) {
          return this.$error("请选择查询面别");
        }
        let model = {
          Backfield1: begin + ',' + end,
          Station: this.searchFormFields.Station,
          Line: this.searchFormFields.Line,
          Sort: this.searchFormFields.Sort,
          Type: this.searchFormFields.Type,
        }
  
        this.http.post("api/MesResistanceBase/ExcelReport", model).then(result => {
  
          const url = this.http.ipAddress + result;
  
          const a = document.createElement('a')
          a.href = url
          a.click();
  
        })
       }
    }
};
export default extension;