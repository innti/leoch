// 封装常用字符串方法
export default {

  // 判断字符串是否为空或者为null
  isNullOrEmpty(str) {
    let result = false
    if (str == undefined || str == '' || str == null) {
      result = true
    }
    return result
  },
  nullToString(str) {
    let result = str
    if (str == undefined || str == '' || str == null) {
      result = ''
    }
    return result
  },
  // getDateTime(str) {
  //   let JsonStr = JSON.stringify(str).replace('\"', '')
  //     .replace('[', '')
  //     .replace(']', '')
  //     console.log(JsonStr);
  //   const StrArr = JsonStr.split(',')
  //   let arr = [];
  //   if (StrArr.length > 0) {
  //     StrArr.forEach(e => {
  //       let item = e.split('T')[0].replace('\"', '')
  //       arr.push(item)
  //     });
  //   }
  //   return arr
  // },
  //修改日期格式
  format(time, format) {
    var t = new Date(time);
    var tf = function (i) { return (i < 10 ? '0' : '') + i };
    return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
        switch (a) {
            case 'yyyy':
                return tf(t.getFullYear());
                break;
            case 'MM':
                return tf(t.getMonth() + 1);
                break;
            case 'mm':
                return tf(t.getMinutes());
                break;
            case 'dd':
                return tf(t.getDate());
                break;
            case 'HH':
                return tf(t.getHours());
                break;
            case 'ss':
                return tf(t.getSeconds());
                break;
        }
    })
},
getDateTime(str) {
  let arr = [];
  if (str.length > 1) {
    arr.push(format(str[0],'yyyy-MM-dd')  )
    arr.push(format(str[1],'yyyy-MM-dd')  )
  }
  return arr
}
}
