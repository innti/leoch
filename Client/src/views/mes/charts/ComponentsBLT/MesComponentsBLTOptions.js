function exportData(xAxisData,seriesData) {
  let option = {
    legend: {
      data: ['不良原因', '直通率']
    },
    xAxis: [
      {
        type: 'category',
        data: xAxisData
       
      }
    ], tooltip: {
      trigger: 'axis'
    },
    yAxis: [
      {
        type: 'value',
     
      },
      {
        type: 'value',
        name: '直通率',
        min: 0,
        max: 100,
        interval: 5,
        axisLabel: {
          formatter: '{value}'
        }
       }
    ],
    series: seriesData
  }
  return option;
}
export default exportData;
