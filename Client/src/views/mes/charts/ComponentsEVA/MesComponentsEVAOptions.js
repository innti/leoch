function exportData(xAxisData,seriesData) {
  let option = {
    xAxis: {
      type: 'category',
      boundaryGap: false,
      //data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      data: xAxisData
    },
    tooltip: {
      trigger: 'axis'
    },
    yAxis: {
      type: 'value',
    },
    series:seriesData
  }
  
  return option;
}
export default exportData;
