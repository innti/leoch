let echarts = require("echarts");
import options from "./MesConsumeAgOptions";
export default {
  data() {
    return {
      bar: "b-" + ~~(Math.random(10000, 100000) * 100000),
      pie: "p-" + ~~(Math.random(10000, 100000) * 100000),
      line: "l-" + ~~(Math.random(10000, 100000) * 100000),
      heigth: document.documentElement.clientHeight - 190,
      options: {}
    };
  },
  methods: {
    searchAfter(data) {
      let temp = options( data.mesxAxisData, data.messeriesDatas)

      this.options = temp
      let $bar = echarts.init(document.getElementById(this.bar));
      $bar.setOption(this.options)
    }
  },
};
