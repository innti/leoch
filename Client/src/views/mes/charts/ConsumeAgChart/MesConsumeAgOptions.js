function exportData(xAxisData,seriesData) {
  let option = {
    title: {
      text: '耗银量统计图'
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    legend: {
      data: ['耗银量']
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        boundaryGap: false,
        data: xAxisData,
       // data: ['2021-12-03', '2021-12-04', '2021-12-05', '2021-12-06', '2021-12-07', '2021-12-08', '2021-12-09']
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series:seriesData
    // series: [
    //   {
    //     name: '耗银量',
    //     type: 'line',
    //     stack: 'Total',
    //     emphasis: {
    //       focus: 'series'
    //     },
    //     data: [120, 132, 101, 134, 90, 230, 210]
    //   }
    // ]
  }
  return option;
}
export default exportData;
