let echarts = require("echarts");
import options from "./MesChipOptions";
export default {
  data() {
    return {
      bar: "b-" + ~~(Math.random(10000, 100000) * 100000),
      pie: "p-" + ~~(Math.random(10000, 100000) * 100000),
      line: "l-" + ~~(Math.random(10000, 100000) * 100000),
      heigth: document.documentElement.clientHeight - 190,
      options: {}
    };
  },
  methods: {
    searchAfter(data) {
      let messeries = data.messeriesDatas
      if (messeries.length > 0) {
        messeries.forEach(e => {
          this.$set(e, 'itemStyle', {
            normal: {
              label: {
                show: true,
                position: 'top',	//在上方显示
                textStyle: {	    //数值样式
                  color: 'black',
                  fontSize: 16
                   }
              }
            }
          })
        });
      }
     
      let temp = options(data.mesLegendData,data.mesxAxisData, messeries)

      this.options = temp
      let $bar = echarts.init(document.getElementById(this.bar));
      $bar.setOption(this.options)

    }
  },
};
