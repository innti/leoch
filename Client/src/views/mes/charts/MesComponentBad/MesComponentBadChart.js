let echarts = require("echarts");
import options from "./MesComponentBadOptions";
export default {
  data() {
    return {
      bar: "b-" + ~~(Math.random(10000, 100000) * 100000),
      pie: "p-" + ~~(Math.random(10000, 100000) * 100000),
      line: "l-" + ~~(Math.random(10000, 100000) * 100000),
      heigth: document.documentElement.clientHeight - 190,
      options: {}
    };
  },
  methods: {
    searchAfter(data) {
      let dataFormat = data.mesFQCValue
      dataFormat.push(data.yAxisValue)
      console.log(data.chartX)
      let temp = options(data.chartBadName, data.chartX, dataFormat)
      this.options = temp.line
      let $bar = echarts.init(document.getElementById(this.bar));
      $bar.setOption(this.options);

      let $line = echarts.init(document.getElementById(this.line));
      $line.setOption(this.options);
     
    

    }
  },
};
