function exportData(xAxisData,seriesData) {

  let option = {

      xAxis: {
        type: 'category',
        data: xAxisData,
       // data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      },
      yAxis: {
        type: 'value'
      },
      series: seriesData
      // series: [
      //   {
      //     data: [150, 230, 224, 218, 135, 147, 260],
      //     type: 'line'
      //   }
      // ]
   
  }
  return option;
}
export default exportData;
