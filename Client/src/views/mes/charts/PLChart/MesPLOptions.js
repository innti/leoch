function exportData(xAxisData,seriesData) {
  let option = {
    xAxis: {
      data: xAxisData
     // data: ['2021-10-24', '2021-10-25', '2021-10-26', '2021-10-27']
    },
    yAxis: {},
    series: seriesData
    // series: [{
    //   type: 'candlestick',
    //   data: [
    //     [20, 34, 10, 38],
    //     [40, 35, 30, 50],
    //     [31, 38, 33, 44],
    //     [38, 15, 5, 42]
    //   ]
    // }]
  }
  return option;
}
export default exportData;
