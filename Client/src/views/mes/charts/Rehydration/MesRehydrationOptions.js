function exportData(legendData,xAxisData,seriesData) {
  let option = {
    title: {
      text: '补液工位每小时补液量'
    },
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data:legendData
      //data: ['NH4OH', 'HCL', 'HF', 'H2O2', 'KOH']
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
   
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: xAxisData
      // data: [
      //   '2020/04/30 07:00:00',
      //   '2020/04/30 08:00:00',
      //   '2020/04/30 09:00:00',
      //   '2020/04/30 10:00:00',
      //   '2020/04/30 11:00:00',
      //   '2020/04/30 12:00:00',
      //   '2020/04/30 13:00:00'
      // ]
    },
    yAxis: {
      type: 'value'
    },
    series: seriesData
    // series: [{
    //     name: 'NH4OH',
    //     type: 'line',
    //     stack: 'Total',
    //     data: [120, 132, 101, 134, 90, 230, 210]
    //   },
    //   {
    //     name: 'HCL',
    //     type: 'line',
    //     stack: 'Total',
    //     data: [220, 182, 191, 234, 290, 330, 310]
    //   },
    //   {
    //     name: 'HF',
    //     type: 'line',
    //     stack: 'Total',
    //     data: [150, 232, 201, 154, 190, 330, 410]
    //   },
    //   {
    //     name: 'KOH',
    //     type: 'line',
    //     stack: 'Total',
    //     data: [320, 332, 301, 334, 390, 330, 320]
    //   },
    //   {
    //     name: 'H2O2',
    //     type: 'line',
    //     stack: 'Total',
    //     data: [820, 932, 901, 934, 1290, 1330, 1320]
    //   }
    // ]
  };
  return option;
}
export default exportData;
