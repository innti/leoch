function exportData() {
  let option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    legend: {
      data: ['Forest', 'Steppe', 'Desert', 'Wetland']
    },
    toolbox: {
      show: true,
      orient: 'vertical',
      left: 'right',
      top: 'center',
    },
    xAxis: [
      {
        type: 'category',
        axisTick: { show: false },
        data: ['2012', '2013', '2014', '2015', '2016']
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: 'Forest',
        type: 'bar',
        barGap: 0,
        emphasis: {
          focus: 'series'
        },
        data: [320, 332, 301, 334, 390],
          itemStyle: {
                     normal: {
                         label: {
                             show: true,		//开启显示
                             position: 'top',	//在上方显示
                             textStyle: {	    //数值样式
                                 color: 'black',
                                 fontSize: 16
                             }
                         }
                     }
       }
      },
      {
        name: 'Steppe',
        type: 'bar',
        emphasis: {
          focus: 'series'
        },
        data: [220, 182, 191, 234, 290],
         itemStyle: {
                     normal: {
                         label: {
                             show: true,		//开启显示
                             position: 'top',	//在上方显示
                             textStyle: {	    //数值样式
                                 color: 'black',
                                 fontSize: 16
                             }
                         }
                     }
       }
      },
      {
        name: 'Desert',
        type: 'bar',
        emphasis: {
          focus: 'series'
        },
        data: [150, 232, 201, 154, 190],
          itemStyle: {
                     normal: {
                         label: {
                             show: true,		//开启显示
                             position: 'top',	//在上方显示
                             textStyle: {	    //数值样式
                                 color: 'black',
                                 fontSize: 16
                             }
                         }
                     }
       }
      },
      {
        name: 'Wetland',
        type: 'bar',
        emphasis: {
          focus: 'series'
        },
        data: [98, 77, 101, 99, 40],
          itemStyle: {
                     normal: {
                         label: {
                             show: true,		//开启显示
                             position: 'top',	//在上方显示
                             textStyle: {	    //数值样式
                                 color: 'black',
                                 fontSize: 16
                             }
                         }
                     }
       }
      }
    ]
  }
   return option;
}
export default exportData;
