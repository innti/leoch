function exportData(xAxisData,seriesData) {
  let option = {
  
      //标题
      title:{
        text:''
      },
      //工具箱
      //保存图片
     
      //图例-每一条数据的名字叫销量
      legend:{
          data:['']
      },
      //x轴
      xAxis:{
        data: xAxisData,
      
     
        //  data:["苹果","橘子","橙子","香蕉","菠萝","榴莲"]
      },
      //y轴没有显式设置，根据值自动生成y轴
      yAxis:{},
      //数据-data是最终要显示的数据
      series:seriesData
      // series:[{
      //     name:'销量',
      //     type:'line',
      //     data:[40,20,35,60,55,10]
      // }]
  
  }
  return option;
}
export default exportData;
