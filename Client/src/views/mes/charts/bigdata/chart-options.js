var echarts = require("echarts");
let chartLeft1 = {
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow"
    }
  },
  grid: {
    left: "0%",
    top: "10px",
    right: "0%",
    bottom: "4%",
    containLabel: true
  },
  xAxis: [
    {
      type: "category",
      data: [
        "制绒",
        "CVD",
        "PVD",
        "印刷",
        "黄光",
        "电镀",
        "测试"
      ],
      axisLine: {
        show: true,
        lineStyle: {
          color: "rgba(255,255,255,.1)",
          width: 1,
          type: "solid"
        }
      },

      axisTick: {
        show: false
      },
      axisLabel: {
        interval: 0,
        show: true,
        splitNumber: 15,
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: "12"
        }
      }
    }
  ],
  yAxis: [
    {
      type: "value",
      axisLabel: {
        show: true,
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: "12"
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: "rgba(255,255,255,.1	)",
          width: 1,
          type: "solid"
        }
      },
      splitLine: {
        lineStyle: {
          color: "rgba(255,255,255,.1)"
        }
      }
    }
  ],
  series: [
    {
      type: "bar",
      data: [22000, 21980, 21980, 21970, 21970, 21980, 21960],
      barWidth: "35%",
      itemStyle: {
        normal: {
          color: "#2f89cf",
          opacity: 1,
          barBorderRadius: 5
        }
      }
    }
  ]
};


let chartLeft2 = {
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow"
    }
  },
  grid: {
    left: "0%",
    top: "10px",
    right: "0%",
    bottom: "4%",
    containLabel: true
  },
  xAxis: [
    {
      type: "category",
      data: [
        "周一",
        "周二",
        "周三",
        "周四",
        "周五",
      
      ],
      axisLine: {
        show: true,
        lineStyle: {
          color: "rgba(255,255,255,.1)",
          width: 1,
          type: "solid"
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        interval: 0,
        // rotate:50,
        show: true,
        splitNumber: 15,
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: "12"
        }
      }
    }
  ],
  yAxis: [
    {
      type: "value",
      axisLabel: {
        show: true,
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: "12"
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: "rgba(255,255,255,.1	)",
          width: 1,
          type: "solid"
        }
      },
      splitLine: {
        lineStyle: {
          color: "rgba(255,255,255,.1)"
        }
      }
    },
    {
      type: "value",
      axisLabel: {
        show: true,
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: "12"
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: "rgba(255,255,255,.1	)",
          width: 1,
          type: "solid"
        }
      },
      splitLine: {
        lineStyle: {
          color: "rgba(255,255,255,.1)"
        }
      }
    }
  ], series: [
    {
      type: "bar",
      name: "白班",
      data: [100000, 83000, 85000, 90250,22000],
      barWidth: "25%",
      itemStyle: {
        normal: {
          color: "#2f89cf",
          opacity: 1,
          barBorderRadius: 5
        }
      }
    }, {
      type: "bar",
      name: "夜班",
      data: [98000,100000, 96700, 100000, 0],
      barWidth: "25%",
      itemStyle: {
        normal: {
          color: "#46d000",
          opacity: 1,
          barBorderRadius: 5
        }
      }
    }
  ]
};

let chartLeft3 = {
  tooltip: {
      trigger: 'axis',
      axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
      }
  },
  grid: {
    left: "0%",
    top: "-5px",
    right: "3%",
    bottom: "4%",
    containLabel: true
  },
  xAxis: {
      type: 'value',
    axisLabel: {
      show: true,
      textStyle: {
        color: "rgba(255,255,255,.6)",
        fontSize: "12"
      }
    },
    axisTick: {
      show: false
    },
    axisLine: {
      show: true,
      lineStyle: {
        color: "rgba(255,255,255,.1	)",
        width: 1,
        type: "solid"
      }
    },
    splitLine: {
      lineStyle: {
        color: "rgba(255,255,255,.1)"
      }
    }
  },
  yAxis: {
      type: 'category',
    axisLabel: {
      show: true,
      textStyle: {
        color: "rgba(255,255,255,.6)",
        fontSize: "12"
      }
    },
    axisTick: {
      show: false
    },
    axisLine: {
      show: true,
      lineStyle: {
        color: "rgba(255,255,255,.1	)",
        width: 1,
        type: "solid"
      }
    },
    splitLine: {
      lineStyle: {
        color: "rgba(255,255,255,.1)"
      }
    },
      data: ['周一', '周二', '周三', '周四', '周五']
  },
  series: [
      {
          name: '直接访问',
          type: 'bar',
          stack: '总量',
          label: {
              show: true,
              position: 'insideRight'
          },
            barWidth: "55%",
    itemStyle: {
      normal: {
        color: "#2f89cf",
        opacity: 1,
        barBorderRadius: 5
      }
    },
          data: [198000, 183000, 186700, 190250, 22000]
      }
  ]
};

let chartRight1 = {
  title: {},
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "cross",
      label: {
        backgroundColor: "#6a7985"
      }
    }
  },

  color: ["#ffab6f", "#1E90FF", "#83cddc","#7FFF00",'#FF00FF'], //图例颜色
  legend: {
    top: "0%",
    icon: "roundRect",
    data: ["制绒", "CVD", "PVD","印刷","测试"],
    textStyle: {
      color: "rgba(255,255,255,.5)",
      fontSize: "12"
    }
  },
  toolbox: {
    feature: {}
  },
  grid: {
    left: "10",
    top: "20",
    right: "10",
    bottom: "10",
    containLabel: true
  },
  xAxis: [
    {
      type: "category",
      boundaryGap: false,
      axisLabel: {
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: 12
        }
      },
      axisLine: {
        lineStyle: {
          color: "rgba(255,255,255,.2)"
        }
      },
      data: [
        "2021.01",
        "2021.02",
        "2021.03",
        "2021.04",
        "2021.05",
        "2021.06",
        "2021.07",
        "2021.08",
        "2021.09",
        "2021.10",
      ]
    }
  ],
  yAxis: [
    {
      type: "value",
      axisTick: { show: false },
      min:80,
      max:105,
      minInterval: 60,
      axisLine: {
        lineStyle: {
          color: "rgba(255,255,255,.1)"
        }
      },
      axisLabel: {
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: 12
        }
      },

      splitLine: {
        lineStyle: {
          color: "rgba(255,255,255,.1)"
        }
      }
    }
  ],
  series: [
    {
      name: "制绒",
      type: "line",
      smooth: true,
      data: [98.2, 98.3, 97.3, 98.3, 98.2, 98.8, 97.8, 98.01,99.02,99.2]
    },
    {
      name: "CVD",
      type: "line",
      smooth: true,
      data: [99.2, 98.7, 99.5, 99.13, 98.2, 98.8, 98.8, 99.31,99.05,99.08]
    },
    {
      name: "PVD",
      type: "line",
      smooth: true,
      data: [99.62, 98.17, 99.85, 99.43, 96.22, 98.78, 99.98, 99.671,99,99.08]
    }
    ,
    {
      name: "印刷",
      type: "line",
      smooth: true,
      data: [98.62, 98.77, 99.95, 95.33, 96.42, 98.28, 97.89, 99.31,98,99.67]
    }
    ,
    {
      name: "测试",
      type: "line",
      smooth: true,
      data: [97.2, 96.7, 97.75, 97.23, 98.78, 98.08, 98.91, 98.61,98.18,99.38]
    }
  ]
};

let chartRight2 = {
  title: {},
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "cross",
      label: {
        backgroundColor: "#6a7985"
      }
    }
  },

  color: ["#ffab6f", "#09b916", "#83cddc"], //图例颜色
  legend: {
    top: "0%",
    icon: "roundRect",
    data: ["白班", "夜班"],
    textStyle: {
      color: "rgba(255,255,255,.5)",
      fontSize: "12"
    }
  },
  toolbox: {
    feature: {}
  },
  grid: {
    left: "10",
    top: "20",
    right: "10",
    bottom: "10",
    containLabel: true
  },
  xAxis: [
    {
      type: "category",
      boundaryGap: false,
      axisLabel: {
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: 12
        }
      },
      axisLine: {
        lineStyle: {
          color: "rgba(255,255,255,.2)"
        }
      },
      data: [
        "2021.01",
        "2021.02",
        "2021.03",
        "2021.04",
        "2021.05",
        "2021.06",
        "2021.07",
        "2021.08",
        "2021.09",
        "2021.10",
      ]
    }
  ],
  yAxis: [
    {
      type: "value",
      axisTick: { show: false },
      minInterval: 60,
      axisLine: {
        lineStyle: {
          color: "rgba(255,255,255,.1)"
        }
      },
      axisLabel: {
        textStyle: {
          color: "rgba(255,255,255,.6)",
          fontSize: 12
        }
      },

      splitLine: {
        lineStyle: {
          color: "rgba(255,255,255,.1)"
        }
      }
    }
  ],
  series: [
    {
      name: "白班",
      type: "line",
      smooth: true,
      lineStyle: {
        color: "#45d4ba",
        width: 1
      }, //线条的样式
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: "#83cddc"
          },
          {
            offset: 1,
            color: "#bfdffbb5"
          }
        ])
      },
      data: [98, 98, 96, 93, 92, 90, 91, 92,89,99]
    },
    {
      name: "夜班",
      type: "line",
      smooth: true,
      lineStyle: {
        color: "#04a710",
        width: 1
      }, //
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: "#0cbf22"
          },
          {
            offset: 1,
            color: "#b8f7d1b5"
          }
        ])
      },
      data: [99, 88, 95, 101, 98, 96, 90, 70,90,99]
    }
  ]
};
export { chartLeft1, chartLeft2,chartLeft3, chartRight1, chartRight2}
