let options= {
    line: {
   color: ["#3398DB"],
    title: {
        text: '',
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['最高气温']
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['1', '2', '3', '4', '5', '6', '7']
    },
    yAxis: {
        type: 'value',
        max:650,
        min:380
    },
    series: [
        {
            name: '最高气温',
            type: 'line',
            data: [510, 511, 513, 521, 527, 512, 569],
            markLine: {
                data: [ { yAxis: 530, name: '最大值',color: ["#DC143C"],} , { yAxis: 515, name: '最大值'},{type: 'average', name: '平均值'} ]
            }
        }
    ]
}
};
 export default options;