let options= {
    line: {
   color: ['#37A2DA', '#91c7ae', '#67E0E3', '#9FE6B8', '#FFDB5C','#ff9f7f', '#fb7293', '#E062AE', '#E690D1', '#e7bcf3', '#9d96f5', '#8378EA', '#96BFFF'],
   tooltip: {
    trigger: 'axis',
    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
    }
},
legend: {
    data: ['破片', '断栅', '脏污', '色差', '色斑', '缺口', '脱线', '栅线偏移', '栅线异色']
},
grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
},
xAxis: [
    {
        type: 'category',
        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
    }
],
yAxis: [
    { 
        name: '不良率',
        type: 'value'
    },
    {
        type: 'value',
        name: '直通率',
        min: 0,
        max: 100,
        interval: 5,
        axisLabel: {
            formatter: '{value}'
        }
    }
],
series: [
    {
        name: '破片',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
        data: [0.3, 0.2, 0.05, 0.23, 0.21, 0.12, 0.4]
    },
    {
        name: '断栅',
        type: 'bar',
      stack: '不良',
        emphasis: {
            focus: 'series'
        },
        data: [0.1, 0, 0.05, 0.0, 0, 0, 0]
    },
    {
        name: '脏污',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
        data: [0.01, 0.03, 0.05, 0.06, 0.15, 0.34, 0.12]
    },
    {
        name: '色差',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
       data: [0.00, 0.03, 0.15, 0.26, 0.25, 0.14, 0.02]
    },
    {
        name: '色斑',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
        data: [0.01, 0.02,  0.015, 0.06, 0.15, 0.04, 0.12]
    },
    {
        name: '缺口',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
        data: [0.01, 0.02,  0.25, 0.26, 0.25, 0.14, 0.02]
    },
    {
        name: '脱线',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
        data: [0, 0,  0,0, 0.04, 0.07, 0.05]
    },
    {
        name: '栅线偏移',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
        data: [0,0,0.02,0.08,0,0,0]
    },
    {
        name: '栅线异色',
        type: 'bar',
        stack: '不良',
        emphasis: {
            focus: 'series'
        },
      data: [0.01,0.03,0.02,0,0,0,0]
    }

    ,{
        name: '直通率',
        type: 'line',
        yAxisIndex: 1,
        color: ["#3398DB"],
        data: [98.0, 96.2, 96.3, 95.5, 95.3, 95.2, 97.3]
    }
]
}
};
 export default options;