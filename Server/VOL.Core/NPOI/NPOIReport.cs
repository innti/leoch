﻿using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using NPOI.SS.UserModel;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using VOL.Core.Extensions;

namespace VOL.Core.NPOI
{
    public class NPOIReport
    {
        public string GetReportFile(string fileName)
        {
            string dicPath = $"Upload/Template/".MapPath();

            var exportTemplatePath = dicPath + fileName;

            var newName = $"{ Path.GetFileNameWithoutExtension(fileName)}-{System.DateTime.Now.ToString("yyyyMMddhhmmfff")}.xlsx";
            string dicPath1 = $"Upload/Excel/".MapPath();
            var exprotPath = dicPath1 + newName;

            if (System.IO.File.Exists(exprotPath))
            {
                System.IO.File.Delete(exprotPath);
            }
            System.IO.File.Copy(exportTemplatePath, exprotPath);
            return exprotPath;
        }

        public void GetReport<T>(string fileName, List<string> GroupList, List<T> list, Func<T, bool> filter, string propertyName, int x1, int x2, int y1, int y2)
        {
            var directory = System.IO.Directory.GetCurrentDirectory();

            var exportTemplatePath = directory + "/wwwroot/Template/" + fileName;
            var newName = $"{fileName}-{System.DateTime.Now.ToString("yyyyMMdd")}.xlsx";
            var exprotPath = directory + "/wwwroot/Upload/Excel/" + newName;

            if (System.IO.File.Exists(exprotPath))
            {
                System.IO.File.Delete(exprotPath);
            }
            System.IO.File.Copy(exportTemplatePath, exprotPath);
            FileStream file = new FileStream(exprotPath, FileMode.Open, FileAccess.Read);
            var workbook = new XSSFWorkbook(file);
            var sheet = workbook.GetSheetAt(0);

            for (int i = 0; i < GroupList.Count; i++)
            {
                ICell cell = sheet.GetRow(x1).GetCell(y1 + i);
                cell.SetCellValue(GroupList[i].ToString());

                var listData = GetPropertyValue(list, filter, propertyName);
                listData.Sort();
                for (int j = 0; j < listData.Count; j++)
                {
                    ICell cell1 = sheet.GetRow(x2 + j).GetCell(y2 + i);
                    var num = Convert.ToDouble(listData[j]);
                    cell1.SetCellValue(num);
                }
            }

            using (var stream = new FileStream(exprotPath, FileMode.Create, FileAccess.Write))
            {
                workbook.Write(stream);
            }
        }


        /// <summary>
        /// 根据属性名称获取属性值
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="propertyName">属性名称</param>
        /// <returns></returns>
        private List<T> GetPropertyValue<T>(List<T> list, Func<T, bool> filter)
        {

            List<T> listData = new List<T>();
            try
            {
                foreach (T value in list)
                {
                    if (filter(value))
                    {
                        //通过传入进来的List<T> list进行linq或者lambda筛选
                        //求大神来解决  泛型List<T>不是已知的集合
                        listData.Add(value);
                    }
                }

            }
            catch (Exception)
            {

            }

            return listData;
        }

        /// <summary>
        /// 根据属性名称获取属性值
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="propertyName">属性名称</param>
        /// <returns></returns>
        private List<object> GetPropertyValue<T>(List<T> list, Func<T, bool> filter, string propertyName)
        {

            List<object> listData = new List<object>();
            try
            {
                foreach (T value in list)
                {
                    if (filter(value))
                    {
                        //通过传入进来的List<T> list进行linq或者lambda筛选
                        //求大神来解决  泛型List<T>不是已知的集合

                        Type entityType = typeof(T);
                        try
                        {
                            PropertyInfo proInfo = entityType.GetProperty(propertyName);
                            var result = proInfo.GetValue(value);
                            listData.Add(result);
                        }
                        catch (Exception)
                        {

                        }


                    }
                }

            }
            catch (Exception)
            {

            }

            return listData;
        }


        /// <summary>
        /// 根据List填充Excel
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="startCell"></param>
        /// <param name="startRow"></param>
        public bool SetListDatas<T>(List<T> list, int startCell, int startRow, string path, int sheetAt = 0)
        {
            try
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                var workbook = new XSSFWorkbook(file);
                var sheet = workbook.GetSheetAt(sheetAt);
                var propertyInfo = typeof(T).GetProperties();
                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = 0; j < propertyInfo.Count(); j++)
                    {

                        object cellValue = propertyInfo[j].GetValue(list[i]);
                        if (cellValue != null)
                        {
                            var row = sheet.GetRow(startRow + i);
                            if (row == null)
                                row = sheet.CreateRow(startRow + i);
                            var cell = row.GetCell(startCell + j);
                            if (cell == null)
                                cell = row.CreateCell(startCell + j);

                            cell.SetCellValue(cellValue.ToString());
                        }
                    }
                }
                using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    workbook.Write(stream);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }


        /// <summary>
        /// 根据List填充Excel并创建文件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="startCell"></param>
        /// <param name="startRow"></param>
        public string SetListDatasAndCreateFile<T>(List<T> list, int startCell, int startRow, string fileName, int sheetAt = 0)
        {
            fileName = fileName.Contains(".xlsx") ? fileName : fileName += ".xlsx";
            var path = GetReportFile(fileName);
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            var workbook = new XSSFWorkbook(file);
            var sheet = workbook.GetSheetAt(sheetAt);
            var propertyInfo = typeof(T).GetProperties();
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < propertyInfo.Count(); j++)
                {

                    object cellValue = propertyInfo[j].GetValue(list[i]);
                    if (cellValue != null)
                    {
                        var row = sheet.GetRow(startRow + i);
                        if (row == null)
                            row = sheet.CreateRow(startRow + i);
                        var cell = row.GetCell(startCell + j);
                        if (cell == null)
                            row.CreateCell(startCell + j);

                        cell.SetCellValue(cellValue.ToString());
                    }
                }
            }
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                workbook.Write(stream);
            }
            return path;
        }

        public string ReturnNewName(string str)
        {
            string returnStr = "";
            switch (str)
            {
                case "制绒清洗插片":
                    returnStr = "制绒清洗";
                    break;
                case "PECVD制造下料":
                    returnStr = "CVD制造";
                    break;
                case "PVD下料":
                    returnStr = "PVD制造";
                    break;
                case "印刷下料":
                    returnStr = "印刷制造";
                    break;
                case "测试扫描":
                    returnStr = "测试制造";
                    break;
                case "铜栅去边":
                    returnStr = "铜栅去边";
                    break;
                case "铜栅黄光":
                    returnStr = "铜栅黄光";
                    break;
                case "铜栅电镀":
                    returnStr = "铜栅电镀";
                    break;
                default:
                    returnStr = str;
                    break;
            }
            return returnStr;
        }

        public static List<string> ReturnListCompStation()
        {
            List<string> list = new List<string>
            {
                "组框",
                "焊接",
                "叠层",
                "EL1测试",
                "叠层返修",
                "层压",
                "外观检",
                "IV测试",
                "绝缘耐压",
                "EL2测试",
                "外观检复核",
                "FQC",
                "FQC复核",
                "包装",
                "OQC",
            };
            return list;

        }
        public static List<string> ReturnListCompTime()
        {
            List<string> list = new List<string>
            {
            "00:00",
            "02:00",
            "04:00",
            "06:00",
            "08:00",
            "10:00",
            "12:00",
            "14:00",
            "16:00",
            "18:00",
            "20:00",
            "22:00",
            };
            return list;

        }
        public static List<string> ReturnListPLbad()
        {
            List<string> list = new List<string>
            {
                "PLA",
                "顶杆印",
                "低亮度",
                "刮伤",
                "吸盘印",
                "皮带",
                "黑斑",
                "饶镀",
                "扇形",
                "手指印",
                "大面积麻点",
                "同心圆",
                "实心卡尺印",
                "空心卡尺印",
                "黑片",
                "暗区",
                "底杆印",
                "卡片",
            };
            return list;
        }
        public static List<string> ReturnListPLbadnum()
        {
            List<string> list = new List<string>
            {
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
            };
            return list;
        }
        public static List<string> ReturnListWeek()
        {
            DateTime currentTime = DateTime.Now;
            int week = Convert.ToInt32(currentTime.DayOfWeek);
            week = week == 0 ? 7 : week;

            //获取本周星期一/星期天
            var start_time_current_week = currentTime.AddDays(1 - week);//本周星期一
            var end_time_current_week = currentTime.AddDays(7 - week);//本周星期天
            List<string> list = new List<string>();
            for (int i = 0; i <7; i++)
            {
                var strdate = start_time_current_week.AddDays(i).ToString("yyyy-MM-dd");
                list.Add(strdate);
            }

            return list;
        }
        public static List<string> ReturnListMonth()
        {
            DateTime currentTime = DateTime.Now;
            List<string> list = new List<string>();
            for (int i = -11; i < 1; i++)
            {
                var strdate = currentTime.AddMonths(i).ToString("yyyy-MM");
                list.Add(strdate);
            }
            return list;
        }

    }

}
