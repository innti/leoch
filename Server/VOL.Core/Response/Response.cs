﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOL.Core.Response
{
    public class Response
    {
        /// <summary>
        /// 操作消息【当Status不为 200时，显示详细的错误信息】
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 操作状态码，200为正常
        /// </summary>
        public int code { get; set; }
        public bool status { get; set; }

        public Response()
        {
            code = 200;
            status = true;
            message = "操作成功";
        }
    }


    /// <summary>
    /// WEBAPI通用返回泛型基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Response<T> : Response
    {
        /// <summary>
        /// 回传的结果
        /// </summary>
        public T Result { get; set; }
    }
}
