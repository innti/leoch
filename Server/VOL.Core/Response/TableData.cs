﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOL.Core.Response
{
    /// <summary>
    /// table的返回数据
    /// </summary>
    public class TableData
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int code;
        /// <summary>
        /// 操作消息
        /// </summary>
        public string message;

        /// <summary>
        /// 总记录条数
        /// </summary>
        public int count;
        public bool status;

        /// <summary>
        /// 数据内容
        /// </summary>
        public dynamic data;
        string[] empty = { };
        public TableData()
        {
            code = 200;
            status = true;
            message = "加载成功";
            data = empty;
        }
    }
}