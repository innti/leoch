/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "Bin效率",TableName = "MesBinbase")]
    public class MesBinbase:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///配方名称
       /// </summary>
       [Display(Name ="配方名称")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string Binrecipe { get; set; }

       /// <summary>
       ///Bin盒
       /// </summary>
       [Display(Name ="Bin盒")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinBox { get; set; }

       /// <summary>
       ///类型
       /// </summary>
       [Display(Name ="类型")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string Bintype { get; set; }

       /// <summary>
       ///规格
       /// </summary>
       [Display(Name ="规格")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinSpec { get; set; }

       /// <summary>
       ///Class
       /// </summary>
       [Display(Name ="Class")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string BinClass { get; set; }

       /// <summary>
       ///Pmax
       /// </summary>
       [Display(Name ="Pmax")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       [Editable(true)]
       public string BinPmax { get; set; }

       /// <summary>
       ///等级
       /// </summary>
       [Display(Name ="等级")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinGrade { get; set; }

       /// <summary>
       ///车间
       /// </summary>
       [Display(Name ="车间")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinRoom { get; set; }

       /// <summary>
       ///材质
       /// </summary>
       [Display(Name ="材质")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinTexture { get; set; }

       /// <summary>
       ///图形
       /// </summary>
       [Display(Name ="图形")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinGraph { get; set; }

       /// <summary>
       ///仓位
       /// </summary>
       [Display(Name ="仓位")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinPosition { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinLine { get; set; }

       /// <summary>
       ///效率
       /// </summary>
       [Display(Name ="效率")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinEff { get; set; }

       /// <summary>
       ///厚度
       /// </summary>
       [Display(Name ="厚度")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinThickness { get; set; }

       /// <summary>
       ///Eta
       /// </summary>
       [Display(Name ="Eta")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinEta { get; set; }

       /// <summary>
       ///Eta最小值
       /// </summary>
       [Display(Name ="Eta最小值")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinEtaMin { get; set; }

       /// <summary>
       ///Eta最大值
       /// </summary>
       [Display(Name ="Eta最大值")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinEtaMax { get; set; }

       /// <summary>
       ///Isc
       /// </summary>
       [Display(Name ="Isc")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinIsc { get; set; }

       /// <summary>
       ///Isc最小值
       /// </summary>
       [Display(Name ="Isc最小值")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinIscMin { get; set; }

       /// <summary>
       ///Isc最大值
       /// </summary>
       [Display(Name ="Isc最大值")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinIscMax { get; set; }

       /// <summary>
       ///Voc
       /// </summary>
       [Display(Name ="Voc")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinVoc { get; set; }

       /// <summary>
       ///Voc最小值
       /// </summary>
       [Display(Name ="Voc最小值")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinVocMin { get; set; }

       /// <summary>
       ///Voc最大值
       /// </summary>
       [Display(Name ="Voc最大值")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       [Editable(true)]
       public string BinVocMax { get; set; }

       /// <summary>
       ///删除标记
       /// </summary>
       [Display(Name ="删除标记")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       [Required(AllowEmptyStrings=false)]
       public DateTime CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人Id
       /// </summary>
       [Display(Name ="创建人Id")]
       [Column(TypeName="int")]
       public int CreateUserId { get; set; }

       /// <summary>
       ///修改时间
       /// </summary>
       [Display(Name ="修改时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///修改人
       /// </summary>
       [Display(Name ="修改人")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///修改人Id
       /// </summary>
       [Display(Name ="修改人Id")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}