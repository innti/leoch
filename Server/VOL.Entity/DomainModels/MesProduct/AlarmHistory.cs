/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "PVD报警",TableName = "AlarmHistory")]
    public class AlarmHistory:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Display(Name ="CODE")]
       [MaxLength(10)]
       [Column(TypeName="varchar(10)")]
       [Required(AllowEmptyStrings=false)]
       public string CODE { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="InTime")]
       [Column(TypeName="datetime")]
       [Required(AllowEmptyStrings=false)]
       public DateTime InTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Description")]
       [MaxLength(16)]
       [Column(TypeName="ntext(16)")]
       [Required(AllowEmptyStrings=false)]
       public string Description { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Solution")]
       [MaxLength(16)]
       [Column(TypeName="ntext(16)")]
       [Required(AllowEmptyStrings=false)]
       public string Solution { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="OutTime")]
       [Column(TypeName="datetime")]
       [Required(AllowEmptyStrings=false)]
       public DateTime OutTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Line")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Line { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       
    }
}