/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "清洗报警",TableName = "MesAlarm")]
    public class MesAlarm:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Display(Name ="ModuleNo")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ModuleNo { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ALTX_Eng")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string ALTX_Eng { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ALTX_Jpn")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string ALTX_Jpn { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Disp")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Disp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="PatDisp")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string PatDisp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Color")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Color { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Bz")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Bz { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CarrStop")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CarrStop { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="InputStop")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string InputStop { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LDStop")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LDStop { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="DetailNo")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DetailNo { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Opt_01")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Opt_01 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Opt_02")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Opt_02 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Opt_03")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Opt_03 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Opt_04")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Opt_04 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Opt_05")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Opt_05 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="AdsLocal")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string AdsLocal { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="AdsGlobal")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string AdsGlobal { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BathNo")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string BathNo { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="AreaNo")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string AreaNo { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ALCD")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ALCD { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ALID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ALID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="OccurTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string OccurTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ResetTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ResetTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="id")]
       [MaxLength(100)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Line")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Line { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="StrPath")]
       [Column(TypeName="nvarchar(max)")]
       public string StrPath { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="WjName")]
       [Column(TypeName="nvarchar(max)")]
       public string WjName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="isdel")]
       [Column(TypeName="int")]
       public int? isdel { get; set; }

       
    }
}