/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "不良查询",TableName = "MesBadDataBase")]
    public class MesBadDataBase:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///编码
       /// </summary>
       [Display(Name ="编码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string billno { get; set; }

       /// <summary>
       ///花篮编号
       /// </summary>
       [Display(Name ="花篮编号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string HLnum { get; set; }

       /// <summary>
       ///批次
       /// </summary>
       [Display(Name ="批次")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string lot { get; set; }

       /// <summary>
       ///不良分类
       /// </summary>
       [Display(Name ="不良分类")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string BadSort { get; set; }

       /// <summary>
       ///不良原因
       /// </summary>
       [Display(Name ="不良原因")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string BadCase { get; set; }

       /// <summary>
       ///数量
       /// </summary>
       [Display(Name ="数量")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Qty { get; set; }

       /// <summary>
       ///工序
       /// </summary>
       [Display(Name ="工序")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Station { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Line { get; set; }

       /// <summary>
       ///工艺
       /// </summary>
       [Display(Name ="工艺")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Technology { get; set; }

       /// <summary>
       ///操作人
       /// </summary>
       [Display(Name ="操作人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserId")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EditName")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string EditName { get; set; }

       /// <summary>
       ///备注
       /// </summary>
       [Display(Name ="备注")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Remark { get; set; }

       /// <summary>
       ///操作时间
       /// </summary>
       [Display(Name ="操作时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       
    }
}