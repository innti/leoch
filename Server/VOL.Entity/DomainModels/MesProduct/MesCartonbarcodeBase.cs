/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "芯片内箱查询",TableName = "MesCartonbarcodeBase")]
    public class MesCartonbarcodeBase:BaseEntity
    {
        /// <summary>
       ///主键
       /// </summary>
       [Key]
       [Display(Name ="主键")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LineName { get; set; }

       /// <summary>
       ///内包装条码
       /// </summary>
       [Display(Name ="内包装条码")]
       [MaxLength(25)]
       [Column(TypeName="varchar(25)")]
       public string CodeBM { get; set; }

       /// <summary>
       ///外箱条码
       /// </summary>
       [Display(Name ="外箱条码")]
       [MaxLength(25)]
       [Column(TypeName="varchar(25)")]
       public string CodeType { get; set; }

       /// <summary>
       ///型号
       /// </summary>
       [Display(Name ="型号")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeSpec { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CodeCode")]
       [Column(TypeName="int")]
       public int? CodeCode { get; set; }

       /// <summary>
       ///Class
       /// </summary>
       [Display(Name ="Class")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeClass { get; set; }

       /// <summary>
       ///PMax
       /// </summary>
       [Display(Name ="PMax")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodePmax { get; set; }

       /// <summary>
       ///数量
       /// </summary>
       [Display(Name ="数量")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeQty { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CodeNMB")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeNMB { get; set; }

       /// <summary>
       ///时间
       /// </summary>
       [Display(Name ="时间")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeDate { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CodeGW")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeGW { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CodeRoom")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeRoom { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Codetime")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string Codetime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CodePosition")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodePosition { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CodeTMSpec")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeTMSpec { get; set; }

       /// <summary>
       ///扫描状态
       /// </summary>
       [Display(Name ="扫描状态")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string CodeNum { get; set; }

       /// <summary>
       ///完成状态
       /// </summary>
       [Display(Name ="完成状态")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人ID
       /// </summary>
       [Display(Name ="创建人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///更新时间
       /// </summary>
       [Display(Name ="更新时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///更新人
       /// </summary>
       [Display(Name ="更新人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///更新人ID
       /// </summary>
       [Display(Name ="更新人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///备用字段1
       /// </summary>
       [Display(Name ="备用字段1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///备用字段2
       /// </summary>
       [Display(Name ="备用字段2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///备用字段3
       /// </summary>
       [Display(Name ="备用字段3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///备用字段4
       /// </summary>
       [Display(Name ="备用字段4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///备用字段5
       /// </summary>
       [Display(Name ="备用字段5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}