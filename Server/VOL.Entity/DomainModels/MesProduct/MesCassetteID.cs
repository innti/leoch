/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "清洗采集",TableName = "MesCassetteID")]
    public class MesCassetteID:BaseEntity
    {
        /// <summary>
       ///主键
       /// </summary>
       [Key]
       [Display(Name ="主键")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MemoryID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MemoryID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CassetteID_01")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CassetteID_01 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CassetteID_02")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CassetteID_02 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessStartTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessStartTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessEndTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessEndTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="PPID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string PPID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="WaferCount_01")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string WaferCount_01 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="WaferCount_02")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string WaferCount_02 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LotID_01")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LotID_01 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LotID_02")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LotID_02 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="PPID_Act")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string PPID_Act { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProductID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProductID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="StepID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string StepID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="PPIDChangeTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string PPIDChangeTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CarrierType")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CarrierType { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CarrierPos")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CarrierPos { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessStep")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessStep { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="TactTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string TactTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="StartInterval")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string StartInterval { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CancelFlag")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CancelFlag { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="RecipeNo")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string RecipeNo { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MoveINWeight_F")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MoveINWeight_F { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MoveINWeight_R")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MoveINWeight_R { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MoveOutWeight_F")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MoveOutWeight_F { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ContentGUID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ContentGUID { get; set; }

       /// <summary>
       ///工位
       /// </summary>
       [Display(Name ="工位")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Station { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Line { get; set; }

       /// <summary>
       ///工艺
       /// </summary>
       [Display(Name ="工艺")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Technology { get; set; }

       /// <summary>
       ///是否删除
       /// </summary>
       [Display(Name ="是否删除")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人ID
       /// </summary>
       [Display(Name ="创建人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///更新时间
       /// </summary>
       [Display(Name ="更新时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///更新人
       /// </summary>
       [Display(Name ="更新人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///更新人ID
       /// </summary>
       [Display(Name ="更新人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///备用字段1
       /// </summary>
       [Display(Name ="备用字段1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///备用字段2
       /// </summary>
       [Display(Name ="备用字段2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///备用字段3
       /// </summary>
       [Display(Name ="备用字段3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///备用字段4
       /// </summary>
       [Display(Name ="备用字段4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///备用字段5
       /// </summary>
       [Display(Name ="备用字段5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}