/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "清洗采集明细",TableName = "MesCassetteIDContent")]
    public class MesCassetteIDContent:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BathName")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string BathName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessTimeAct")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessTimeAct { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LifeTime")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LifeTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LifeBatch")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LifeBatch { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessTemp")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessTemp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="FlowRate")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string FlowRate { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ContentGUID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ContentGUID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Station")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Station { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Line")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Line { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Technology")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Technology { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int IsDel { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateTime")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserName")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserId")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateTime")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserName")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserId")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}