/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "补液信息",TableName = "MesChemicalBase")]
    public class MesChemicalBase:BaseEntity
    {
        /// <summary>
       ///时间
       /// </summary>
       [Display(Name ="时间")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Date { get; set; }

       /// <summary>
       ///机器号
       /// </summary>
       [Display(Name ="机器号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string BathName { get; set; }

       /// <summary>
       ///药品名称
       /// </summary>
       [Display(Name ="药品名称")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Chemical { get; set; }

       /// <summary>
       ///备注
       /// </summary>
       [Display(Name ="备注")]
       [Column(TypeName="nvarchar(max)")]
       public string Action { get; set; }

       /// <summary>
       ///单位
       /// </summary>
       [Display(Name ="单位")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Unit { get; set; }

       /// <summary>
       ///用量
       /// </summary>
       [Display(Name ="用量")]
       [MaxLength(20)]
       [Column(TypeName="varchar(20)")]
       public string Value { get; set; }

       /// <summary>
       ///主键
       /// </summary>
       [Key]
       [Display(Name ="主键")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LineName { get; set; }

       /// <summary>
       ///路径
       /// </summary>
       [Display(Name ="路径")]
       [Column(TypeName="nvarchar(max)")]
       public string StrPath { get; set; }

       /// <summary>
       ///文件名称
       /// </summary>
       [Display(Name ="文件名称")]
       [Column(TypeName="nvarchar(max)")]
       public string WjName { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人ID
       /// </summary>
       [Display(Name ="创建人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///是否删除
       /// </summary>
       [Display(Name ="是否删除")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///更新时间
       /// </summary>
       [Display(Name ="更新时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///更新人
       /// </summary>
       [Display(Name ="更新人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///更新人ID
       /// </summary>
       [Display(Name ="更新人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///备用字段1
       /// </summary>
       [Display(Name ="备用字段1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///备用字段2
       /// </summary>
       [Display(Name ="备用字段2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///备用字段3
       /// </summary>
       [Display(Name ="备用字段3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///备用字段4
       /// </summary>
       [Display(Name ="备用字段4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///备用字段5
       /// </summary>
       [Display(Name ="备用字段5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}