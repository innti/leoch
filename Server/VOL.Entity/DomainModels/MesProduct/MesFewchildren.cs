/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "少子寿命",TableName = "MesFewchildren")]
    public class MesFewchildren:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Display(Name ="ActionID")]
       [Column(TypeName="float")]
       public float? ActionID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Batch_ID")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Batch_ID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Sample_ID")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Sample_ID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Sample_Type")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Sample_Type { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Optical_Constant")]
       [Column(TypeName="float")]
       public float? Optical_Constant { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Minority_Carrier_Trap_Correction")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Minority_Carrier_Trap_Correction { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Analysis_Mode")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Analysis_Mode { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Instrument_Type")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Instrument_Type { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Serial_Number")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Serial_Number { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="A")]
       [Column(TypeName="float")]
       public float? A { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="B")]
       [Column(TypeName="float")]
       public float? B { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Offset")]
       [Column(TypeName="float")]
       public float? Offset { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Auger_Coefficient")]
       [Column(TypeName="float")]
       public float? Auger_Coefficient { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Illuminated_Area_Fraction")]
       [Column(TypeName="float")]
       public float? Illuminated_Area_Fraction { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Fit_Range")]
       [Column(TypeName="float")]
       public float? Fit_Range { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Auger_Model")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Auger_Model { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Transient_Mode_Taus")]
       [Column(TypeName="float")]
       public float? Transient_Mode_Taus { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Comments")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Comments { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="id")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="PathName")]
       [Column(TypeName="nvarchar(max)")]
       public string PathName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="QSSDAQtimeus")]
       [Column(TypeName="float")]
       public float? QSSDAQtimeus { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Lifetime_us")]
       [Column(TypeName="float")]
       public float? Lifetime_us { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="TotalSheetResistanceohmsq")]
       [Column(TypeName="float")]
       public float? TotalSheetResistanceohmsq { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MeasuredResistivityohmcm")]
       [Column(TypeName="float")]
       public float? MeasuredResistivityohmcm { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="JoAcm2")]
       [Column(TypeName="float")]
       public float? JoAcm2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EstBulkLifetimeus")]
       [Column(TypeName="float")]
       public float? EstBulkLifetimeus { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LifetimeimpliedVmpus")]
       [Column(TypeName="float")]
       public float? LifetimeimpliedVmpus { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="AnalysisMCDcm3")]
       [Column(TypeName="float")]
       public float? AnalysisMCDcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Min_MCDcm3")]
       [Column(TypeName="float")]
       public float? Min_MCDcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MaxMCDcm3")]
       [Column(TypeName="float")]
       public float? MaxMCDcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BiasPointCDcm3")]
       [Column(TypeName="float")]
       public float? BiasPointCDcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="TrapDensitycm3")]
       [Column(TypeName="float")]
       public float? TrapDensitycm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BiasLightsuns")]
       [Column(TypeName="float")]
       public float? BiasLightsuns { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Dopingcm3")]
       [Column(TypeName="float")]
       public float? Dopingcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="sunImpliedVocV")]
       [Column(TypeName="float")]
       public float? sunImpliedVocV { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="TransientDAQtimeus")]
       [Column(TypeName="float")]
       public float? TransientDAQtimeus { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ImpliedFF")]
       [Column(TypeName="float")]
       public float? ImpliedFF { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MaxIntensitysuns")]
       [Column(TypeName="float")]
       public float? MaxIntensitysuns { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ImpliedVmpV")]
       [Column(TypeName="float")]
       public float? ImpliedVmpV { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ImpliedImpAcm2")]
       [Column(TypeName="float")]
       public float? ImpliedImpAcm2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CDatVmpcm3")]
       [Column(TypeName="float")]
       public float? CDatVmpcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Jo2Acm2")]
       [Column(TypeName="float")]
       public float? Jo2Acm2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BRR_Hz")]
       [Column(TypeName="float")]
       public float? BRR_Hz { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Thickness_cm")]
       [Column(TypeName="float")]
       public float? Thickness_cm { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Base_Resistivityohmcm")]
       [Column(TypeName="float")]
       public float? Base_Resistivityohmcm { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LifetimeSpecMCDcm3")]
       [Column(TypeName="float")]
       public float? LifetimeSpecMCDcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="JoSpecMCDcm3")]
       [Column(TypeName="float")]
       public float? JoSpecMCDcm3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="RefCellVsun")]
       [Column(TypeName="float")]
       public float? RefCellVsun { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BandgapNarrowingAnalysis")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string BandgapNarrowingAnalysis { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="QSSFlashWaits")]
       [Column(TypeName="float")]
       public float? QSSFlashWaits { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="TransientFlashWaits")]
       [Column(TypeName="float")]
       public float? TransientFlashWaits { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="DarkVoltageV")]
       [Column(TypeName="float")]
       public float? DarkVoltageV { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LastMeasurementTimestamp")]
       [Column(TypeName="datetime")]
       public DateTime? LastMeasurementTimestamp { get; set; }

       
    }
}