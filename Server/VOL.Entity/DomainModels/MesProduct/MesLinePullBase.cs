/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "印刷拉力",TableName = "MesLinePullBase")]
    public class MesLinePullBase:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///编码
       /// </summary>
       [Display(Name ="编码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Billno { get; set; }

       /// <summary>
       ///日期
       /// </summary>
       [Display(Name ="日期")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string SDate { get; set; }

       /// <summary>
       ///备注
       /// </summary>
       [Display(Name ="备注")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Remark { get; set; }

       /// <summary>
       ///类型
       /// </summary>
       [Display(Name ="类型")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Sort { get; set; }

       /// <summary>
       ///面别
       /// </summary>
       [Display(Name ="面别")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string AorB { get; set; }

       /// <summary>
       ///值
       /// </summary>
       [Display(Name ="值")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string SValue { get; set; }

       /// <summary>
       ///上规格
       /// </summary>
       [Display(Name ="上规格")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string SUp { get; set; }

       /// <summary>
       ///下规格
       /// </summary>
       [Display(Name ="下规格")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Sdown { get; set; }

       /// <summary>
       ///工位
       /// </summary>
       [Display(Name ="工位")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Station { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Line { get; set; }

       /// <summary>
       ///工艺
       /// </summary>
       [Display(Name ="工艺")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Technology { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int IsDel { get; set; }

       /// <summary>
       ///操作时间
       /// </summary>
       [Display(Name ="操作时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///操作人
       /// </summary>
       [Display(Name ="操作人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserId")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateTime")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserName")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserId")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}