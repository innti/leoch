/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "PL亮度",TableName = "MesPLDitls")]
    public class MesPLDitls:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Display(Name ="UniqueID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UniqueID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="SN")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string SN { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Title")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Title { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Batch")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Batch { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="date")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string date { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="time")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string time { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="NG")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string NG { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Scratch")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Scratch { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Finger")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Finger { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Kapian")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Kapian { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Ring")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Ring { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Bottom")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Bottom { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Black")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Black { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Raodu")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Raodu { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Shanxing")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Shanxing { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Dot")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Dot { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Pitting")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Pitting { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Chuck")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Chuck { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Belt")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Belt { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Top")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Top { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Cassette2")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Cassette2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Cassette1")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Cassette1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Intensity")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Intensity { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="iVoc")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string iVoc { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="darkArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string darkArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Bin")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Bin { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Black0")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Black0 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Black1")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Black1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="WjName")]
       [Column(TypeName="nvarchar(max)")]
       public string WjName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="StrPath")]
       [Column(TypeName="nvarchar(max)")]
       public string StrPath { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///采集时间
       /// </summary>
       [Display(Name ="采集时间")]
       [Column(TypeName="datetime")]
       public DateTime Createdate { get; set; }

       
    }
}