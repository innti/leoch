/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "花篮扫描",TableName = "MesProcuceMark")]
    public class MesProcuceMark:BaseEntity
    {
        /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Line { get; set; }

       /// <summary>
       ///工艺
       /// </summary>
       [Display(Name ="工艺")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Technology { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///编号
       /// </summary>
       [Display(Name ="编号")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Code { get; set; }

       /// <summary>
       ///工序
       /// </summary>
       [Display(Name ="工序")]
       [MaxLength(30)]
       [Column(TypeName="nvarchar(30)")]
       public string Sequence { get; set; }

       /// <summary>
       ///炉次
       /// </summary>
       [Display(Name ="炉次")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string heat { get; set; }

       /// <summary>
       ///扫描开始时间
       /// </summary>
       [Display(Name ="扫描开始时间")]
       [Column(TypeName="datetime")]
       public DateTime? ScanBeforDate { get; set; }

       /// <summary>
       ///扫描结束时间
       /// </summary>
       [Display(Name ="扫描结束时间")]
       [Column(TypeName="datetime")]
       public DateTime? ScanEndDate { get; set; }

       /// <summary>
       ///花篮1
       /// </summary>
       [Display(Name ="花篮1")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Basket1 { get; set; }

       /// <summary>
       ///花篮2
       /// </summary>
       [Display(Name ="花篮2")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Basket2 { get; set; }

       /// <summary>
       ///花篮3
       /// </summary>
       [Display(Name ="花篮3")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Basket3 { get; set; }

       /// <summary>
       ///花篮4
       /// </summary>
       [Display(Name ="花篮4")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Basket4 { get; set; }

       /// <summary>
       ///采集id
       /// </summary>
       [Display(Name ="采集id")]
       [MaxLength(200)]
       [Column(TypeName="varchar(200)")]
       public string BasketId { get; set; }

       /// <summary>
       ///花篮拼凑编号
       /// </summary>
       [Display(Name ="花篮拼凑编号")]
       [MaxLength(200)]
       [Column(TypeName="varchar(200)")]
       public string NewCode { get; set; }

       /// <summary>
       ///来料盒号1
       /// </summary>
       [Display(Name ="来料盒号1")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string InBox1 { get; set; }

       /// <summary>
       ///来料盒号2
       /// </summary>
       [Display(Name ="来料盒号2")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string InBox2 { get; set; }

       /// <summary>
       ///来料盒号3
       /// </summary>
       [Display(Name ="来料盒号3")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string InBox3 { get; set; }

       /// <summary>
       ///数量
       /// </summary>
       [Display(Name ="数量")]
       [DisplayFormat(DataFormatString="10,2")]
       [Column(TypeName="decimal")]
       public decimal? Count { get; set; }

       /// <summary>
       ///NG数
       /// </summary>
       [Display(Name ="NG数")]
       [DisplayFormat(DataFormatString="10,2")]
       [Column(TypeName="decimal")]
       public decimal? NG { get; set; }

       /// <summary>
       ///人
       /// </summary>
       [Display(Name ="人")]
       [MaxLength(30)]
       [Column(TypeName="nvarchar(30)")]
       public string UserName { get; set; }

       /// <summary>
       ///删除
       /// </summary>
       [Display(Name ="删除")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人Id
       /// </summary>
       [Display(Name ="创建人Id")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///修改时间
       /// </summary>
       [Display(Name ="修改时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///修改人
       /// </summary>
       [Display(Name ="修改人")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///修改人Id
       /// </summary>
       [Display(Name ="修改人Id")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///扫描枪位置
       /// </summary>
       [Display(Name ="扫描枪位置")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}