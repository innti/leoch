/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "测试报警",TableName = "MesResultAlarm")]
    public class MesResultAlarm:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="NID")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid NID { get; set; }

       /// <summary>
       ///采集ID
       /// </summary>
       [Display(Name ="采集ID")]
       [Column(TypeName="int")]
       public int? PNID { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Line { get; set; }

       /// <summary>
       ///报警编码
       /// </summary>
       [Display(Name ="报警编码")]
       [Column(TypeName="nvarchar(max)")]
       public string AramCode { get; set; }

       /// <summary>
       ///报警原因
       /// </summary>
       [Display(Name ="报警原因")]
       [Column(TypeName="nvarchar(max)")]
       public string Remark { get; set; }

       /// <summary>
       ///路径
       /// </summary>
       [Display(Name ="路径")]
       [Column(TypeName="nvarchar(max)")]
       public string LogPath { get; set; }

       /// <summary>
       ///文件名
       /// </summary>
       [Display(Name ="文件名")]
       [Column(TypeName="nvarchar(max)")]
       public string LogName { get; set; }

       /// <summary>
       ///时间
       /// </summary>
       [Display(Name ="时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       
    }
}