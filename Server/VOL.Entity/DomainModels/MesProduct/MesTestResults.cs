/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "测试计算",TableName = "MesTestResults")]
    public class MesTestResults : BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="NID")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int NID { get; set; }

       /// <summary>
       ///测试时间
       /// </summary>
       [Display(Name ="测试时间")]
       [Column(TypeName="datetime")]
       public DateTime? Date { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string Line { get; set; }

       /// <summary>
       ///班次
       /// </summary>
       [Display(Name ="班次")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string ShiftClass { get; set; }

       /// <summary>
       ///标题
       /// </summary>
       [Display(Name ="标题")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Title { get; set; }

       /// <summary>
       ///测试批号
       /// </summary>
       [Display(Name ="测试批号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Comment { get; set; }

       /// <summary>
       ///计算片数
       /// </summary>
       [Display(Name ="计算片数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Count { get; set; }

       /// <summary>
       ///ISC中位数
       /// </summary>
       [Display(Name ="ISC中位数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MedISC { get; set; }

       /// <summary>
       ///VOC中位数
       /// </summary>
       [Display(Name ="VOC中位数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MedVoc { get; set; }

       /// <summary>
       ///FF中位数
       /// </summary>
       [Display(Name ="FF中位数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MedFF { get; set; }

       /// <summary>
       ///ETA中位数
       /// </summary>
       [Display(Name ="ETA中位数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MedEta { get; set; }

       /// <summary>
       ///ETA平均数
       /// </summary>
       [Display(Name ="ETA平均数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MedEtaAvg { get; set; }

       /// <summary>
       ///ETA230
       /// </summary>
       [Display(Name ="ETA230")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Eta230 { get; set; }

       /// <summary>
       ///Eta235
       /// </summary>
       [Display(Name ="Eta235")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Eta235 { get; set; }

       /// <summary>
       ///Eta240
       /// </summary>
       [Display(Name ="Eta240")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Eta240 { get; set; }

       /// <summary>
       ///Eta242
       /// </summary>
       [Display(Name ="Eta242")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Eta242 { get; set; }

       /// <summary>
       ///Eta245
       /// </summary>
       [Display(Name ="Eta245")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Eta245 { get; set; }

       /// <summary>
       ///RS中位数
       /// </summary>
       [Display(Name ="RS中位数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MedRs { get; set; }

       /// <summary>
       ///C隐裂
       /// </summary>
       [Display(Name ="C隐裂")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string PAtEL2 { get; set; }

       /// <summary>
       ///ELClassA
       /// </summary>
       [Display(Name ="ELClassA")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ELClassA { get; set; }

       /// <summary>
       ///暗电流1
       /// </summary>
       [Display(Name ="暗电流1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ADL1 { get; set; }

       /// <summary>
       ///暗电流2
       /// </summary>
       [Display(Name ="暗电流2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ADL2 { get; set; }

       /// <summary>
       ///ELA
       /// </summary>
       [Display(Name ="ELA")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ELA { get; set; }

       /// <summary>
       ///OpticalA
       /// </summary>
       [Display(Name ="OpticalA")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string OpticalA { get; set; }

       /// <summary>
       ///LowVoc
       /// </summary>
       [Display(Name ="LowVoc")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LowVoc { get; set; }

       /// <summary>
       ///电流超档效率C
       /// </summary>
       [Display(Name ="电流超档效率C")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ADLXLC { get; set; }

       /// <summary>
       ///电流超档效率B
       /// </summary>
       [Display(Name ="电流超档效率B")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ADLXLB { get; set; }

       /// <summary>
       ///虚印
       /// </summary>
       [Display(Name ="虚印")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Fake { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateTime")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string Backfield5 { get; set; }

       /// <summary>
       ///编码
       /// </summary>
       [Display(Name ="编码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Code { get; set; }

       /// <summary>
       ///类型
       /// </summary>
       [Display(Name ="类型")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Sort { get; set; }

       /// <summary>
       ///RSH中位数
       /// </summary>
       [Display(Name ="RSH中位数")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MedRsh { get; set; }

       
    }
}