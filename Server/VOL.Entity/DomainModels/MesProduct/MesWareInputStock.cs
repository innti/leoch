/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "库存查询",TableName = "MesWareInputStock")]
    public class MesWareInputStock:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int Id { get; set; }

       /// <summary>
       ///仓库
       /// </summary>
       [Display(Name ="仓库")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string WareHouse { get; set; }

       /// <summary>
       ///仓位
       /// </summary>
       [Display(Name ="仓位")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string WarePostion { get; set; }

       /// <summary>
       ///外箱码
       /// </summary>
       [Display(Name ="外箱码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CodeBM { get; set; }

       /// <summary>
       ///内箱码
       /// </summary>
       [Display(Name ="内箱码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Codecode { get; set; }

       /// <summary>
       ///产品代码
       /// </summary>
       [Display(Name ="产品代码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProductNo { get; set; }

       /// <summary>
       ///产品名称
       /// </summary>
       [Display(Name ="产品名称")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       public string ProductName { get; set; }

       /// <summary>
       ///规格型号
       /// </summary>
       [Display(Name ="规格型号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProductUnit { get; set; }

       /// <summary>
       ///规格说明
       /// </summary>
       [Display(Name ="规格说明")]
       [MaxLength(400)]
       [Column(TypeName="nvarchar(400)")]
       public string ProductSpec { get; set; }

       /// <summary>
       ///批号
       /// </summary>
       [Display(Name ="批号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProductLot { get; set; }

       /// <summary>
       ///数量
       /// </summary>
       [Display(Name ="数量")]
       [Column(TypeName="int")]
       public int? Qty { get; set; }

       /// <summary>
       ///状态
       /// </summary>
       [Display(Name ="状态")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Status { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserId")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateTime")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserName")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserId")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Type")]
       [Column(TypeName="nvarchar(max)")]
       public string Type { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Spec")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Spec { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CodeClass")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CodeClass { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Qty1")]
       [Column(TypeName="int")]
       public int? Qty1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Qty2")]
       [Column(TypeName="int")]
       public int? Qty2 { get; set; }

       
    }
}