/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "仓库管理",TableName = "MesWarehouse")]
    public class MesWarehouse:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="NID")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int NID { get; set; }

       /// <summary>
       ///仓库编码
       /// </summary>
       [Display(Name ="仓库编码")]
       [MaxLength(400)]
       [Column(TypeName="nvarchar(400)")]
       [Editable(true)]
       public string Warehouse_No { get; set; }

       /// <summary>
       ///仓库名称
       /// </summary>
       [Display(Name ="仓库名称")]
       [MaxLength(400)]
       [Column(TypeName="nvarchar(400)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string Warehouse_Name { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///操作时间
       /// </summary>
       [Display(Name ="操作时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateDate { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserId")]
       [Column(TypeName="int")]
       public int? CreateUserId { get; set; }

       /// <summary>
       ///操作人
       /// </summary>
       [Display(Name ="操作人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BK01")]
       [Column(TypeName="nvarchar(max)")]
       public string BK01 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BK02")]
       [Column(TypeName="nvarchar(max)")]
       public string BK02 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BK03")]
       [Column(TypeName="nvarchar(max)")]
       public string BK03 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BK04")]
       [Column(TypeName="nvarchar(max)")]
       public string BK04 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BK05")]
       [Column(TypeName="nvarchar(max)")]
       public string BK05 { get; set; }

       
    }
}