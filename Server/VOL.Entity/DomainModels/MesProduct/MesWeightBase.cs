/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "蚀刻减重",TableName = "MesWeightBase")]
    public class MesWeightBase:BaseEntity
    {
        /// <summary>
       ///主键
       /// </summary>
       [Key]
       [Display(Name ="主键")]
       [MaxLength(50)]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Linename { get; set; }

       /// <summary>
       ///班次
       /// </summary>
       [Display(Name ="班次")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ClassName { get; set; }

       /// <summary>
       ///花篮编号
       /// </summary>
       [Display(Name ="花篮编号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Basket { get; set; }

       /// <summary>
       ///编码
       /// </summary>
       [Display(Name ="编码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Billno { get; set; }

       /// <summary>
       ///盒号
       /// </summary>
       [Display(Name ="盒号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Boxnum { get; set; }

       /// <summary>
       ///重量1
       /// </summary>
       [Display(Name ="重量1")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Weight1 { get; set; }

       /// <summary>
       ///重量2
       /// </summary>
       [Display(Name ="重量2")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Weight2 { get; set; }

       /// <summary>
       ///重量3
       /// </summary>
       [Display(Name ="重量3")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Weight3 { get; set; }

       /// <summary>
       ///重量4
       /// </summary>
       [Display(Name ="重量4")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Weight4 { get; set; }

       /// <summary>
       ///反射率1
       /// </summary>
       [Display(Name ="反射率1")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Ref1 { get; set; }

       /// <summary>
       ///反射率2
       /// </summary>
       [Display(Name ="反射率2")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Ref2 { get; set; }

       /// <summary>
       ///反射率3
       /// </summary>
       [Display(Name ="反射率3")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Ref3 { get; set; }

       /// <summary>
       ///反射率4
       /// </summary>
       [Display(Name ="反射率4")]
       [DisplayFormat(DataFormatString="18,2")]
       [Column(TypeName="decimal")]
       public decimal? Ref4 { get; set; }

       /// <summary>
       ///工艺
       /// </summary>
       [Display(Name ="工艺")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Technology { get; set; }

       /// <summary>
       ///是否删除
       /// </summary>
       [Display(Name ="是否删除")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人ID
       /// </summary>
       [Display(Name ="创建人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///更新时间
       /// </summary>
       [Display(Name ="更新时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///更新人
       /// </summary>
       [Display(Name ="更新人")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///更新人ID
       /// </summary>
       [Display(Name ="更新人ID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///备用字段1
       /// </summary>
       [Display(Name ="备用字段1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///备用字段2
       /// </summary>
       [Display(Name ="备用字段2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///备用字段3
       /// </summary>
       [Display(Name ="备用字段3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///备用字段4
       /// </summary>
       [Display(Name ="备用字段4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///备用字段5
       /// </summary>
       [Display(Name ="备用字段5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}