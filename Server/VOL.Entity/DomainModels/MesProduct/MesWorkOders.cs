/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "工单管理",TableName = "MesWorkOders")]
    public class MesWorkOders:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///产品
       /// </summary>
       [Display(Name ="产品")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string Goods { get; set; }

       /// <summary>
       ///车间
       /// </summary>
       [Display(Name ="车间")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string WorkShop { get; set; }

       /// <summary>
       ///生产线
       /// </summary>
       [Display(Name ="生产线")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string Line { get; set; }

       /// <summary>
       ///班次
       /// </summary>
       [Display(Name ="班次")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string Shift { get; set; }

       /// <summary>
       ///计划员
       /// </summary>
       [Display(Name ="计划员")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string PlanUser { get; set; }

       /// <summary>
       ///生产计划
       /// </summary>
       [Display(Name ="生产计划")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string Plans { get; set; }

       /// <summary>
       ///工单号
       /// </summary>
       [Display(Name ="工单号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string WorkOrder { get; set; }

       /// <summary>
       ///计划生产日期
       /// </summary>
       [Display(Name ="计划生产日期")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? StartDate { get; set; }

       /// <summary>
       ///计划完成日期
       /// </summary>
       [Display(Name ="计划完成日期")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? EndDate { get; set; }

       /// <summary>
       ///计划数量
       /// </summary>
       [Display(Name ="计划数量")]
       [Column(TypeName="decimal")]
       [Editable(true)]
       public decimal? Counts { get; set; }

       /// <summary>
       ///开始条码
       /// </summary>
       [Display(Name ="开始条码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string StartBarCode { get; set; }

       /// <summary>
       ///结束条码
       /// </summary>
       [Display(Name ="结束条码")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string EndBarCode { get; set; }

       /// <summary>
       ///计划时间
       /// </summary>
       [Display(Name ="计划时间")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? PlanDate { get; set; }

       /// <summary>
       ///是否完成
       /// </summary>
       [Display(Name ="是否完成")]
       [MaxLength(8)]
       [Column(TypeName="nvarchar(8)")]
       [Editable(true)]
       public string IsOver { get; set; }

       /// <summary>
       ///完成时间
       /// </summary>
       [Display(Name ="完成时间")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? OverDate { get; set; }

       /// <summary>
       ///删除
       /// </summary>
       [Display(Name ="删除")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人Id
       /// </summary>
       [Display(Name ="创建人Id")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///修改时间
       /// </summary>
       [Display(Name ="修改时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///修改人
       /// </summary>
       [Display(Name ="修改人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///修改人Id
       /// </summary>
       [Display(Name ="修改人Id")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}