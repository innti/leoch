/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "保养停工明细",TableName = "MesdeviceMaintainance")]
    public class MesdeviceMaintainance:BaseEntity
    {
        /// <summary>
       ///主键
       /// </summary>
       [Key]
       [Display(Name ="主键")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///分类
       /// </summary>
       [Display(Name ="分类")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Sort { get; set; }

       /// <summary>
       ///设备编号
       /// </summary>
       [Display(Name ="设备编号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceNum { get; set; }

       /// <summary>
       ///设备名称
       /// </summary>
       [Display(Name ="设备名称")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceName { get; set; }

       /// <summary>
       ///设备类型
       /// </summary>
       [Display(Name ="设备类型")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceTypeName { get; set; }

       /// <summary>
       ///设备类型ID
       /// </summary>
       [Display(Name ="设备类型ID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceTypeID { get; set; }

       /// <summary>
       ///生产线
       /// </summary>
       [Display(Name ="生产线")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LineName { get; set; }

       /// <summary>
       ///产线ID
       /// </summary>
       [Display(Name ="产线ID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string LineID { get; set; }

       /// <summary>
       ///工序
       /// </summary>
       [Display(Name ="工序")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessesName { get; set; }

       /// <summary>
       ///工序ID
       /// </summary>
       [Display(Name ="工序ID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string ProcessesID { get; set; }

       /// <summary>
       ///设备状态
       /// </summary>
       [Display(Name ="设备状态")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceStatus { get; set; }

       /// <summary>
       ///型号
       /// </summary>
       [Display(Name ="型号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceModel { get; set; }

       /// <summary>
       ///内部SN
       /// </summary>
       [Display(Name ="内部SN")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceSN { get; set; }

       /// <summary>
       ///保养内容
       /// </summary>
       [Display(Name ="保养内容")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string DeviceContent { get; set; }

       /// <summary>
       ///状态
       /// </summary>
       [Display(Name ="状态")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Status { get; set; }

       /// <summary>
       ///备注
       /// </summary>
       [Display(Name ="备注")]
       [MaxLength(400)]
       [Column(TypeName="nvarchar(400)")]
       public string Remark { get; set; }

       /// <summary>
       ///保养时间
       /// </summary>
       [Display(Name ="保养时间")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MachinetopTime { get; set; }

       /// <summary>
       ///下次保养时间
       /// </summary>
       [Display(Name ="下次保养时间")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MachineDatetime { get; set; }

       /// <summary>
       ///是否删除
       /// </summary>
       [Display(Name ="是否删除")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人ID
       /// </summary>
       [Display(Name ="创建人ID")]
       [Column(TypeName="int")]
       public int? CreateUserId { get; set; }

       /// <summary>
       ///更新时间
       /// </summary>
       [Display(Name ="更新时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///更新人
       /// </summary>
       [Display(Name ="更新人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///更新人ID
       /// </summary>
       [Display(Name ="更新人ID")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///备用字段1
       /// </summary>
       [Display(Name ="备用字段1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///备用字段2
       /// </summary>
       [Display(Name ="备用字段2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///备用字段3
       /// </summary>
       [Display(Name ="备用字段3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///备用字段4
       /// </summary>
       [Display(Name ="备用字段4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///备用字段5
       /// </summary>
       [Display(Name ="备用字段5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}