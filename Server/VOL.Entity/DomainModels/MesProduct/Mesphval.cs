/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "铜栅数据",TableName = "Mesphval")]
    public class Mesphval:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///类型
       /// </summary>
       [Display(Name ="类型")]
       [MaxLength(40)]
       [Column(TypeName="nvarchar(40)")]
       public string Type { get; set; }

       /// <summary>
       ///日期
       /// </summary>
       [Display(Name ="日期")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Dates { get; set; }

       /// <summary>
       ///时间
       /// </summary>
       [Display(Name ="时间")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Times { get; set; }

       /// <summary>
       ///产线
       /// </summary>
       [Display(Name ="产线")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string line { get; set; }

       /// <summary>
       ///载板编号
       /// </summary>
       [Display(Name ="载板编号")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Code { get; set; }

       /// <summary>
       ///run次
       /// </summary>
       [Display(Name ="run次")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Run { get; set; }

       /// <summary>
       ///值
       /// </summary>
       [Display(Name ="值")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Nvalue { get; set; }

       /// <summary>
       ///上灯上框
       /// </summary>
       [Display(Name ="上灯上框")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Nvalue1 { get; set; }

       /// <summary>
       ///上灯下框
       /// </summary>
       [Display(Name ="上灯下框")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Nvalue2 { get; set; }

       /// <summary>
       ///下灯上框
       /// </summary>
       [Display(Name ="下灯上框")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Nvalue3 { get; set; }

       /// <summary>
       ///下灯下框
       /// </summary>
       [Display(Name ="下灯下框")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Nvalue4 { get; set; }

       /// <summary>
       ///判定
       /// </summary>
       [Display(Name ="判定")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string Pvalue { get; set; }

       /// <summary>
       ///工序
       /// </summary>
       [Display(Name ="工序")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Sequence { get; set; }

       /// <summary>
       ///备注
       /// </summary>
       [Display(Name ="备注")]
       [MaxLength(400)]
       [Column(TypeName="nvarchar(400)")]
       public string Remark { get; set; }

       /// <summary>
       ///删除
       /// </summary>
       [Display(Name ="删除")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateTime { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///创建人Id
       /// </summary>
       [Display(Name ="创建人Id")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///修改时间
       /// </summary>
       [Display(Name ="修改时间")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///修改人
       /// </summary>
       [Display(Name ="修改人")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///修改人Id
       /// </summary>
       [Display(Name ="修改人Id")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string Backfield5 { get; set; }

       
    }
}