/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "测试数据",TableName = "Mesresults")]
    public class Mesresults:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Display(Name ="UniqueID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UniqueID { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BatchID")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string BatchID { get; set; }

       /// <summary>
       ///标题
       /// </summary>
       [Display(Name ="标题")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Title { get; set; }

       /// <summary>
       ///测试时间
       /// </summary>
       [Display(Name ="测试时间")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string TestTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="TestDate")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string TestDate { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Comment")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string Comment { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BIN")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string BIN { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Isc")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Isc { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Uoc")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Uoc { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="FF")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string FF { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Eta")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Eta { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Impp")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Impp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Umpp")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Umpp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Pmpp")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Pmpp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IRev1")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string IRev1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IRev2")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string IRev2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="RserLfDfIEC")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string RserLfDfIEC { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="RshuntDfDr")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string RshuntDfDr { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Tcell")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Tcell { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Tmonicell")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Tmonicell { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Insol")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Insol { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="RserLfDf")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string RserLfDf { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="RshuntLf")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string RshuntLf { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Tenv")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Tenv { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CellParamTkI")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CellParamTkI { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CellParamTkU")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CellParamTkU { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CellParamArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CellParamArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CellParamTyp")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string CellParamTyp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="InsolMpp")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string InsolMpp { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CorrectedToInsol")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string CorrectedToInsol { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CellIDStr")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string CellIDStr { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IRevmax")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string IRevmax { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="MonicellMvToInsol")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string MonicellMvToInsol { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="FlashMonicellVoltage")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string FlashMonicellVoltage { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="OpticalFrontColor")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string OpticalFrontColor { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="OpticalFrontQual")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string OpticalFrontQual { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="OpticalRearQual")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string OpticalRearQual { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ELMeanGray")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string ELMeanGray { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2CrackDefaultCount")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2CrackDefaultCount { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2CrackDefaultArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2CrackDefaultArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2CrackDefaultMaxArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2CrackDefaultMaxArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2CrackDefaultMaxMeanConfidence")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2CrackDefaultMaxMeanConfidence { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2CrackGSSolar2Count")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2CrackGSSolar2Count { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2DarkDefaultCount")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2DarkDefaultCount { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2DarkDefaultArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2DarkDefaultArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2DarkDefaultMaxArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2DarkDefaultMaxArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2DarkDefaultMaxMeanConfidence")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2DarkDefaultMaxMeanConfidence { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2DarkDefaultSeverity")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2DarkDefaultSeverity { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2FingerDefaultCount")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2FingerDefaultCount { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2FingerDefaultArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2FingerDefaultArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2FingerDefaultMaxArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2FingerDefaultMaxArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2FingerDefaultMaxMeanConfidence")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2FingerDefaultMaxMeanConfidence { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2FingerDefaultSeverity")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2FingerDefaultSeverity { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ELA")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string ELA { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2OxRing")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2OxRing { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2Class")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2Class { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2FingerPrint")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2FingerPrint { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2Contamination")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2Contamination { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="OpticalA")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string OpticalA { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2CardSlot")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2CardSlot { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="DS")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string DS { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="GFS")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string GFS { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Cut")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Cut { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="SXPY")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string SXPY { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="XI")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string XI { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="BB")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string BB { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Copper")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string Copper { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="FSBJ")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string FSBJ { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LB")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string LB { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="LT")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string LT { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ZW")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string ZW { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="OTHER")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string OTHER { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2ScratchDefaultCount")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2ScratchDefaultCount { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2ScratchDefaultArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2ScratchDefaultArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2ScratchDefaultMaxArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2ScratchDefaultMaxArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2ScratchDefaultMaxMeanConfidence")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2ScratchDefaultMaxMeanConfidence { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2ScratchDefaultSeverity")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2ScratchDefaultSeverity { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2SpotDefaultCount")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2SpotDefaultCount { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2SpotDefaultArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2SpotDefaultArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2SpotDefaultMaxArea")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2SpotDefaultMaxArea { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="EL2SpotDefaultMaxMeanConfidence")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string EL2SpotDefaultMaxMeanConfidence { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ELClassA")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string ELClassA { get; set; }

       /// <summary>
       ///Recipe
       /// </summary>
       [Display(Name ="Recipe")]
       [MaxLength(510)]
       [Column(TypeName="nvarchar(510)")]
       public string RecipeName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ELGradeA")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string ELGradeA { get; set; }

       /// <summary>
       ///配方
       /// </summary>
       [Display(Name ="配方")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string MAXID { get; set; }

       /// <summary>
       ///盒号
       /// </summary>
       [Display(Name ="盒号")]
       [MaxLength(60)]
       [Column(TypeName="nvarchar(60)")]
       public string PrintCode { get; set; }

       /// <summary>
       ///文件名
       /// </summary>
       [Display(Name ="文件名")]
       [MaxLength(400)]
       [Column(TypeName="nvarchar(400)")]
       public string PrintCodeNum { get; set; }

       /// <summary>
       ///线别
       /// </summary>
       [Display(Name ="线别")]
       [MaxLength(20)]
       [Column(TypeName="nvarchar(20)")]
       public string Line { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///采集时间
       /// </summary>
       [Display(Name ="采集时间")]
       [Column(TypeName="datetime")]
       public DateTime? Createdate { get; set; }

       
    }
}