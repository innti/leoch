/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "人员管理",TableName = "Mesusersbase")]
    public class Mesusersbase:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="uniqueidentifier")]
       [Required(AllowEmptyStrings=false)]
       public Guid Id { get; set; }

       /// <summary>
       ///工号
       /// </summary>
       [Display(Name ="工号")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string UserCode { get; set; }

       /// <summary>
       ///姓名
       /// </summary>
       [Display(Name ="姓名")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string UserName { get; set; }

       /// <summary>
       ///电话
       /// </summary>
       [Display(Name ="电话")]
       [MaxLength(24)]
       [Column(TypeName="nvarchar(24)")]
       [Editable(true)]
       public string UserPhone { get; set; }

       /// <summary>
       ///邮箱
       /// </summary>
       [Display(Name ="邮箱")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string UserEmail { get; set; }

       /// <summary>
       ///所属车间
       /// </summary>
       [Display(Name ="所属车间")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string UserShop { get; set; }

       /// <summary>
       ///所属生产线
       /// </summary>
       [Display(Name ="所属生产线")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string UserLine { get; set; }

       /// <summary>
       ///工序
       /// </summary>
       [Display(Name ="工序")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string UserSequence { get; set; }

       /// <summary>
       ///所属班次
       /// </summary>
       [Display(Name ="所属班次")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string UserClass { get; set; }

       /// <summary>
       ///岗位名称
       /// </summary>
       [Display(Name ="岗位名称")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string UserPost { get; set; }

       /// <summary>
       ///入厂日期
       /// </summary>
       [Display(Name ="入厂日期")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? UserInDate { get; set; }

       /// <summary>
       ///状态
       /// </summary>
       [Display(Name ="状态")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string UserStatus { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsDel")]
       [Column(TypeName="int")]
       public int? IsDel { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateTime")]
       [Column(TypeName="datetime")]
       [Required(AllowEmptyStrings=false)]
       public DateTime CreateTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserName")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="CreateUserId")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string CreateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateTime")]
       [Column(TypeName="datetime")]
       public DateTime? UpdateTime { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserName")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string UpdateUserName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="UpdateUserId")]
       [MaxLength(50)]
       [Column(TypeName="varchar(50)")]
       public string UpdateUserId { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(1000)]
       [Column(TypeName="nvarchar(1000)")]
       public string Backfield1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield2")]
       [MaxLength(1000)]
       [Column(TypeName="nvarchar(1000)")]
       public string Backfield2 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield3")]
       [MaxLength(1000)]
       [Column(TypeName="nvarchar(1000)")]
       public string Backfield3 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield4")]
       [MaxLength(1000)]
       [Column(TypeName="nvarchar(1000)")]
       public string Backfield4 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield5")]
       [MaxLength(1000)]
       [Column(TypeName="nvarchar(1000)")]
       public string Backfield5 { get; set; }

       
    }
}