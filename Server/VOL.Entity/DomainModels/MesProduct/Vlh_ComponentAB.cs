/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "AB胶查询",TableName = "Vlh_ComponentAB")]
    public class Vlh_ComponentAB:BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        [Display(Name = "RecordDate")]
        [Column(TypeName = "datetime")]
        public DateTime? RecordDate { get; set; }


        /// <summary>
        ///线别
        /// </summary>
        [Display(Name ="线别")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       [Required(AllowEmptyStrings=false)]
       public string Name { get; set; }

       /// <summary>
       ///工序
       /// </summary>
       [Display(Name ="工序")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       public string ProcessName { get; set; }

       /// <summary>
       ///工序代码
       /// </summary>
       [Display(Name ="工序代码")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       public string ProcessCode { get; set; }

       /// <summary>
       ///位置
       /// </summary>
       [Display(Name ="位置")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       [Required(AllowEmptyStrings=false)]
       public string EquipmentName { get; set; }

       /// <summary>
       ///上规格
       /// </summary>
       [Display(Name ="上规格")]
       [Column(TypeName="float")]
       [Required(AllowEmptyStrings=false)]
       public float SpecUp { get; set; }

       /// <summary>
       ///中心值
       /// </summary>
       [Display(Name ="中心值")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string SpecLine { get; set; }

       /// <summary>
       ///下规格
       /// </summary>
       [Display(Name ="下规格")]
       [Column(TypeName="float")]
       [Required(AllowEmptyStrings=false)]
       public float SpecLower { get; set; }

       /// <summary>
       ///值1
       /// </summary>
       [Display(Name ="值1")]
       [Column(TypeName="float")]
       [Required(AllowEmptyStrings=false)]
       public float P1 { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Id")]
       [Column(TypeName="bigint")]
       [Required(AllowEmptyStrings=false)]
       public long Id { get; set; }

       /// <summary>
       ///值2
       /// </summary>
       [Display(Name ="值2")]
       [Column(TypeName="float")]
       [Required(AllowEmptyStrings=false)]
       public float P2 { get; set; }

       /// <summary>
       ///值3
       /// </summary>
       [Display(Name ="值3")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       public string P3 { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName= "datetime")]
       public DateTime CreationTime { get; set; }


    }
}