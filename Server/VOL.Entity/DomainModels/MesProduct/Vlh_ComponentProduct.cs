/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "组件产能",TableName = "Vlh_ComponentProduct")]
    public class Vlh_ComponentProduct:BaseEntity
    {
        /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="OperatorDate")]
       [Column(TypeName= "DateTime")]
       [Required(AllowEmptyStrings=false)]
       public DateTime OperatorDate { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="ProcessName")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       public string ProcessName { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="IsPass")]
       [Column(TypeName="bit")]
       [Required(AllowEmptyStrings=false)]
       public bool  IsPass { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Backfield1")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Required(AllowEmptyStrings=false)]
       public string Backfield1 { get; set; }

       
    }
}