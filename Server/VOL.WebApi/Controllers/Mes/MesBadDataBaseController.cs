/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesBadDataBaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using VOL.Entity.DomainModels;
using System.Linq;

namespace Vol.Mes.Controllers
{
    [Route("api/MesBadDataBase")]
    [PermissionTable(Name = "MesBadDataBase")]
    public partial class MesBadDataBaseController : ApiBaseController<IMesBadDataBaseService>
    {
        public MesBadDataBaseController(IMesBadDataBaseService service)
        : base(service)
        {
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] MesBadDataBase model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();

            List<string> Xday = new List<string>();

            var listType = list.Select(t => t.BadCase).GroupBy(t => t).ToList();
            foreach (var item in listType)
            {
                Xday.Add(item.Key);
            }
            List<string> yday1 = new List<string>();
            List<string> yday2 = new List<string>();
            List<MesseriesData> listmes = new List<MesseriesData>();
            if (list.Count > 0)
            {
                var listqty = list.Select(t => t.Qty).ToList();
                double sumqty = 0;
                foreach (var item in listqty)
                {
                    sumqty += Convert.ToDouble(item);
                }
                foreach (var item in Xday)
                {
                    var listbadqty = list.Where(t => t.BadCase == item).ToList().Select(t => t.Qty).ToList();
                    double badsumqty = 0;
                    foreach (var itemqty in listbadqty)
                    {
                        badsumqty += Convert.ToDouble(itemqty);
                    }
                    yday1.Add(badsumqty.ToString());
                    var dqty = Convert.ToDouble(badsumqty) / Convert.ToDouble(sumqty) * 100.00;
                    yday2.Add(Math.Round(dqty, 3).ToString());
                }
                yday1.Sort((x, y) => -x.CompareTo(y));//降序
                MesseriesData messeriesData = new MesseriesData();
                messeriesData.data = yday1.ToArray();
                messeriesData.name = "不良原因";
                messeriesData.type = "bar";
                messeriesData.yAxisIndex = 0;
                listmes.Add(messeriesData);

                MesseriesData messeriesData1 = new MesseriesData();
                messeriesData1.data = yday2.ToArray();
                messeriesData1.name = "直通率";
                messeriesData1.type = "line";
                messeriesData1.yAxisIndex = 1;
                listmes.Add(messeriesData1);
            }

            mesChartModel.MesseriesDatas = listmes;
            mesChartModel.MesxAxisData = Xday.ToArray();

            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {
            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; }
            public string type { get; set; } = "line";
            public int yAxisIndex { get; set; } = 0;
            public string[] data { get; set; }

        }
    }
}

