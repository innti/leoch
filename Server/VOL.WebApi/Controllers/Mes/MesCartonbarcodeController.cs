/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesCartonbarcodeController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
namespace Vol.Mes.Controllers
{
    [Route("api/MesCartonbarcode")]
    [PermissionTable(Name = "MesCartonbarcode")]
    public partial class MesCartonbarcodeController : ApiBaseController<IMesCartonbarcodeService>
    {
        public MesCartonbarcodeController(IMesCartonbarcodeService service)
        : base(service)
        {
        }
    }
}

