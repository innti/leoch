/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesChemicalBaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
namespace Vol.Mes.Controllers
{
    [Route("api/MesChemicalBase")]
    [PermissionTable(Name = "MesChemicalBase")]
    public partial class MesChemicalBaseController : ApiBaseController<IMesChemicalBaseService>
    {
        public MesChemicalBaseController(IMesChemicalBaseService service)
        : base(service)
        {
        }
    }
}

