/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesInputWarehouseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using VOL.Core.Response;

namespace Vol.Mes.Controllers
{
    [Route("api/MesInputWarehouse")]
    [PermissionTable(Name = "MesInputWarehouse")]
    public partial class MesInputWarehouseController : ApiBaseController<IMesInputWarehouseService>
    {
        public MesInputWarehouseController(IMesInputWarehouseService service)
        : base(service)
        {

        }
        [HttpGet, Route("SearchBoxNumAsync"), AllowAnonymous]
        public async Task<TableData> SearchBoxNumAsync(string Code, string Box)
        {
            var result = new TableData();
            //查询数据
            var list = await Service.SearchBoxNum(Code, Box);
            if (list.Count > 0)
            {
                result.code = 200;
                result.data = list;
                result.count = list.Count;

            }
            else
            {
                result.code = 0;
                result.count = 0;
            }

            return result;

        }

        [HttpGet, Route("InsertTakeOverBoxNum"), AllowAnonymous]
        public TableData InsertTakeOverBoxNum(string Code, string Box)
        {
            var result = new TableData();
            //查询数据
            var flag = Service.InsertTakeOverBoxNum(Code, Box);
            if (flag)
            {
                result.code = 200;
            }
            else
            {
                result.code = 0;
                result.count = 0;
            }

            return result;

        }

    }
}

