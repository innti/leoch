/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesLineWidthBaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using VOL.Core.NPOI;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using VOL.Entity.DomainModels;
using System.Linq;
using System.Collections.Generic;

namespace Vol.Mes.Controllers
{
    [Route("api/MesLineWidthBase")]
    [PermissionTable(Name = "MesLineWidthBase")]
    public partial class MesLineWidthBaseController : ApiBaseController<IMesLineWidthBaseService>
    {
        public MesLineWidthBaseController(IMesLineWidthBaseService service)
        : base(service)
        {
        }
        NPOIReport nPOIReport = new NPOIReport();
        [HttpPost, Route("ExcelReport"), AllowAnonymous]
        public async Task<IActionResult> ExcelReport([FromBody] MesLineWidthBase model)
        {
            try
            {
                string exprotPath = "";
                if (model.Type.Contains("电镀线高"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC电镀线高.xlsx");
                }
                else
                if (model.Type.Contains("印刷线高"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC印刷线高.xlsx");
                }
                else
                if (model.Type.Contains("印刷线宽"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC印刷线宽.xlsx");
                }
                else
                if (model.Type.Contains("黄光线宽"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC黄光线宽.xlsx");
                }
                else
                if (model.Type.Contains("焊盘高度"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC印刷焊盘高度.xlsx");
                }
                else
                if (model.Type.Contains("电镀线宽"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC电镀线宽.xlsx");
                }
                else
                if (model.Type.Contains("显影露铜"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC黄光露铜.xlsx");
                }
                else
                {
                    exprotPath = nPOIReport.GetReportFile("SPC印刷线宽.xlsx");
                }

                FileStream file = new FileStream(exprotPath, FileMode.Open, FileAccess.Read);
                var workbook = new XSSFWorkbook(file);
                var sheet = workbook.GetSheetAt(0);
                //查询数据
                var list = await Service.ExcelReport(model);

                ICell cell2 = sheet.GetRow(2).GetCell(2);
                cell2.SetCellValue(model.Type + list[0].Line);
                ICell cell3 = sheet.GetRow(4).GetCell(2);
                cell3.SetCellValue(list[0].Station + list[0].Sort);

                int numX = 2;
                string date = model.Backfield1.Split(',')[0];
                string strdate = string.Format("{0}-{1}-{2}", date.Split('-')[0], date.Split('-')[1], "01");
                DateTime dt = Convert.ToDateTime(strdate);
                int day = DateTime.DaysInMonth(dt.Year, dt.Month) + 1;

                for (int i = 1; i < day; i++)
                {
                    string newdate = string.Format("{0}-{1}-{2}", date.Split('-')[0], date.Split('-')[1], i.ToString().PadLeft(2, '0'));
                    DateTime dateTime = Convert.ToDateTime(newdate);
                    var dataList = list.Where(t => t.SDate == dateTime).OrderBy(t => t.Billno).ThenBy(r => r.AorB).ToList();

                    var newWhiteList = dataList.Where(t => t.ShiftClass.Contains("白")).OrderBy(t => t.Billno).ThenBy(r => r.AorB).ToList();
                    var newBlackList = dataList.Where(t => t.ShiftClass.Contains("夜")).OrderBy(t => t.Billno).ThenBy(r => r.AorB).ToList();

                    for (int m = 0; m < newWhiteList.Count; m++)
                    {
                        ICell cell4 = sheet.GetRow(8 + m).GetCell(numX);
                        double qty = Convert.ToDouble(newWhiteList[m].SValue);
                        cell4.SetCellValue(qty);
                    }
                    for (int m = 0; m < newBlackList.Count; m++)
                    {
                        ICell cell5 = sheet.GetRow(8 + m).GetCell(numX + 1);
                        double qty = Convert.ToDouble(newBlackList[m].SValue);
                        cell5.SetCellValue(qty);

                    }
                    numX += 2;
                }

                using (var stream = new FileStream(exprotPath, FileMode.Create, FileAccess.Write))
                {
                    workbook.Write(stream);
                }

                string dicPath = $"Upload/Excel/";
                string newName = System.IO.Path.GetFileName(exprotPath);
                return Content(dicPath + newName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return default;
            }

        }
        [HttpPost, Route("ExceChart"), AllowAnonymous]
        public async Task<IActionResult> ExceChart([FromBody] MesLineWidthBase model)
        {

            //查询数据
            var list = await Service.ExcelReport(model);

            MesChartModel mesChartModel = new MesChartModel();

            var listdattime = list.Select(t => t.Billno).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            List<double> array = new List<double>();
            List<MesseriesData> listanew = new List<MesseriesData>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
                var listvalue = list.Where(t => t.Billno == item.Key).ToList();
                double AGValue = 0;
                foreach (var itemValue in listvalue)
                {
                    AGValue += Convert.ToDouble(itemValue.SValue == string.Empty ? "0" : itemValue.SValue);
                }
                var vagValue = AGValue /(double) listvalue.Count();
                array.Add(vagValue);
            }
            MesseriesData messeriesData = new MesseriesData();

            messeriesData.data = array.ToArray();

            listanew.Add(messeriesData);

            mesChartModel.MesseriesDatas = listanew;
            mesChartModel.MesxAxisData = Xday.ToArray();

            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {
            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string type { get; set; } = "line";

            public double[] data { get; set; }
        }
    }
}

