/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesProcuceMarkController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using VOL.Entity.DomainModels;
using System.Linq;
using System;
using VOL.Core.NPOI;

namespace Vol.Mes.Controllers
{
    [Route("api/MesProcuceMark")]
    [PermissionTable(Name = "MesProcuceMark")]
    public partial class MesProcuceMarkController : ApiBaseController<IMesProcuceMarkService>
    {
        public MesProcuceMarkController(IMesProcuceMarkService service)
        : base(service)
        {
        }
        NPOIReport nPOI = new NPOIReport();
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] MesProcuceMark model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();

            list.ToList().ForEach(i => i.Backfield1 = Convert.ToDateTime(i.CreateTime).ToString("yyyy-MM-dd"));

            var listdattime = list.Select(t => t.Backfield1).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
            }

            List<MesseriesData> listData = new List<MesseriesData>();

            string[] arr = new string[] { "制绒清洗插片", "PECVD制造下料", "PVD下料", "印刷下料", "铜栅去边", "铜栅黄光", "铜栅电镀", "测试扫描" };

            //   var listLegendData = list.Select(t => t.Sequence).GroupBy(t => t).ToList();

            foreach (var item in arr)
            {
                List<int> Yday = new List<int>();
                foreach (var itemday in Xday)
                {
                    var listcount = list.Where(t => t.Backfield1 == itemday && t.Sequence == item).Count();
                    if (listcount > 0)
                    {
                        Yday.Add(listcount * 100);
                    }
                }
                MesseriesData messeriesData = new MesseriesData();
                messeriesData.name = nPOI.ReturnNewName(item);
                messeriesData.data = Yday.ToArray();
                listData.Add(messeriesData);
            }
            mesChartModel.MesLegendData = new string[] { "制绒清洗", "CVD制造", "PVD制造", "印刷制造", "铜栅去边", "铜栅黄光", "铜栅电镀", "测试制造" };
            mesChartModel.MesxAxisData = Xday.ToArray();
            mesChartModel.MesseriesDatas = listData;
            return Json(mesChartModel);
        }

        [HttpPost, Route("ExcelbigDataChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelbigDataChart()
        {
            var startDate = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date.ToString("yyyy-MM-dd");
            var endtDate = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date.AddMonths(1).ToString("yyyy-MM-dd");
            
            MesProcuceMark model = new MesProcuceMark();
            model.Backfield1 = startDate + "," + endtDate;// "2022-02-01,2022-02-28";
            //查询数据
            var list = await Service.SearchCharts(model);
            var week = NPOIReport.ReturnListWeek();
            var month = NPOIReport.ReturnListMonth();
            MesChartbigDataModel mesChartModel = new MesChartbigDataModel();

            list.ToList().ForEach(i => i.Backfield1 = Convert.ToDateTime(i.CreateTime).ToString("yyyy-MM-dd"));
            List<int> listdataWeekcount = new List<int>();
            List<int> listdataWeekb = new List<int>();
            List<int> listdataWeekW = new List<int>();
            foreach (var item in week)
            {
                var listcount1 = list.Where(t => t.Backfield1.Contains(item)).ToList().Count();
                var listcount2 = list.Where(t => t.Backfield1.Contains(item) && t.UserName.Contains("白班")).ToList().Count();
                var listcount3 = list.Where(t => t.Backfield1.Contains(item) && t.UserName.Contains("夜班")).ToList().Count();

                listdataWeekcount.Add(listcount1 * 100);
                listdataWeekb.Add(listcount2 * 100);
                listdataWeekW.Add(listcount3 * 100);
            }
            string[] arr = new string[] { "制绒清洗插片", "PECVD制造下料", "PVD下料", "印刷下料", "铜栅去边", "铜栅黄光", "铜栅电镀", "测试扫描" };
            List<int> listMonth = new List<int>();
            List<int> listMonthcount = new List<int>();
            foreach (var itemlist in arr)
            {
                var listcount = list.Where(t => t.Sequence == itemlist).Count();
                if (listcount > 0)
                {
                    listMonth.Add(listcount * 100);
                }
    
            }
            List<MesChartbigModel> mesChartbigs = new List<MesChartbigModel>(); 

            List<string> mothchat=new List<string>();
            List<double> listmode1 = new List<double>();
            List<double> listmode2 = new List<double>();
            List<double> listmode3 = new List<double>();
            List<double> listmode4 = new List<double>();
            List<double> listmode5 = new List<double>();

            MesChartbigModel model1 = new MesChartbigModel();
            model1.name = "制绒";
            MesChartbigModel model2 = new MesChartbigModel();
            model2.name = "CVD";
            MesChartbigModel model3 = new MesChartbigModel();
            model3.name = "PVD";
            MesChartbigModel model4 = new MesChartbigModel();
            model4.name = "印刷";
            MesChartbigModel model5 = new MesChartbigModel();
            model5.name = "测试";
            foreach (var item in month)
            {
                mothchat.Add(item);
                listmode1.Add(100.0);
                listmode2.Add(100.0);
                listmode3.Add(100.0);
                listmode4.Add(100.0);
                listmode5.Add(100.0);
            }
            model1.data = listmode1.ToArray();
            model2.data = listmode2.ToArray();
            model3.data = listmode3.ToArray();
            model4.data = listmode4.ToArray();
            model5.data = listmode5.ToArray();
            mesChartbigs.Add(model1);
            mesChartbigs.Add(model2);
            mesChartbigs.Add(model3);
            mesChartbigs.Add(model4);
            mesChartbigs.Add(model5);

            listMonthcount.Add(list.Count);
            var LISTB= list.Where(t =>  t.UserName.Contains("白班")).ToList().Count();
            var LISTw = list.Where(t => t.UserName.Contains("夜班")).ToList().Count();
            listMonthcount.Add(LISTB );
            listMonthcount.Add(LISTw );
          
            mesChartModel.chartLeft1data= listMonth.ToArray();
            mesChartModel.chartLeft2data = listdataWeekb.ToArray();
            mesChartModel.chartLeft2data2 = listdataWeekW.ToArray();
            mesChartModel.chartLeft3data = listdataWeekcount.ToArray();
            mesChartModel.Chartcount = listMonthcount.ToArray();
            mesChartModel.ChartRight1month= mothchat.ToArray();
            mesChartModel.chartRight1series = mesChartbigs;
            return Json(mesChartModel);
        }
        
        public partial class MesChartbigDataModel
        {
            public int[] chartLeft1data { get; set; }
            public int[] chartLeft2data { get; set; }
            public int[] chartLeft2data2 { get; set; }
            public int[] chartLeft3data { get; set; }
            public int[] Chartcount { get; set; }
            public string[] ChartRight1month { get; set; }
            public List<MesChartbigModel> chartRight1series { get; set; }
        }
        public partial class MesChartbigModel
        {
            public string name { get; set; }
            public string type { get; set; } = "line";
            public bool smooth { get; set; } = true;
            public double[] data { get; set; }
        }
        public partial class MesChartModel
        {
            public string[] MesLegendData { get; set; }
            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; }
            public string type { get; set; } = "bar";
            public int barGap { get; set; } = 0;
            public emphasis Emphasis { get; set; } = new emphasis();
            public int[] data { get; set; }
        }
    }
}

