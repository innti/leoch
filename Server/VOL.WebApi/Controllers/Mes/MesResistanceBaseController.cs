/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesResistanceBaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using VOL.Core.NPOI;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using VOL.Entity.DomainModels;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Linq;

namespace Vol.Mes.Controllers
{
    [Route("api/MesResistanceBase")]
    [PermissionTable(Name = "MesResistanceBase")]
    public partial class MesResistanceBaseController : ApiBaseController<IMesResistanceBaseService>
    {
        public MesResistanceBaseController(IMesResistanceBaseService service)
        : base(service)
        {
        }
        NPOIReport nPOIReport = new NPOIReport();
        [HttpPost, Route("ExcelReport"), AllowAnonymous]
        public async Task<IActionResult> ExcelReport([FromBody] MesResistanceBase model)
        {
            try
            {
                string exprotPath = "";
                    exprotPath = nPOIReport.GetReportFile("SPC印刷线电阻.xlsx");
                
                FileStream file = new FileStream(exprotPath, FileMode.Open, FileAccess.Read);
                var workbook = new XSSFWorkbook(file);
                var sheet = workbook.GetSheetAt(0);
                //查询数据
                var list = await Service.ExcelReport(model);


                ICell cell2 = sheet.GetRow(2).GetCell(2);
                cell2.SetCellValue(model.Type + list[0].Line);
                ICell cell3 = sheet.GetRow(4).GetCell(2);
                cell3.SetCellValue(list[0].Station + list[0].Sort);

                string date = model.Backfield1.Split(',')[0];
                string strdate = string.Format("{0}-{1}-{2}", date.Split('-')[0], date.Split('-')[1], "01");
                DateTime dt = Convert.ToDateTime(strdate);
                int day = DateTime.DaysInMonth(dt.Year, dt.Month) + 1;
                int numX = 2;
                for (int i = 1; i < day; i++)
                {
                    string newdate = string.Format("{0}-{1}-{2}", date.Split('-')[0], date.Split('-')[1], i.ToString().PadLeft(2, '0'));
                    DateTime dateTime = Convert.ToDateTime(newdate);
                    var dataList = list.Where(t => t.SDate == dateTime).OrderBy(t => t.ShiftClass).ToList();

                    var newWhiteList = dataList.Where(t => t.ShiftClass.Contains("白")).ToList();
                    var newBlackList = dataList.Where(t => t.ShiftClass.Contains("夜")).ToList();

                    for (int m = 0; m < newWhiteList.Count; m++)
                    {
                        ICell cell4 = sheet.GetRow(8 + m).GetCell(numX);
                        double qty = Convert.ToDouble(newWhiteList[m].SValue);
                        cell4.SetCellValue(qty);
                    }
                    for (int m = 0; m < newBlackList.Count; m++)
                    {
                        ICell cell5 = sheet.GetRow(8 + m).GetCell(numX + 1);
                        double qty = Convert.ToDouble(newBlackList[m].SValue);
                        cell5.SetCellValue(qty);

                    }
                    numX += 2;
                }

                using (var stream = new FileStream(exprotPath, FileMode.Create, FileAccess.Write))
                {
                    workbook.Write(stream);
                }

                string dicPath = $"Upload/Excel/";
                string newName = System.IO.Path.GetFileName(exprotPath);
                return Content(dicPath + newName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return default;
            }

        }
    }
}

