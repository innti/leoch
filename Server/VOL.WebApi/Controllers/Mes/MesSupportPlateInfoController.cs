/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesSupportPlateInfoController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using VOL.Core.NPOI;
using System.IO;
using NPOI.XSSF.UserModel;
using Microsoft.AspNetCore.Authorization;
using VOL.Entity.DomainModels;
using NPOI.SS.UserModel;
using System;
using System.Threading.Tasks;
using System.Linq;
using VOL.Mes.Dtos;
using System.Collections.Generic;

namespace Vol.Mes.Controllers
{
    [Route("api/MesSupportPlateInfo")]
    [PermissionTable(Name = "MesSupportPlateInfo")]
    public partial class MesSupportPlateInfoController : ApiBaseController<IMesSupportPlateInfoService>
    {
        public MesSupportPlateInfoController(IMesSupportPlateInfoService service, AOIRowsExport aOIRowsExport)
        : base(service)
        {

        }
        NPOIReport nPOIReport = new NPOIReport();
        [HttpPost, Route("ExcelReport"), AllowAnonymous]
        public async Task<IActionResult> ExcelReport([FromBody] MesSupportPlateInfo model)
        {
            try
            {
                string exprotPath = "";

                if (model.Type.Contains("耗银量"))
                {
                    exprotPath = nPOIReport.GetReportFile("SPC印刷耗银量.xlsx");
                }
                else
                {
                    exprotPath = nPOIReport.GetReportFile("SPCPVDCU方阻.xlsx");

                }
                FileStream file = new FileStream(exprotPath, FileMode.Open, FileAccess.Read);
                var workbook = new XSSFWorkbook(file);
                var sheet = workbook.GetSheetAt(0);
                //查询数据
                var list = await Service.ExcelReport(model);

                ICell cell2 = sheet.GetRow(2).GetCell(2);
                cell2.SetCellValue("PVD" + list[0].Line);
                ICell cell3 = sheet.GetRow(4).GetCell(2);
                cell3.SetCellValue(list[0].Station + list[0].Type + list[0].AorB);
                string date = model.Backfield1.Split(',')[0];
                string strdate = string.Format("{0}-{1}-{2}", date.Split('-')[0], date.Split('-')[1], "01");
                DateTime dt = Convert.ToDateTime(strdate);
                int day = DateTime.DaysInMonth(dt.Year, dt.Month) + 1;
                int numX = 2;
                for (int i = 1; i < day; i++)
                {
                    string newdate = string.Format("{0}-{1}-{2}", date.Split('-')[0], date.Split('-')[1], i.ToString().PadLeft(2, '0'));
                    DateTime dateTime = Convert.ToDateTime(newdate);
                    var dataList = list.Where(t => t.Dates == dateTime).OrderBy(t => t.ShiftClass).ToList();

                    var newWhiteList = dataList.Where(t => t.ShiftClass.Contains("白")).ToList();
                    var newBlackList = dataList.Where(t => t.ShiftClass.Contains("夜")).ToList();

                    for (int m = 0; m < newWhiteList.Count; m++)
                    {
                        ICell cell4 = sheet.GetRow(8 + m).GetCell(numX);
                        double qty = Convert.ToDouble(newWhiteList[m].Nvalue);
                        cell4.SetCellValue(qty);
                    }
                    for (int m = 0; m < newBlackList.Count; m++)
                    {
                        ICell cell5 = sheet.GetRow(8 + m).GetCell(numX + 1);
                        double qty = Convert.ToDouble(newBlackList[m].Nvalue);
                        cell5.SetCellValue(qty);

                    }
                    numX += 2;
                }

                using (var stream = new FileStream(exprotPath, FileMode.Create, FileAccess.Write))
                {
                    workbook.Write(stream);
                }

                string dicPath = $"Upload/Excel/";
                string newName = System.IO.Path.GetFileName(exprotPath);
                return Content(dicPath + newName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return default;
            }

        }

        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] MesSupportPlateInfo model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();

            var listdattime = list.Select(t => t.Dates).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            List<double> array = new List<double>();
            List<MesseriesData> listanew = new List<MesseriesData>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key.ToString("yyyy-MM-dd"));
                var listvalue = list.Where(t => t.Dates == item.Key).ToList();
                double AGValue = 0;
                foreach (var itemValue in listvalue)
                {
                    AGValue += Convert.ToDouble(itemValue.Nvalue == string.Empty ? "0" : itemValue.Nvalue);
                }

                array.Add(AGValue);

            }
            MesseriesData messeriesData = new MesseriesData();

            messeriesData.data = array.ToArray();

            listanew.Add(messeriesData);

            mesChartModel.MesseriesDatas = listanew;
            mesChartModel.MesxAxisData = Xday.ToArray();

            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {

            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; } = "耗银量";
            public string type { get; set; } = "line";
            public string stack { get; set; } = "Total";

            public AreaStyle areaStyle { get; set; } = new AreaStyle();
            public emphasis focus { get; set; } = new emphasis();
            public double[] data { get; set; }
        }
        public partial class AreaStyle
        {

        }

    }
}

