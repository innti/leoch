﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Vol.Mes.IServices;
using Vol.Mes.IServices.MesProduct;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Controllers
{
    [Route("api/MesTestChart")]
    [PermissionTable(Name = "MesTestResults")]
    public class MesTestChartController : ApiBaseController<IMesTestChartService>
    {
        public MesTestChartController(IMesTestChartService service)
            : base(service)
        {
        }
        [HttpPost, Route("ExcelReport"), AllowAnonymous]
        public async Task<IActionResult> ExcelReport([FromBody] MesTestResults model)
        {
            //查询数据
            var list = await Service.ExcelReport(model);
            return Json(list);
        }
    }
}
