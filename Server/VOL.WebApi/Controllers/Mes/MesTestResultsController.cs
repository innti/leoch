/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesTestResultsController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using VOL.Entity.DomainModels;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace Vol.Mes.Controllers
{
    [Route("api/MesTestResults")]
    [PermissionTable(Name = "MesTestResults")]
    public partial class MesTestResultsController : ApiBaseController<IMesTestResultsService>
    {
        public MesTestResultsController(IMesTestResultsService service)
        : base(service)
        {
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] MesTestResults model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();

            var listdattime = list.Select(t => t.Code).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
            }
            dynamic obj = null;
            switch (model.Backfield2)
            {
                case "ISC中位数":
                    obj = list.Select(t => t.MedISC).ToList();
                    break;
                case "FF中位数":
                    obj = list.Select(t => t.MedFF).ToList();
                    break;
                case "ETA中位数":
                    obj = list.Select(t => t.MedEta).ToList();
                    break;
                case "UOC中位数":
                    obj = list.Select(t => t.MedVoc).ToList();
                    break;
                case "Rs":
                    obj = list.Select(t => t.MedRs).ToList();
                    break;
                case "Rsh":
                    obj = list.Select(t => t.MedRsh).ToList();
                    break;
                case "EL":
                    obj = list.Select(t => t.ELClassA).ToList();
                    break;
                case "OPticalA":
                    obj = list.Select(t => t.OpticalA).ToList();
                    break;
                case "FF":
                    obj = list.Select(t => t.MedFF).ToList();
                    break;
                case "Isc效率c":
                    obj = list.Select(t => t.ADLXLC).ToList();
                    break;
                case "Isc效率b":
                    obj = list.Select(t => t.ADLXLB).ToList();
                    break;
                case "ADL":
                    obj = list.Select(t => t.ADL1).ToList();
                    break;
                default:
                    obj = list.Select(t => t.MedISC).ToList();
                    break;
            }
            List<string> array = new List<string>();
            foreach (var item in obj)
            {
                array.Add(item);
            }

            List<MesseriesData> listanew = new List<MesseriesData>();
            MesseriesData messeriesData = new MesseriesData();
            messeriesData.name = model.Backfield2;
            messeriesData.data = array.ToArray();

            listanew.Add(messeriesData);

            mesChartModel.MesseriesDatas = listanew;
            mesChartModel.MesxAxisData = Xday.ToArray();

            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {

            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; }
            public string type { get; set; } = "line";

            public string[] data { get; set; }
        }
    }
}

