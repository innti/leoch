/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesWareInputDetilsController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
namespace Vol.Mes.Controllers
{
    [Route("api/MesWareInputDetils")]
    [PermissionTable(Name = "MesWareInputDetils")]
    public partial class MesWareInputDetilsController : ApiBaseController<IMesWareInputDetilsService>
    {
        public MesWareInputDetilsController(IMesWareInputDetilsService service)
        : base(service)
        {
        }
    }
}

