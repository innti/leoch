/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesWareTakeOverbaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using VOL.Core.Response;
using System.Threading.Tasks;

namespace Vol.Mes.Controllers
{
    [Route("api/MesWareTakeOverbase")]
    [PermissionTable(Name = "MesWareTakeOverbase")]
    public partial class MesWareTakeOverbaseController : ApiBaseController<IMesWareTakeOverbaseService>
    {
        public MesWareTakeOverbaseController(IMesWareTakeOverbaseService service)
        : base(service)
        {

        }
        [HttpGet, Route("SearchBoxNumAsync"), AllowAnonymous]
        public async Task<TableData> SearchBoxNumAsync(string Code, string Box)
        {
            var result = new TableData();
            //查询数据
            var list = await Service.SearchBoxNum(Code, Box);
            if (list.Count > 0)
            {
                result.code = 200;
                result.data = list;
                result.count = list.Count;

            }
            else
            {
                result.code = 0;
                result.count = 0;
            }

            return result;

        }

    }
}

