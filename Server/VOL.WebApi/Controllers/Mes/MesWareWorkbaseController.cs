/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesWareWorkbaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using VOL.Core.Response;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Controllers
{
    [Route("api/MesWareWorkbase")]
    [PermissionTable(Name = "MesWareWorkbase")]
    public partial class MesWareWorkbaseController : ApiBaseController<IMesWareWorkbaseService>
    {
        public MesWareWorkbaseController(IMesWareWorkbaseService service)
        : base(service)
        {
        }
        [HttpGet, Route("SearchWorkNo"), AllowAnonymous]
        public async Task<TableData> SearchWorkNo(string  WorkerNo,string sort)
        {
            var result = new TableData();
            //查询数据
            var list = await Service.SearchWorkerNo(WorkerNo,sort);

            if (list.Count > 0)
            {
                result.code = 200;
                result.data = list;
                result.count = list.Count;

            }
            else
            {
                result.code = 0;
                result.count = 0;
            }

            return result;
        }
        [HttpPost, Route("InsertWareHouse"), AllowAnonymous]
        public  TableData InsertWareHouse([FromBody]MesWareInputDetils model)
        {
            var result = new TableData();
         
            //查询数据
            var flag = Service.InsertWareHouse(model);
            if (flag)
            {
                result.code = 200;
            }
            else
            {
                result.code = 0;
                result.count = 0;
            }

            return result;
        }
        [HttpPost, Route("OutWareWareHouse"), AllowAnonymous]
        public TableData OutWareWareHouse([FromBody] MesWareInputDetils model)
        {
            var result = new TableData();

            //查询数据
            var flag = Service.OutWareWareHouse(model);
            if (flag)
            {
                result.code = 200;
            }
            else
            {
                result.code = 0;
                result.count = 0;
            }

            return result;
        }
    }
}

