/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesdeviceMaintainanceController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Vol.Mes.Controllers
{
    [Route("api/MesdeviceMaintainance")]
    [PermissionTable(Name = "MesdeviceMaintainance")]
    public partial class MesdeviceMaintainanceController : ApiBaseController<IMesdeviceMaintainanceService>
    {
        public MesdeviceMaintainanceController(IMesdeviceMaintainanceService service)
        : base(service)
        {
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart()
        {
            //查询数据
            var list = await Service.ExcelChart();

            List<DeviceModel> mesChartModel = new List<DeviceModel>();
            foreach (var item in list)
            {
                DeviceModel deviceModel = new DeviceModel();
                deviceModel.DeviceName = item.DeviceName;
                deviceModel.DeviceTypeName= item.DeviceTypeName;
                deviceModel.Sort = item.Sort;
                deviceModel.LineName = item.LineName;
                switch (item.Status)
                {
                    case "计划保养":
                        deviceModel.Status1 = "";
                        deviceModel.Status2 = "0";
                        break;
                    case "计划维护":
                        deviceModel.Status1 ="";
                        deviceModel.Status3 = "0";
                        break;
                    case "应急保养":
                        deviceModel.Status1 = "";
                        deviceModel.Status4 = "0";
                        break;
                    case "应急维护":
                        deviceModel.Status1 = "";
                        deviceModel.Status5 = "0";
                        break;
                    case "缺料暂停":
                        deviceModel.Status1 = "";
                        deviceModel.Status6 = "0";
                        break;
                    case "其他原因":
                        deviceModel.Status1 = "";
                        deviceModel.Status7 = "0";
                        break;
              
                    default:
                        break;
                }
                mesChartModel.Add(deviceModel);
            }

            return Json(mesChartModel);
        }
        public partial class DeviceModel
        {
            public string Sort { get; set; }
            public string LineName { get; set; }
            public string DeviceName { get; set; } 
            public string DeviceTypeName { get; set; }
            public string Status1 { get; set; } = "0";
            public string Status2 { get; set; } = "";
            public string Status3 { get; set; } = "";
            public string Status4 { get; set; } = "";
            public string Status5 { get; set; } = "";
            public string Status6 { get; set; } = "";
            public string Status7 { get; set; } = "";

        }
    }
}

