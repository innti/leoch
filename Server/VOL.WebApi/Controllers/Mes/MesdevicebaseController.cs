/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesdevicebaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
namespace Vol.Mes.Controllers
{
    [Route("api/Mesdevicebase")]
    [PermissionTable(Name = "Mesdevicebase")]
    public partial class MesdevicebaseController : ApiBaseController<IMesdevicebaseService>
    {
        public MesdevicebaseController(IMesdevicebaseService service)
        : base(service)
        {
        }
    }
}

