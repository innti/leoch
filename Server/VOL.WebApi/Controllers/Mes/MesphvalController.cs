/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesphvalController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
namespace Vol.Mes.Controllers
{
    [Route("api/Mesphval")]
    [PermissionTable(Name = "Mesphval")]
    public partial class MesphvalController : ApiBaseController<IMesphvalService>
    {
        public MesphvalController(IMesphvalService service)
        : base(service)
        {
        }
    }
}

