/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesspotchecbaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using VOL.Core.Filters;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System;
using VOL.Core.Enums;

namespace Vol.Mes.Controllers
{
    [Route("api/Messpotchecbase")]
    [PermissionTable(Name = "Messpotchecbase")]
    public partial class MesspotchecbaseController : ApiBaseController<IMesspotchecbaseService>
    {
        public MesspotchecbaseController(IMesspotchecbaseService service)
        : base(service)
        {
        }

        [HttpPost, Route("Import1")]
        [ApiActionPermission(ActionPermissionOptions.Import)]
        public ActionResult Import1(List<IFormFile> fileInput)
        {
            return Json(Service.Import(fileInput));
        }
    }
}

