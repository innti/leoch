/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹MesusersbaseController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
namespace Vol.Mes.Controllers
{
    [Route("api/Mesusersbase")]
    [PermissionTable(Name = "Mesusersbase")]
    public partial class MesusersbaseController : ApiBaseController<IMesusersbaseService>
    {
        public MesusersbaseController(IMesusersbaseService service)
        : base(service)
        {
        }
    }
}

