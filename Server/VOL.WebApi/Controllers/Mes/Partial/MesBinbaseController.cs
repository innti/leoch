/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("MesBinbase",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using VOL.Core.Response;
using Vol.Mes.Dtos.Query;
using Vol.Mes.Repositories;
using System.Linq;

namespace Vol.Mes.Controllers
{
    public partial class MesBinbaseController
    {
        private readonly IMesBinbaseService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        [ActivatorUtilitiesConstructor]
        public MesBinbaseController(
            IMesBinbaseService service,
            IHttpContextAccessor httpContextAccessor
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet, Route("Load"), AllowAnonymous]
        public TableData GetList(QueryMesBinbaseListReq request)
        {
            var result = new TableData();
            var objs = MesBinbaseRepository.Instance.FindQuery(e => true);
            if (!string.IsNullOrEmpty(request.key))
            if (!string.IsNullOrEmpty(request.BinEff))
            {
                objs = objs.Where(u => u.BinEff.Contains(request.BinEff));
            }
            result.data = objs.OrderBy(u => u.Id).OrderByDescending(e => e.CreateTime)
                //.Skip((request.page - 1) * request.limit)
                //.Take(request.limit)
                .ToList();
            result.count = objs.Count();
            return result;
        }
    }
}
