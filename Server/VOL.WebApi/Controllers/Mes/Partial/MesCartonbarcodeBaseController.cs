/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("MesCartonbarcodeBase",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using Vol.Mes.IServices;
using VOL.Core.Response;
using Vol.Mes.Dtos.Query;
using Vol.Mes.Repositories;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace Vol.Mes.Controllers
{
    public partial class MesCartonbarcodeBaseController
    {
        private readonly IMesCartonbarcodeBaseService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        [ActivatorUtilitiesConstructor]
        public MesCartonbarcodeBaseController(
            IMesCartonbarcodeBaseService service,
            IHttpContextAccessor httpContextAccessor
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet, Route("Load"), AllowAnonymous]
        public TableData GetList(QueryMesCartonbarcodeBaseListReq request)
        {
            var result = new TableData();
            var objs = MesCartonbarcodeBaseRepository.Instance.FindQuery(e => true);
            if (!string.IsNullOrEmpty(request.CodeBM))
            {
                objs = objs.Where(u => u.CodeBM.Contains(request.CodeBM));
            }
            if (request.IsDel == 1)
            {
                objs = objs.Where(u => u.IsDel == 1);
            }
            else
            {
                objs = objs.Where(u => u.IsDel == 0);
            }
            if (!string.IsNullOrEmpty(request.CreateDate))
            {
                objs = objs.Where(u => u.CreateTime >= Convert.ToDateTime(request.CreateDate + " 00:00:00"));
            }

            if (!string.IsNullOrEmpty(request.EndDate))
            {
                objs = objs.Where(u => u.CreateTime <= Convert.ToDateTime(request.EndDate + " 23:59:59"));
            }
            if (!string.IsNullOrWhiteSpace(request.SearchType) && request.SearchType == "内包装条码" && !string.IsNullOrEmpty(request.SearchValue))
            {
                objs = objs.Where(u => u.CodeBM.Contains(request.SearchValue));
            }
            if (!string.IsNullOrWhiteSpace(request.SearchType) && request.SearchType == "class" && !string.IsNullOrEmpty(request.SearchValue))
            {
                objs = objs.Where(u => u.CodeClass.Contains(request.SearchValue));
            }
            result.data = objs.OrderBy(u => u.Id).OrderByDescending(e => e.CreateTime)
                //.Skip((request.page - 1) * request.limit)
                //.Take(request.limit)
                .ToList();
            result.count = objs.Count();
            return result;
        }
    }
}
