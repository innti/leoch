/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("MesCartonbarcode",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using Vol.Mes.IServices;
using VOL.Core.Response;
using Vol.Mes.Dtos.Query;
using Vol.Mes.Repositories;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace Vol.Mes.Controllers
{
    public partial class MesCartonbarcodeController
    {
        private readonly IMesCartonbarcodeService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        [ActivatorUtilitiesConstructor]
        public MesCartonbarcodeController(
            IMesCartonbarcodeService service,
            IHttpContextAccessor httpContextAccessor
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet, Route("Load"), AllowAnonymous]
        public TableData GetList([FromQuery]QueryMesCartonbarcodeListReq request)
        {
            var result = new TableData();
            var objs = MesCartonbarcodeRepository.Instance.FindQuery(e => true);
            if (!string.IsNullOrEmpty(request.CodeBM))
            {
                objs = objs.Where(u => u.CodeBM.Contains(request.CodeBM));
            }
            if (!string.IsNullOrEmpty(request.CreateDate))
            {
                objs = objs.Where(u => u.CreateTime >= Convert.ToDateTime(request.CreateDate + " 00:00:00"));
            }

            if (!string.IsNullOrEmpty(request.EndDate))
            {
                objs = objs.Where(u => u.CreateTime <= Convert.ToDateTime(request.EndDate + " 23:59:59"));
            }
            if (!string.IsNullOrWhiteSpace(request.SearchType) && request.SearchType == "外箱条码" && !string.IsNullOrEmpty(request.SearchValue))
            {
                objs = objs.Where(u => u.CodeBM.Contains(request.SearchValue));
            }
            if (!string.IsNullOrWhiteSpace(request.SearchType) && request.SearchType == "class" && !string.IsNullOrEmpty(request.SearchValue))
            {
                objs = objs.Where(u => u.CodeClass.Contains(request.SearchValue));
            }
            result.data = objs.OrderBy(u => u.Id).OrderByDescending(e => e.CreateTime)
                //.Skip((request.page - 1) * request.limit)
                //.Take(request.limit)
                .ToList();
            result.count = objs.Count();
            return result;
        }
    }
}
