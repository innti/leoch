/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("MesChemicalBase",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using System.Linq;

namespace Vol.Mes.Controllers
{
    public partial class MesChemicalBaseController
    {
        private readonly IMesChemicalBaseService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        [ActivatorUtilitiesConstructor]
        public MesChemicalBaseController(
            IMesChemicalBaseService service,
            IHttpContextAccessor httpContextAccessor
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] MesChemicalBase model)
        {
            //查询数据
            var list = await Service.ExcelChart(model.LineName, model.Backfield1);

            MesChartModel mesChartModel = new MesChartModel();

            List<string> meslegenddataArr = new List<string>();
            var legenddata = list.Select(t => t.Chemical).GroupBy(t => t).ToArray();
            foreach (var item in legenddata)
            {
                meslegenddataArr.Add(item.Key);
            }
            mesChartModel.MeslegendData = meslegenddataArr.ToArray();

            List<MesseriesData> messeriesDatas = new List<MesseriesData>();


            list.ToList().ForEach(i => i.Date = Convert.ToDateTime(i.Date.ToString()).ToString("yyyy-MM-dd"));
         
            var listdattime = list.Select(t => t.Date).GroupBy(t => t).ToList();

            List<string> Xday = new List<string>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
            }

            foreach (var item in meslegenddataArr)
            {
                List<double> dayValue = new List<double>();
                foreach (var strdate in Xday)
                {
                    var datevalue = list.Where(t => t.Chemical == item && t.Date.Contains(strdate)).Select(t => t.Value).ToList();
                    if (datevalue.Count <= 0)
                    {
                        dayValue.Add(0);
                    }
                    else
                    {
                        List<double> li = datevalue.ConvertAll<double>(i => double.Parse(i));
                        var sumValue = li.Sum(i => i);
                        dayValue.Add(sumValue);
                    }
                }
                MesseriesData messeriesData = new MesseriesData();
                messeriesData.name = item;
                messeriesData.data = dayValue.ToArray();
                messeriesDatas.Add(messeriesData);
            }
            mesChartModel.MesxAxisData = Xday.ToArray();
            mesChartModel.MesseriesDatas = messeriesDatas;
            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {
            public string[] MeslegendData { get; set; }

            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }

        public partial class MesseriesData
        {
            public string name { get; set; }
            public string type { get; set; } = "line";
            public string stack { get; set; } = "Total";
            public double[] data { get; set; }
        }


    }
}
