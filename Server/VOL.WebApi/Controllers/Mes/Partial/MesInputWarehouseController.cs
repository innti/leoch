/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("MesInputWarehouse",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using Vol.Mes.IServices;
using VOL.Core.Response;
using Microsoft.AspNetCore.Authorization;
using Vol.Mes.Repositories;
using System.Linq;
using Vol.Mes.Dtos.Query;
using VOL.Core.Request;
using VOL.Core;
using VOL.Core.ManageUser;
using VOL.Core.BaseProvider;

namespace Vol.Mes.Controllers
{
    public partial class MesInputWarehouseController
    {
        private readonly IMesInputWarehouseService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IMesCartonbarcodeBaseService _cartonbarcodeBase;
        private readonly IMesCartonbarcodeService _cartonbarcode;
        [ActivatorUtilitiesConstructor]
        public MesInputWarehouseController(
            IMesInputWarehouseService service,
            IHttpContextAccessor httpContextAccessor,
            IMesCartonbarcodeBaseService cartonbarcodeBase,
            IMesCartonbarcodeService cartonbarcode
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
            _cartonbarcodeBase = cartonbarcodeBase;
            _cartonbarcode = cartonbarcode;
        }

        [HttpGet, Route("Load"), AllowAnonymous]
        public TableData GetList(QueryMesInputWarehouseListReq request)
        {
            var result = new TableData();

            var objs = MesInputWarehouseRepository.Instance.FindQuery(e => true)
                .WhereIf(!string.IsNullOrEmpty(request.CodeBM), u => u.CodeBM.Contains(request.CodeBM))
                .WhereIf(!string.IsNullOrEmpty(request.CodeCode), u => u.CodeCode.Contains(request.CodeCode));

            if (!string.IsNullOrEmpty(request.CreateDate))
            {
                objs = objs.Where(u => u.CreateTime >= Convert.ToDateTime(request.CreateDate + " 00:00:00"));
            }

            if (!string.IsNullOrEmpty(request.EndDate))
            {
                objs = objs.Where(u => u.CreateTime <= Convert.ToDateTime(request.EndDate + " 23:59:59"));
            }
            if (!string.IsNullOrEmpty(request.CreateOutDate))
            {
                objs = objs.Where(u => u.UpdateTime >= Convert.ToDateTime(request.CreateDate + " 00:00:00"));
            }

            if (!string.IsNullOrEmpty(request.EndOutDate))
            {
                objs = objs.Where(u => u.UpdateTime <= Convert.ToDateTime(request.EndDate + " 23:59:59"));
            }


            result.data = objs.OrderBy(u => u.Id).OrderByDescending(e => e.CreateTime)
                .Skip((request.page - 1) * request.limit)
                .Take(request.limit)
                .ToList();
            result.count = objs.Count();
            return result;
        }
        [HttpGet, Route("LoadSearch"), AllowAnonymous]
        public TableData GetSearchList(string CodeBM, string Code, string UserName, string Date, string Sort, int Status)
        {
            var result = new TableData();

            var objs = MesInputWarehouseRepository.Instance.FindQuery(e => true);
            if (!string.IsNullOrEmpty(CodeBM))
            {
                objs = objs.Where(u => u.CodeBM.Contains(CodeBM));
            }
            if (!string.IsNullOrEmpty(Code))
            {
                objs = objs.Where(u => u.CodeCode.Contains(Code));
            }
            if (!string.IsNullOrEmpty(Date))
            {
                objs = objs.Where(u => u.CreateTime >= Convert.ToDateTime(Date + " 00:00:00"));
            }

            if (!string.IsNullOrEmpty(Date))
            {
                objs = objs.Where(u => u.CreateTime <= Convert.ToDateTime(Date + " 23:59:59"));
            }

            if (!string.IsNullOrEmpty(UserName))
            {
                objs = objs.Where(u => u.CreateUserName.Contains(UserName));
            }
            if (!string.IsNullOrEmpty(Sort))
            {
                objs = objs.Where(u => u.WareSort.Contains(Sort));
            }
            objs = objs.Where(u => u.WareStatus == Status);

            result.data = objs.ToList();
            result.count = objs.Count();
            return result;
        }
        /// <summary>
        /// 数据插入 --入库操作  
        /// 1 数据插入 MesInputWarehouse 
        /// 2 改变 MesCartonbarcodeBase 【isdel=1，CodeType='传值CodeBM'】
        /// 3 判断是否改变 MesCartonbarcode  isdel=传入值 0: 不改 1: 修改
        /// </summary>
        /// <param name="req"></param>
        [HttpPost, Route("AddTrans"), AllowAnonymous]
        public Response AddTrans([FromBody] InputWarehouseTerans req)
        {

            var result = new Response();
            try
            {
                ////1 数据插入 MesInputWarehouse
                _service.AddCached(req.addInputWarehouseReq);
                // _mesDistilsWarehouseApp.AddCached(req.addInputWarehouseReq);
                //2 改变 MesCartonbarcodeBase 【isdel=1，CodeType='传值CodeBM'】
                _cartonbarcodeBase.UpdateStatus(req.BarcodeBaseIsdel, req.BarcodeBaseCodeType, req.addInputWarehouseReq.CodeCode);
                // 3 判断是否改变 MesCartonbarcode  isdel = 传入值 0: 不改 1: 修改
                _cartonbarcode.UpdateStatus(req.BarcodeIsdel, req.BarcodeBaseCodeType);

            }
            catch (Exception ex)
            {
                result.code = 500;
                result.message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;



        }

        /// <summary>
        /// 数据插入 --入库操作  
        /// 1 数据插入 MesInputWarehouse 
        /// 2 改变 MesCartonbarcodeBase 【isdel=1，CodeType='传值CodeBM'】
        /// 3 判断是否改变 MesCartonbarcode  isdel=传入值 0: 不改 1: 修改
        /// </summary>
        /// <param name="req"></param>
        [HttpPost, Route("OutTrans"), AllowAnonymous]
        public Response OutTrans([FromBody] InputWarehouseTerans req)
        {
            var result = new Response();
            try
            {
                //1 数据插入 MesInputWarehouse
                _service.OutUpdate(req.addInputWarehouseReq, req.CodeType);
                if (req.CodeType == "箱")
                {
                    var boxlist = MesInputWarehouseRepository.Instance.FindAsIQueryable(e => e.CodeBM == req.addInputWarehouseReq.CodeBM).ToList();
                    foreach (var list in boxlist)
                    {
                        AddOrUpdateMesInputWarehouseReq addlist = new AddOrUpdateMesInputWarehouseReq()
                        {
                            CodeBM = list.CodeBM,
                            CodeType = list.CodeType,
                            CodeSpec = list.CodeSpec,
                            CodeCode = list.CodeCode,
                            CodeClass = list.CodeClass,
                            CodePmax = list.CodePmax,
                            CodeQty = list.CodeQty,
                            CodeNMB = list.CodeNMB,
                            CodeDate = list.CodeDate,
                            CodeGW = list.CodeGW,
                            CodeRoom = list.CodeRoom,
                            Codetime = list.Codetime,
                            CodePosition = list.CodePosition,
                            CodeTMSpec = list.CodeTMSpec,
                            CodeNum = list.CodeNum,
                            CreateTime = DateTime.Now,
                            CreateUserName =  UserContext.Current.UserTrueName,
                            CreateUserId = UserContext.Current.UserId.ToString(),
                            WareStatus = 1,
                            WareSort = "出库",
                        };
                        _service.AddCached(addlist);

                    }
                }
                else
                {
                    _service.AddCached(req.addInputWarehouseReq);
                    // _mesDistilsWarehouseApp.AddCached();
                }
                // 查询今日修改数据
        
            }
            catch (Exception ex)
            {
                result.code = 500;
                result.message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

    }
}
