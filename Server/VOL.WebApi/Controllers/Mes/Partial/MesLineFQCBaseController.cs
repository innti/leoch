/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("MesLineFQCBase",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using System.Linq;

namespace Vol.Mes.Controllers
{
    public partial class MesLineFQCBaseController
    {
        private readonly IMesLineFQCBaseService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        [ActivatorUtilitiesConstructor]
        public MesLineFQCBaseController(
            IMesLineFQCBaseService service,
            IHttpContextAccessor httpContextAccessor
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] MesLineFQCBase model)
        {
            //查询数据
            var list = await Service.ExcelChart(model);
            MesChartModel mesChartModel = new MesChartModel();
            var listbadName = list.Select(t =>t.Sort).GroupBy(t => t).ToArray();
            List<string> arrBadName = new List<string>();

            List<MesFQCValue> MesFQCValue = new List<MesFQCValue>();
            foreach (var item in listbadName)
            {
                arrBadName.Add(item.Key);
                //开始形成X轴的值
                MesFQCValue mesVale = new MesFQCValue();
                mesVale.name = item.Key;

                List<double> strValue = new List<double>();
                var listDataValue = list.Where(t => t.Sort == item.Key).ToList();
                foreach (var itemvalue in listDataValue)
                {
                    double arrvalue = Math.Round(Convert.ToDouble(itemvalue.SValue==String.Empty?"0": itemvalue.SValue) / Convert.ToDouble(itemvalue.ELCount), 4);
                    strValue.Add(arrvalue);
                }
                mesVale.data = strValue.ToArray();
                MesFQCValue.Add(mesVale);
            }
            mesChartModel.mesFQCValue = MesFQCValue;
            MesFQCBadName mesFQCBad = new MesFQCBadName();
            mesFQCBad.ChartBadName = arrBadName.ToArray();
            mesChartModel.MesFQCBad = mesFQCBad;

            var listX = list.Select(t => t.Billno).GroupBy(t => t).ToArray();
            YAxisValue yAxisvalue = new YAxisValue();
            List<double> arrY = new List<double>();
            List<string> arrX = new List<string>();
            foreach (var item in listX)
            {
                arrX.Add(item.Key);
                var listY = list.Where(t=>t.Billno== item.Key).FirstOrDefault();
                double arrvalue = Math.Round(Convert.ToDouble(listY.ELCount == String.Empty ? "0" : listY.ELCount) / Convert.ToDouble(listY.TestCount), 4);
                arrY.Add(arrvalue*100.0);
            }
            yAxisvalue.data = arrY.ToArray();

            MesFQCXmodel mesFQCXmodel = new MesFQCXmodel();
            mesFQCXmodel.ChartX = arrX.ToArray();
            mesChartModel.mesFQCXmodel = mesFQCXmodel;

            mesChartModel.yAxisValue = yAxisvalue;
            return Json(mesChartModel);
        }
    }
    public partial class MesChartModel
    {
        public MesFQCXmodel mesFQCXmodel { get; set; }
        public YAxisValue yAxisValue { get; set; } = new YAxisValue();
        public List<MesFQCValue> mesFQCValue { get; set; }
        public MesFQCBadName MesFQCBad { get; set; }

    }
    public class MesFQCXmodel
    {
        public string[] ChartX { get; set; }
    }
    public partial class MesFQCValue
    {
        public string name { get; set; }
        public string type { get; set; } = "bar";
        public string stack { get; set; } = "不良";
        public double[] data { get; set; }
        public emphasis focus { get; set; }=new emphasis ();
    }

    public partial class YAxisValue
    {
        public string name { get; set; } = "直通率";
        public string type { get; set; } = "line";
        public int yAxisIndex { get; set; } = 1;
        public string[] color { get; set; } = new string[1] { "#3398DB" };
        public double[] data { get; set; } = new double[4] { 98.0, 96.2, 96.3, 95.5 };
    }

    public partial class MesFQCBadName { public string[] ChartBadName { get; set; } }
    public partial class emphasis
    {
        public string focus { get; set; } = "series";

    }

}
