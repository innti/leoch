/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("MesPLDitls",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using VOL.Entity.DomainModels;
using Vol.Mes.IServices;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using VOL.Core.NPOI;

namespace Vol.Mes.Controllers
{
    public partial class MesPLDitlsController
    {
        private readonly IMesPLDitlsService _service;//访问业务代码
        private readonly IHttpContextAccessor _httpContextAccessor;

        [ActivatorUtilitiesConstructor]
        public MesPLDitlsController(
            IMesPLDitlsService service,
            IHttpContextAccessor httpContextAccessor
        )
        : base(service)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] MesPLDitls model)
        {
            //查询数据
            var list = await Service.ExcelChart(model.Title);

            MesChartModel mesChartModel = new MesChartModel();


            list.ToList().ForEach(i => i.date = Convert.ToDateTime(i.date.ToString()).ToString("yyyy-MM-dd"));

            var listdattime = list.Select(t => t.date).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
            }
            List<MesseriesData> listanew = new List<MesseriesData>();
            //不规则二维数组  
            List<double[]> array = new List<double[]>();
            int num = 0;
            foreach (var item in Xday)
            {  
            
                List<double> xValue = new List<double>();
                var datevalue = list.Where(t => t.date.Contains(item)).Select(t => t.Bin).ToList();
                foreach (var itemValue in datevalue)
                {
                    if (!string.IsNullOrEmpty(itemValue))
                    {
                        xValue.Add(Convert.ToDouble(itemValue));
                    }
                    else
                    {
                        xValue.Add(0);
                    }
                }
                array.Add(xValue.ToArray());
                num++;
         
            }
            MesseriesData messeriesData = new MesseriesData();
            messeriesData.data = array;
            listanew.Add(messeriesData);

            mesChartModel.MesseriesDatas = listanew;
            mesChartModel.MesxAxisData = Xday.ToArray();
    
            return Json(mesChartModel);
        }

        [HttpPost, Route("ExcelPLbigChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelPLbigChart([FromBody] MesPLDitls model)
        {
            //查询数据
            var list = await Service.ExcelChart(model.Title);

            MesChartModelchar mesChartModel = new MesChartModelchar();
            list.ToList().ForEach(i => i.date = Convert.ToDateTime(i.date.ToString()).ToString("yyyy-MM-dd"));
           
          // list = list.Where(t => t.IsDel == model.IsDel).ToList();

            var listdattime = list.Select(t => t.date).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
            }

            List<MesseriesDatachar> listData = new List<MesseriesDatachar>();
            var listLegendData = NPOIReport.ReturnListPLbad();
            var listlist = NPOIReport.ReturnListPLbadnum();
            var numdb = 0;
            foreach (var item in listLegendData)
            {
                List<int> Yday = new List<int>();
                foreach (var itemday in Xday)
                {
                    var listcount = list.Where(t => t.date == itemday && t.Scratch == numdb.ToString()).Count();
                    if (listcount > 0)
                    {
                        Yday.Add(listcount);
                    }
                }
                MesseriesDatachar messeriesData = new MesseriesDatachar();
                messeriesData.name = item;
                messeriesData.data = Yday.ToArray();
                listData.Add(messeriesData);
                numdb++;
            }
            mesChartModel.MesLegendData = listLegendData.ToArray();
            mesChartModel.MesxAxisData = Xday.ToArray();
            mesChartModel.MesseriesDatas = listData;
            return Json(mesChartModel);
        }

        public partial class MesChartModelchar
        {
            public string[] MesLegendData { get; set; }
            public string[] MesxAxisData { get; set; }
            public List<MesseriesDatachar> MesseriesDatas { get; set; }

        }
        public partial class MesseriesDatachar
        {
            public string name { get; set; }
            public string type { get; set; } = "bar";
            public int barGap { get; set; } = 0;
            public emphasis Emphasis { get; set; } = new emphasis();
            public int[] data { get; set; }
        }

        public partial class MesChartModel
        {

            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string type { get; set; } = "candlestick";

            public List<double[]> data { get; set; }
        }
    }
}
