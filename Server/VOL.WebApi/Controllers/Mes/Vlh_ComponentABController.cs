/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹Vlh_ComponentABController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using VOL.Entity.DomainModels;
using System.Linq;

namespace Vol.Mes.Controllers
{
    [Route("api/Vlh_ComponentAB")]
    [PermissionTable(Name = "Vlh_ComponentAB")]
    public partial class Vlh_ComponentABController : ApiBaseController<IVlh_ComponentABService>
    {
        public Vlh_ComponentABController(IVlh_ComponentABService service)
        : base(service)
        {
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] Vlh_ComponentAB model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();

            List<string> Xday = new List<string>();

            List<string> yday = new List<string>();
            MarkLine mark = new MarkLine();
            foreach (var item in list)
            {
                Xday.Add(item.CreationTime.ToString("yyyy-MM-dd HH:mm:ss"));

                List<MarkLineyAxis> MarkLineData = new List<MarkLineyAxis>();

                MarkLineyAxis markLiney = new MarkLineyAxis();
                markLiney.yAxis = item.SpecUp.ToString();
                MarkLineData.Add(markLiney);

                MarkLineyAxis markLiney1 = new MarkLineyAxis();
                markLiney1.yAxis = item.SpecLine.ToString();
                MarkLineData.Add(markLiney1);

                MarkLineyAxis markLiney2 = new MarkLineyAxis();
                markLiney2.yAxis = item.SpecLower.ToString();
                MarkLineData.Add(markLiney2);

                mark.data = MarkLineData;
                yday.Add(item.P3);
            }

            MesseriesData messeriesData = new MesseriesData();
            messeriesData.data = yday.ToArray();
            messeriesData.markLine = mark;

            mesChartModel.MesseriesDatas = messeriesData;
            mesChartModel.MesxAxisData = Xday.ToArray();

            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {
            public string[] MesxAxisData { get; set; }
            public MesseriesData MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; } = "AB胶比";
            public string type { get; set; } = "line";

            public string[] data { get; set; }
            public MarkLine markLine { get; set; }
        }
        public class MarkLine
        {
            public List<MarkLineyAxis> data { get; set; }
        }
        public class MarkLineyAxis
        {
            public string yAxis { get; set; }
        }
    }
}

