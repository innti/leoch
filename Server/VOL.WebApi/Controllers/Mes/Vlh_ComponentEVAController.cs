/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹Vlh_ComponentEVAController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Controllers
{
    [Route("api/Vlh_ComponentEVA")]
    [PermissionTable(Name = "Vlh_ComponentEVA")]
    public partial class Vlh_ComponentEVAController : ApiBaseController<IVlh_ComponentEVAService>
    {
        public Vlh_ComponentEVAController(IVlh_ComponentEVAService service)
        : base(service)
        {
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] Vlh_ComponentEVA model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();

            List<string> Xday = new List<string>();

            List<string> yday1 = new List<string>();
            List<string> yday2 = new List<string>();
            List<string> yday3 = new List<string>();
            List<string> yday4 = new List<string>();
            List<string> yday5 = new List<string>();
            List<string> yday6 = new List<string>();
            MarkLine mark = new MarkLine();
            foreach (var item in list)
            {
                Xday.Add(item.CreationTime.ToString("yyyy-MM-dd HH:mm:ss"));       
                List<MarkLineyAxis> MarkLineData = new List<MarkLineyAxis>();

                MarkLineyAxis markLiney = new MarkLineyAxis();
                markLiney.yAxis = item.SpecUp.ToString();
                MarkLineData.Add(markLiney);

                MarkLineyAxis markLiney1 = new MarkLineyAxis();
                markLiney1.yAxis = item.SpecLine.ToString();
                MarkLineData.Add(markLiney1);

                MarkLineyAxis markLiney2 = new MarkLineyAxis();
                markLiney2.yAxis = item.SpecLower.ToString();
                MarkLineData.Add(markLiney2);

                mark.data = MarkLineData;
                yday1.Add(item.W1.ToString());
                yday2.Add(item.W2.ToString());
                yday3.Add(item.W3.ToString());
                yday4.Add(item.W4.ToString());
                yday5.Add(item.W5.ToString());
                yday6.Add(item.W6.ToString());
            }

            List<MesseriesData> messeriesData = new List<MesseriesData>();

            MesseriesData messeries=new MesseriesData();
            messeries.data = yday1.ToArray();
            messeries.name = "EVA数据1";
            messeries.markLine = mark;
            messeriesData.Add(messeries);

            MesseriesData messeries1 = new MesseriesData();
            messeries1.data = yday2.ToArray();
            messeries1.name = "EVA数据2";
            messeries1.markLine = mark;
            messeriesData.Add(messeries1);

            MesseriesData messeries3 = new MesseriesData();
            messeries3.data = yday3.ToArray();
            messeries3.name = "EVA数据3";
            messeries3.markLine = mark;
            messeriesData.Add(messeries3);

            MesseriesData messeries4 = new MesseriesData();
            messeries4.data = yday4.ToArray();
            messeries4.name = "EVA数据4";
            messeries4.markLine = mark;
            messeriesData.Add(messeries4);

            MesseriesData messeries5 = new MesseriesData();
            messeries5.data = yday5.ToArray();
            messeries5.name = "EVA数据5";
            messeries5.markLine = mark;
            messeriesData.Add(messeries5);

            MesseriesData messeries6 = new MesseriesData();
            messeries6.data = yday6.ToArray();
            messeries6.name = "EVA数据6";
            messeries6.markLine = mark;
            messeriesData.Add(messeries6);

            mesChartModel.MesseriesDatas = messeriesData;
            mesChartModel.MesxAxisData = Xday.ToArray();

            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {
            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; } = "EVA数据";
            public string type { get; set; } = "line";

            public string[] data { get; set; }
            public MarkLine markLine { get; set; }
        }
        public class MarkLine
        {
            public List<MarkLineyAxis> data { get; set; }
        }
        public class MarkLineyAxis
        {
            public string yAxis { get; set; }
        }
    }
}

