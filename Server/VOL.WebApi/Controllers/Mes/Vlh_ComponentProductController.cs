/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹Vlh_ComponentProductController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using VOL.Entity.DomainModels;
using System.Linq;
using VOL.Core.NPOI;

namespace Vol.Mes.Controllers
{
    [Route("api/Vlh_ComponentProduct")]
    [PermissionTable(Name = "Vlh_ComponentProduct")]
    public partial class Vlh_ComponentProductController : ApiBaseController<IVlh_ComponentProductService>
    {
        public Vlh_ComponentProductController(IVlh_ComponentProductService service)
        : base(service)
        {
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] Vlh_ComponentProduct model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();



            list.ToList().ForEach(i => i.Backfield1 = Convert.ToDateTime(i.OperatorDate).ToString("yyyy-MM-dd"));

            var listdattime = list.Select(t => t.Backfield1).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
            }

            List<MesseriesData> listData = new List<MesseriesData>();
            var listLegendData = list.Select(t => t.ProcessName).GroupBy(t => t).ToList();
            List<string> LegendData = new List<string>();
            foreach (var item in listLegendData)
            {
                LegendData.Add(item.Key);
                List<int> Yday = new List<int>();
                foreach (var itemday in Xday)
                {
                    var listcount = list.Where(t => t.Backfield1 == itemday && t.ProcessName == item.Key).Count();
                    if (listcount > 0)
                    {
                        Yday.Add(listcount);
                    }
                }
                MesseriesData messeriesData = new MesseriesData();
                messeriesData.name = item.Key;
                messeriesData.data = Yday.ToArray();
                listData.Add(messeriesData);
            }
            mesChartModel.MesLegendData = LegendData.ToArray();
            mesChartModel.MesxAxisData = Xday.ToArray();
            mesChartModel.MesseriesDatas = listData;
            return Json(mesChartModel);
        }


        [HttpPost, Route("AccumulationChart"), AllowAnonymous]
        public async Task<IActionResult> AccumulationChart([FromBody] Vlh_ComponentProduct model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);
            MesAccumulationChartModel mesChartModel = new MesAccumulationChartModel();

            list.ToList().ForEach(i => i.Backfield1 = Convert.ToDateTime(i.OperatorDate).ToString("yyyy-MM-dd"));


            var listdattime = list.Select(t => t.Backfield1).GroupBy(t => t).ToList();
            List<string> Xday = new List<string>();
            foreach (var item in listdattime)
            {
                Xday.Add(item.Key);
            }

            List<MesFQCValue> listData = new List<MesFQCValue>();
            YAxisValue yAxisvalue = new YAxisValue();
            var listLegendData = list.Select(t => t.ProcessName).GroupBy(t => t).ToList();
            List<string> LegendData = new List<string>();
            foreach (var item in listLegendData)
            {
                LegendData.Add(item.Key);
                List<double> Yday = new List<double>();

                foreach (var itemday in Xday)
                {
                    var listcount = list.Where(t => t.Backfield1 == itemday && t.ProcessName == item.Key).ToList();
                    if (listcount.Count() > 0)
                    {
                        var listBadcount = listcount.Where(t => t.IsPass == false).Count();
                        Yday.Add(listBadcount);
                    }

                }
                MesFQCValue messeriesData = new MesFQCValue();
                messeriesData.name = item.Key;
                messeriesData.data = Yday.ToArray();
                listData.Add(messeriesData);
            }
            List<double> arrY = new List<double>();
            //直通率计算
            foreach (var itemday in Xday)
            {
                var listcount = list.Where(t => t.Backfield1 == itemday).ToList();
                if (listcount.Count > 0)
                {
                    var listmakecount = listcount.Where(t => t.IsPass == true).Count();
                    double arrvalue = Math.Round(Convert.ToDouble(listmakecount) / Convert.ToDouble(listcount.Count), 4);
                    arrY.Add(arrvalue * 100.0);

                }
            }
            yAxisvalue.data = arrY.ToArray();
            mesChartModel.ChartX = Xday.ToArray();
            mesChartModel.ChartBadName = LegendData.ToArray();
            mesChartModel.mesFQCValue = listData;
            mesChartModel.yAxisValue = yAxisvalue;
            return Json(mesChartModel);
        }

        [HttpPost, Route("MakeHoursChart"), AllowAnonymous]
        public async Task<IActionResult> MakeHoursChart()
        {
            Vlh_ComponentProduct model = new Vlh_ComponentProduct();
            model.Backfield1 = DateTime.Now.ToString("yyyy-MM-dd") + "," + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            //查询数据
            var list = await Service.SearchCharts(model);
            List<MesCompmakehour> mesChartModelList = new List<MesCompmakehour>();

            //     list.ToList().ForEach(i => i.Backfield1 = Convert.ToDateTime(i.OperatorDate).ToString("yyyy-MM-dd"));

            var stationList = NPOIReport.ReturnListCompStation();
            var timeList = NPOIReport.ReturnListCompTime();

            foreach (var itemstation in stationList)
            {
                MesCompmakehour mesCompmakehour = new MesCompmakehour();
                MeshourChart mshourChart = new MeshourChart();
                MeshourbarChart meshourbar = new MeshourbarChart();
                List<MeshourbarChart> meshourbarCharts=new List<MeshourbarChart>();
                mesCompmakehour.Title = itemstation;
                var stationSearchlist = list.Where(t => t.ProcessName == itemstation).ToList();
                List<string> listTimex = new List<string>();
                List<int> listTimey = new List<int>();
                for (int i = 0; i < timeList.Count - 1; i++)
                {
                  
                    listTimex.Add(timeList[i]);
                    if (i == (timeList.Count - 2))
                    {
                        listTimex.Add(timeList[i + 1]);
                    }
                    var datetime1 = Convert.ToDateTime(timeList[i]);
                    var datetime2 = Convert.ToDateTime(timeList[i + 1]);
                    var time = stationSearchlist.Where(t => t.OperatorDate >= datetime1 && t.OperatorDate < datetime2).ToList();
                    listTimey.Add(time.Count);
                }
                meshourbar.data = listTimey.ToArray();
                mshourChart.MesxAxisData = listTimex.ToArray();
                meshourbarCharts.Add(meshourbar);
                mshourChart.MesYAxisData = meshourbarCharts;
                mesCompmakehour.MshourChart = mshourChart;
                mesChartModelList.Add(mesCompmakehour);
            }

            return Json(mesChartModelList);
        }

        public partial class MesCompmakehour
        {
            public string Title { get; set; }
            public MeshourChart MshourChart { get; set; }

        }
        public partial class MeshourChart
        {
            public string[] MesxAxisData { get; set; }
            public List<MeshourbarChart> MesYAxisData { get; set; }
        }
        public partial class MeshourbarChart
        {
            public string type { get; set; } = "bar";

            public int[] data { get; set; }
        }

        public partial class MesChartModel
        {
            public string[] MesLegendData { get; set; }
            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; }
            public string type { get; set; } = "bar";
            public int barGap { get; set; } = 0;
            public emphasis Emphasis { get; set; } = new emphasis();
            public int[] data { get; set; }
        }
    }
    public partial class MesAccumulationChartModel
    {
        public string[] ChartX { get; set; }
        public YAxisValue yAxisValue { get; set; } = new YAxisValue();
        public List<MesFQCValue> mesFQCValue { get; set; }
        public string[] ChartBadName { get; set; }

    }
   

    
}

