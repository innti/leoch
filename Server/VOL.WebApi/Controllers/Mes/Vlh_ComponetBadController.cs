/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹Vlh_ComponetBadController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using Vol.Mes.IServices;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using VOL.Entity.DomainModels;
using System.Linq;
using System;

namespace Vol.Mes.Controllers
{
    [Route("api/Vlh_ComponetBad")]
    [PermissionTable(Name = "Vlh_ComponetBad")]
    public partial class Vlh_ComponetBadController : ApiBaseController<IVlh_ComponetBadService>
    {
        public Vlh_ComponetBadController(IVlh_ComponetBadService service)
        : base(service)
        {
        }
        [HttpPost, Route("ExcelChart"), AllowAnonymous]
        public async Task<IActionResult> ExcelChart([FromBody] Vlh_ComponetBad model)
        {
            //查询数据
            var list = await Service.SearchCharts(model);

            MesChartModel mesChartModel = new MesChartModel();

            List<string> Xday = new List<string>();

            var listType = list.Select(t => t.BadReason).GroupBy(t => t).ToList();
            foreach (var item in listType)
            {
                Xday.Add(item.Key);
            }
            List<string> yday1 = new List<string>();
            List<string> yday2 = new List<string>();
            List<MesseriesData> listmes=new List<MesseriesData>();
            if (list.Count > 0)
            {
                var listqty = list.Select(t => t.BadNum).Sum();
                foreach (var item in Xday)
                {
                    var listbadqty = list.Where(t => t.BadReason == item).ToList().Select(t => t.BadNum).Sum();

                    yday1.Add(listbadqty.ToString());
                    var dqty = Convert.ToDouble(listbadqty) / Convert.ToDouble(listqty) * 100.00;
                    yday2.Add(Math.Round(dqty, 3).ToString());
                }
                yday1.Sort((x, y) => -x.CompareTo(y));//降序
                MesseriesData messeriesData = new MesseriesData();
                messeriesData.data = yday1.ToArray();
                messeriesData.name = "不良原因";
                messeriesData.type ="bar";
                messeriesData.yAxisIndex = 0;
                listmes.Add(messeriesData);

                MesseriesData messeriesData1 = new MesseriesData();
                messeriesData1.data = yday2.ToArray();
                messeriesData1.name = "直通率";
                messeriesData1.type = "line";
                messeriesData1.yAxisIndex = 1;
                listmes.Add(messeriesData1);
            }

            mesChartModel.MesseriesDatas = listmes;
            mesChartModel.MesxAxisData = Xday.ToArray();

            return Json(mesChartModel);
        }
        public partial class MesChartModel
        {
            public string[] MesxAxisData { get; set; }
            public List<MesseriesData> MesseriesDatas { get; set; }

        }
        public partial class MesseriesData
        {
            public string name { get; set; }
            public string type { get; set; } = "line";
            public int yAxisIndex { get; set; } = 0;
            public string[] data { get; set; }

        }

    }
}

