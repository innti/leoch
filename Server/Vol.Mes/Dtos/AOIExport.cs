﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOL.Mes.Dtos
{
    /// <summary>
    /// 填充数据
    /// </summary>
    public class AOIRowsExport
    {
        public string Line { get; set; }
        public int Between { get; set; }
        public List<int> classes { get; set; }


        //列名称
        public class AOIColumns
        {
            public string Title { get; set; }
            public string LineColumnName { get; set; }
            public string BetweenColumnName { get; set; }
            public List<string> classesColumnName { get; set; }
        }



        public AOIColumns DefaultColumns(string title = null)
        {
            AOIColumns aOIColumns = new AOIColumns();
            aOIColumns.Title = title;
            aOIColumns.LineColumnName = "线别";
            aOIColumns.BetweenColumnName = "数据区间";
            var classList = Classes.GetClasseList();
            aOIColumns.classesColumnName = new List<string>();
            for (int i = 1; i <= 31; i++)
            {
                foreach (var item in classList)
                {
                    aOIColumns.classesColumnName.Add(i + item);
                }
            }
            return aOIColumns;
        }

    }








}
