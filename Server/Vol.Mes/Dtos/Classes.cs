﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOL.Mes.Dtos
{
    public enum ClassesEnum
    {
        A,// 白班
        B,// 夜班
    }

    public static class Classes
    {
        public static List<string> GetClasseList()
        {
            List<string> classes = new List<string>();
            foreach (ClassesEnum suit in Enum.GetValues(typeof(ClassesEnum)))
            {
                classes.Add(suit.ToString());
            }
            return classes;
        }
    }
}
