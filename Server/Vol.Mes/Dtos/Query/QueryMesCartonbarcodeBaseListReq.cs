﻿using VOL.Core.Request;

namespace Vol.Mes.Dtos.Query
{
    public class QueryMesCartonbarcodeBaseListReq : PageReq
    {
        //todo:添加自己的请求字段
        public string CodeBM { get; set; }

        public int? IsDel { get; set; } = 0;

        public string SearchType { get; set; }
        public string SearchValue { get; set; }
        /// <summary>
        /// 开始出库时间
        /// </summary>
        public string CreateOutDate { get; set; }
        /// <summary>
        /// 结束出库时间
        /// </summary>
        public string EndOutDate { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public string CreateDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndDate { get; set; }
    }
}