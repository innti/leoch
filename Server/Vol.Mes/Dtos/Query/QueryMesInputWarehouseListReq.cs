﻿using VOL.Core.Request;

namespace Vol.Mes.Dtos.Query
{
    public class QueryMesInputWarehouseListReq : PageReq
    {
        public string SearchType { get; set; }
        public string SearchValue { get; set; }
        /// <summary>
        /// 箱号
        /// </summary>
        public string CodeBM { get; set; }
        /// <summary>
        /// 盒号
        /// </summary>
        public string CodeCode { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public string CreateDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 开始出库时间
        /// </summary>
        public string CreateOutDate { get; set; }
        /// <summary>
        /// 结束出库时间
        /// </summary>
        public string EndOutDate { get; set; }
        /// <summary>
        /// 操作员
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 出库员
        /// </summary>
        public string OutUser { get; set; }

        /// <summary>
        /// 出库状态
        /// </summary>
        public int? WareStatus { get; set; } = 0;


        /// <summary>
        /// 出库状态
        /// </summary>
        public string  WareSort { get; set; } = "入库";
    }
}
