﻿using System.ComponentModel.DataAnnotations.Schema;

namespace VOL.Core.Request
{
    /// <summary>
	/// 测试Bin盒效率
	/// </summary>
    [Table("MesBinbase")]
    public partial class AddOrUpdateMesBinbaseReq 
    {

        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 配方名称
        /// </summary>
        public string Binrecipe { get; set; }
        /// <summary>
        /// Bin盒
        /// </summary>
        public string BinBox { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public string Bintype { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string BinSpec { get; set; }
        /// <summary>
        /// Class
        /// </summary>
        public string BinClass { get; set; }
        /// <summary>
        /// Pmax
        /// </summary>
        public string BinPmax { get; set; }
        /// <summary>
        /// 图形
        /// </summary>
        public string BinGrade { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public string BinRoom { get; set; }
        /// <summary>
        /// 材质
        /// </summary>
        public string BinTexture { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public string BinGraph { get; set; }
        /// <summary>
        /// 仓位
        /// </summary>
        public string BinPosition { get; set; }
        /// <summary>
        /// 线别
        /// </summary>
        public string BinLine { get; set; }
        /// <summary>
        /// 效率
        /// </summary>
        public string BinEff { get; set; }
        /// <summary>
        /// 厚度
        /// </summary>
        public string BinThickness { get; set; }
        /// <summary>
        /// Eta
        /// </summary>
        public string BinEta { get; set; }
        /// <summary>
        /// Eta最小值
        /// </summary>
        public string BinEtaMin { get; set; }
        /// <summary>
        /// Eta最大值
        /// </summary>
        public string BinEtaMax { get; set; }
        /// <summary>
        /// Isc
        /// </summary>
        public string BinIsc { get; set; }
        /// <summary>
        /// Isc最小值
        /// </summary>
        public string BinIscMin { get; set; }
        /// <summary>
        /// Isc最大值
        /// </summary>
        public string BinIscMax { get; set; }
        /// <summary>
        /// Voc
        /// </summary>
        public string BinVoc { get; set; }
        /// <summary>
        /// Voc最小值
        /// </summary>
        public string BinVocMin { get; set; }
        /// <summary>
        /// Voc最大值
        /// </summary>
        public string BinVocMax { get; set; }
        /// <summary>
        /// 删除标记
        /// </summary>
        public int? IsDel { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime CreateTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserName { get; set; }
        /// <summary>
        /// 创建人Id
        /// </summary>
        public string CreateUserId { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public System.DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UpdateUserName { get; set; }
        /// <summary>
        /// 修改人Id
        /// </summary>
        public string UpdateUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Backfield1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Backfield2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Backfield3 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Backfield4 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Backfield5 { get; set; }
        
         //todo:添加自己的请求字段
    }
}