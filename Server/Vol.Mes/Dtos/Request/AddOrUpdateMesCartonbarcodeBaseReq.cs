﻿using System.ComponentModel.DataAnnotations.Schema;

namespace VOL.Core.Request
{
    /// <summary>
	/// 测试外箱条码存储
	/// </summary>
    [Table("MesCartonbarcodeBase")]
    public partial class AddOrUpdateMesCartonbarcodeBaseReq 
    {

        /// <summary>
        /// 主键
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LineName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeBM { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeSpec { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CodeCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeClass { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodePmax { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeQty { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeNMB { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeGW { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeRoom { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Codetime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodePosition { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeTMSpec { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeNum { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime? CreateTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserName { get; set; }
        /// <summary>
        /// 创建人ID
        /// </summary>
        public string CreateUserId { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public System.DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdateUserName { get; set; }
        /// <summary>
        /// 更新人ID
        /// </summary>
        public string UpdateUserId { get; set; }
        /// <summary>
        /// 备用字段1
        /// </summary>
        public string Backfield1 { get; set; }
        /// <summary>
        /// 备用字段2
        /// </summary>
        public string Backfield2 { get; set; }
        /// <summary>
        /// 备用字段3
        /// </summary>
        public string Backfield3 { get; set; }
        /// <summary>
        /// 备用字段4
        /// </summary>
        public string Backfield4 { get; set; }
        /// <summary>
        /// 备用字段5
        /// </summary>
        public string Backfield5 { get; set; }
        
         //todo:添加自己的请求字段
    }
}