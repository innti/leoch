﻿using System.ComponentModel.DataAnnotations.Schema;

namespace VOL.Core.Request
{
    /// <summary>
	/// 测试外箱条码存储
	/// </summary>
    [Table("MesInputWarehouse")]
    public partial class AddOrUpdateMesInputWarehouseReq
    {

        /// <summary>
        /// 主键
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 出库状态
        /// </summary>
        public int? WareStatus { get; set; }
        /// <summary>
        /// 出库类型
        /// </summary>
        public string WareSort { get; set; }
        /// <summary>
        /// 外箱条码
        /// </summary>
        public string CodeBM { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public string CodeType { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string CodeSpec { get; set; }
        /// <summary>
        /// 内包装条码
        /// </summary>
        public string CodeCode { get; set; }
        /// <summary>
        /// Class
        /// </summary>
        public string CodeClass { get; set; }
        /// <summary>
        /// Pmax
        /// </summary>
        public string CodePmax { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public string CodeQty { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public string CodeNMB { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string CodeDate { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        public string CodeGW { get; set; }
        /// <summary>
        /// 入库仓库
        /// </summary>
        public string CodeRoom { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public string Codetime { get; set; }
        /// <summary>
        /// 仓位
        /// </summary>
        public string CodePosition { get; set; }
        /// <summary>
        /// 打印规则
        /// </summary>
        public string CodeTMSpec { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CodeNum { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public int? IsDel { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime? CreateTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUserName { get; set; }
        /// <summary>
        /// 创建人ID
        /// </summary>
        public string CreateUserId { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public System.DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdateUserName { get; set; }
        /// <summary>
        /// 更新人ID
        /// </summary>
        public string UpdateUserId { get; set; }
        /// <summary>
        /// 备用字段1
        /// </summary>
        public string Backfield1 { get; set; }
        /// <summary>
        /// 备用字段2
        /// </summary>
        public string Backfield2 { get; set; }
        /// <summary>
        /// 备用字段3
        /// </summary>
        public string Backfield3 { get; set; }
        /// <summary>
        /// 备用字段4
        /// </summary>
        public string Backfield4 { get; set; }
        /// <summary>
        /// 备用字段5
        /// </summary>
        public string Backfield5 { get; set; }


    }
    public class InputWarehouseTerans
    {
        public AddOrUpdateMesInputWarehouseReq addInputWarehouseReq { get; set; }

        /// <summary>
        /// BarcodeBaseIsdel
        /// </summary>
        public int BarcodeBaseIsdel { get; set; }

        /// <summary>
        /// BarcodeBase类型
        /// </summary>
        public string BarcodeBaseCodeType { get; set; }

        /// <summary>
        /// barcodeIsdel【中间没有Base】
        /// </summary>
        public int BarcodeIsdel { get; set; }
        /// <summary>
        /// 出库类型【箱/盒】
        /// </summary>
        public string CodeType { get; set; }
    }
}