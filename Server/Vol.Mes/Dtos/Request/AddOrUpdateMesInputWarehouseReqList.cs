﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace VOL.Core.Request
{
    public class AddOrUpdateMesInputWarehouseReqList
    {
        public List<AddOrUpdateMesInputWarehouseReq> MesInputWarehouseList { get; set; }

        public int Count { get; set; }
        public int BoxCount { get; set; }

        public List<string> BoxList { get; set; }
    }
}
