/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 */
using System.Collections.Generic;
using System.Threading.Tasks;
using VOL.Core.BaseProvider;
using VOL.Core.Request;
using VOL.Entity.DomainModels;


namespace Vol.Mes.IServices
{
    public partial interface IMesInputWarehouseService : IService<MesInputWarehouse>
    {
        void OutUpdate(AddOrUpdateMesInputWarehouseReq obj, string CodeType);
        Task<List<MesInputWarehouse>> SearchBoxNum(string code, string box);

        bool InsertTakeOverBoxNum(string code, string box);
    }
}
