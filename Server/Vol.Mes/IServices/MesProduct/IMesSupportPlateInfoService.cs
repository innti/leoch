/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 */
using System.Collections.Generic;
using System.Threading.Tasks;
using VOL.Core.BaseProvider;
using VOL.Entity.DomainModels;

namespace Vol.Mes.IServices
{
    public partial interface IMesSupportPlateInfoService : IService<MesSupportPlateInfo>
    {
        Task<List<MesSupportPlateInfo>> ExcelReport(MesSupportPlateInfo model);
        Task<List<MesSupportPlateInfo>> SearchCharts(MesSupportPlateInfo model);
    }
}
