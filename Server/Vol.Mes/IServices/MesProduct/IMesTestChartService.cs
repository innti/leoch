﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VOL.Core.BaseProvider;
using VOL.Entity.DomainModels;

namespace Vol.Mes.IServices.MesProduct
{
    public partial interface  IMesTestChartService : IService<MesTestResults>
    {
        Task<List<MesTestResults>> ExcelReport(MesTestResults model);
    }
}
