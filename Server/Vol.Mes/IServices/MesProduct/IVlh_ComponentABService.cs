/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 */
using System.Collections.Generic;
using System.Threading.Tasks;
using VOL.Core.BaseProvider;
using VOL.Entity.DomainModels;

namespace Vol.Mes.IServices
{
    public partial interface IVlh_ComponentABService : IService<Vlh_ComponentAB>
    {
        Task<List<Vlh_ComponentAB>> SearchCharts(Vlh_ComponentAB model);
    }
}
