/*
*所有关于MesCartonbarcode类的业务代码接口应在此处编写
*/
using VOL.Core.BaseProvider;
using VOL.Entity.DomainModels;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Vol.Mes.IServices
{
    public partial interface IMesCartonbarcodeService
    {
        void UpdateStatus(int isdel, string codebm);
    }
 }
