/*
*所有关于MesInputWarehouse类的业务代码接口应在此处编写
*/
using VOL.Core.BaseProvider;
using VOL.Entity.DomainModels;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using VOL.Core.Request;

namespace Vol.Mes.IServices
{
    public partial interface IMesInputWarehouseService
    {
        void AddCached(AddOrUpdateMesInputWarehouseReq req);
    }
 }
