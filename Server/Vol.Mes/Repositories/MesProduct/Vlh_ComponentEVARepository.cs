/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *Repository提供数据库操作，如果要增加数据库操作请在当前目录下Partial文件夹Vlh_ComponentEVARepository编写代码
 */
using Vol.Mes.IRepositories;
using VOL.Core.BaseProvider;
using VOL.Core.EFDbContext;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Repositories
{
    public partial class Vlh_ComponentEVARepository : RepositoryBase<Vlh_ComponentEVA> , IVlh_ComponentEVARepository
    {
    public Vlh_ComponentEVARepository(VOLContext dbContext)
    : base(dbContext)
    {

    }
    public static IVlh_ComponentEVARepository Instance
    {
      get {  return AutofacContainerModule.GetService<IVlh_ComponentEVARepository>(); } }
    }
}
