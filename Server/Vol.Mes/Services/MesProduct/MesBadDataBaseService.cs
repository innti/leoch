/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesBadDataBaseService与IMesBadDataBaseService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesBadDataBaseService : ServiceBase<MesBadDataBase, IMesBadDataBaseRepository>
    , IMesBadDataBaseService, IDependency
    {
    public MesBadDataBaseService(IMesBadDataBaseRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IMesBadDataBaseService Instance
    {
      get { return AutofacContainerModule.GetService<IMesBadDataBaseService>(); } }
        public Task<List<MesBadDataBase>> SearchCharts(MesBadDataBase model)
        {
            var list = repository.FindQuery(e => true);

            if (!string.IsNullOrEmpty(model.Line))
            {
                list = list.Where(t => t.Line == model.Line);
            }
            if (!string.IsNullOrEmpty(model.Station))
            {
                list = list.Where(t => t.Station == model.Station);
            }

            if (!string.IsNullOrEmpty(model.BadSort))
            {
                list = list.Where(t => t.BadSort == model.BadSort);
            }

            var date = model.billno.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);
            list = list.Where(t => t.CreateTime >= startdate);
            list = list.Where(t => t.CreateTime <= enddate);
            var result = list.OrderBy(t => t.CreateTime).ToList();

            return Task.FromResult(result);

        }
    }
 }
