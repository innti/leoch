/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesChemicalBaseService与IMesChemicalBaseService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesChemicalBaseService : ServiceBase<MesChemicalBase, IMesChemicalBaseRepository>
    , IMesChemicalBaseService, IDependency
    {
        public MesChemicalBaseService(IMesChemicalBaseRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesChemicalBaseService Instance
        {
            get { return AutofacContainerModule.GetService<IMesChemicalBaseService>(); }
        }

        public Task<List<MesChemicalBase>> ExcelChart(string line, string dates)
        {
            var list = repository.FindQuery(e => true);
            if (!string.IsNullOrWhiteSpace(line))
            {
                list = list.Where(t => t.LineName == line);
            }

            //if (!string.IsNullOrWhiteSpace(model.ShiftClass))
            //{
            //    list = list.Where(t => t.ShiftClass == model.ShiftClass);
            //}
            //  list = list.Where(t => t.Station =="FQC");
            var date = dates.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);


            //           list = list.Where(d => String.Compare(d.Date, date[0]) >= 0
            //&& String.Compare(d.Date, date[1]) <= 0);
            list = list.Where(t => t.CreateTime >= startdate);
            list = list.Where(t => t.CreateTime <= enddate);
            var result = list.OrderBy(t => t.CreateTime).ToList();

            return Task.FromResult(result);
        }
    }
}
