/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesFewchildrenService与IMesFewchildrenService中编写
 */
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesFewchildrenService : ServiceBase<MesFewchildren, IMesFewchildrenRepository>
    , IMesFewchildrenService, IDependency
    {
    public MesFewchildrenService(IMesFewchildrenRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IMesFewchildrenService Instance
    {
      get { return AutofacContainerModule.GetService<IMesFewchildrenService>(); } }
    }
 }
