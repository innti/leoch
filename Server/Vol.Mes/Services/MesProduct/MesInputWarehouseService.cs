/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesInputWarehouseService与IMesInputWarehouseService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Core.ManageUser;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesInputWarehouseService : ServiceBase<MesInputWarehouse, IMesInputWarehouseRepository>
    , IMesInputWarehouseService, IDependency
    {
        public MesInputWarehouseService(IMesInputWarehouseRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesInputWarehouseService Instance
        {
            get { return AutofacContainerModule.GetService<IMesInputWarehouseService>(); }
        }
        public Task<List<MesInputWarehouse>> SearchBoxNum(string code, string box)
        {

            var list = repository.FindQuery(e => true);

            if (box.Contains("箱"))
            {
                list = list.Where(t => t.CodeBM == code);
            }
            else
            {
                list = list.Where(t => t.CodeCode == code);
            }

            list = list.Where(t => t.WareSort == "出库");
            var result = list.ToList();


            return Task.FromResult(result);


        }
        public  bool InsertTakeOverBoxNum(string code, string box)
        {
            string formattableString = $"";
            if (box.Contains("箱"))
            {
                formattableString = $"  insert  into [MesWareTakeOverbase]([CodeBM],[CodeCode] ,[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot] ,[Spec],[CodeClass],[Qty],[Unit] ,[Type]    ,[CreateUserName])SELECT[CodeBM],[CodeCode], [ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[CodeSpec],[CodeClass] , replace([CodeQty],'PCS',''),[Unit] ,CodeType, '{UserContext.Current.UserTrueName}'  FROM[GSMESserver].[dbo].[MesInputWarehouse] as b  left join MesWareProductbase as a on a.Class = b.[CodeClass] and b.CodeType = a.Type and a.Spec = b.CodeSpec  where WareSort = '出库' and CodeBM = '{code}'";

            }
            else
            {
                formattableString = $"  insert  into [MesWareTakeOverbase]([CodeBM],[CodeCode] ,[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot] ,[Spec],[Class],[Qty],[Unit] ,[Type]    ,[CreateUserName])SELECT[CodeBM],[CodeCode], [ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[CodeSpec],[CodeClass] , replace([CodeQty],'PCS',''),[Unit] ,CodeType, '{UserContext.Current.UserTrueName}'  FROM[GSMESserver].[dbo].[MesInputWarehouse] as b  left join MesWareProductbase as a on a.Class = b.[CodeClass] and b.CodeType = a.Type and a.Spec = b.CodeSpec  where WareSort = '出库' and CodeCode = '{code}'";
            }

            var num = repository.ExecuteSqlCommand(formattableString);
            if (num>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
