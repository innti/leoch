/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesLineFQCBaseService与IMesLineFQCBaseService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesLineFQCBaseService : ServiceBase<MesLineFQCBase, IMesLineFQCBaseRepository>
    , IMesLineFQCBaseService, IDependency
    {
        public MesLineFQCBaseService(IMesLineFQCBaseRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesLineFQCBaseService Instance
        {
            get { return AutofacContainerModule.GetService<IMesLineFQCBaseService>(); }
        }
        public Task<List<MesLineFQCBase>> ExcelChart(MesLineFQCBase model)
        {
            var list = repository.FindQuery(e => true);
            if (!string.IsNullOrWhiteSpace(model.Station))
            {
                list = list.Where(t => t.Station == model.Station);
            }
            if (!string.IsNullOrWhiteSpace(model.Line))
            {
                list = list.Where(t => t.Line == model.Line);
            }
            //if (!string.IsNullOrWhiteSpace(model.ShiftClass))
            //{
            //    list = list.Where(t => t.ShiftClass == model.ShiftClass);
            //}
          //  list = list.Where(t => t.Station =="FQC");
            var date = model.Backfield1.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);
            list = list.Where(t => t.SDate >= startdate);
            list = list.Where(t => t.SDate <= enddate);
            var result = list.OrderBy(t => t.Billno).ToList();

            return Task.FromResult(result);
        }
    }
}
