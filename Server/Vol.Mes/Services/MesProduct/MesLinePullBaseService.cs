/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesLinePullBaseService与IMesLinePullBaseService中编写
 */
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesLinePullBaseService : ServiceBase<MesLinePullBase, IMesLinePullBaseRepository>
    , IMesLinePullBaseService, IDependency
    {
    public MesLinePullBaseService(IMesLinePullBaseRepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IMesLinePullBaseService Instance
    {
      get { return AutofacContainerModule.GetService<IMesLinePullBaseService>(); } }
    }
 }
