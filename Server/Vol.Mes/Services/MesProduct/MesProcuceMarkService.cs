/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesProcuceMarkService与IMesProcuceMarkService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesProcuceMarkService : ServiceBase<MesProcuceMark, IMesProcuceMarkRepository>
    , IMesProcuceMarkService, IDependency
    {
        public MesProcuceMarkService(IMesProcuceMarkRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesProcuceMarkService Instance
        {
            get { return AutofacContainerModule.GetService<IMesProcuceMarkService>(); }
        }
        public Task<List<MesProcuceMark>> SearchCharts(MesProcuceMark model)
        {
            var list = repository.FindQuery(e => true);
            var date = model.Backfield1.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);
            list = list.Where(t => t.CreateTime >= startdate);
            list = list.Where(t => t.CreateTime <= enddate);
            var result = list.OrderBy(t => t.CreateTime).ToList();

            return Task.FromResult(result);

        }
    }
}
