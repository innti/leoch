﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using Vol.Mes.IServices.MesProduct;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services.MesProduct
{
    internal class MesTestChartService : ServiceBase<MesTestResults, IMesTestResultsRepository>
    , IMesTestChartService, IDependency
    {
        public MesTestChartService(IMesTestResultsRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesTestChartService Instance
        {
            get { return AutofacContainerModule.GetService<IMesTestChartService>(); }
        }
        public Task<List<MesTestResults>> ExcelReport(MesTestResults model)
        {

            var list = repository.FindQuery(e => true);
            //if (!string.IsNullOrWhiteSpace(model.Station))
            //{
              //  list = list.Where(t => t.Station == model.Station);
            //}
            //if (!string.IsNullOrWhiteSpace(model.Sort))
            //{
            //    list = list.Where(t => t.Sort == model.Sort);
            //}
            //if (!string.IsNullOrWhiteSpace(model.Line))
            //{
            //    list = list.Where(t => t.Line == model.Line);
            //}
            //if (!string.IsNullOrWhiteSpace(model.ShiftClass))
            //{
            //    list = list.Where(t => t.ShiftClass == model.ShiftClass);
            //}
            //if (!string.IsNullOrWhiteSpace(model.Type))
            //{
            //    list = list.Where(t => t.Type == model.Type);
            //}
            //var date = model.Backfield1.Split(',');
            //var startdate = Convert.ToDateTime(date[0].Replace("\"", ""));
            //var enddate = Convert.ToDateTime(date[1].Replace("\"", ""));
            //list = list.Where(t => t.SDate >= startdate);
            //list = list.Where(t => t.SDate <= enddate);
            var result = list.ToList();


            return Task.FromResult(result);
        }
    }
}
