/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesTestResultsService与IMesTestResultsService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesTestResultsService : ServiceBase<MesTestResults, IMesTestResultsRepository>
    , IMesTestResultsService, IDependency
    {
        public MesTestResultsService(IMesTestResultsRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesTestResultsService Instance
        {
            get { return AutofacContainerModule.GetService<IMesTestResultsService>(); }
        }
        public Task<List<MesTestResults>> SearchCharts(MesTestResults model)
        {
            var list = repository.FindQuery(e => true);

            if (!string.IsNullOrEmpty(model.Line))
            {
                list = list.Where(t => t.Line == model.Line);
            }
            if (!string.IsNullOrEmpty(model.ShiftClass))
            {
                list = list.Where(t => t.ShiftClass == model.ShiftClass);
            }
            list = list.Where(t => t.Sort == "汇总");

            var date = model.Backfield1.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);
            list = list.Where(t => t.Date >= startdate);
            list = list.Where(t => t.Date <= enddate);
            var result = list.OrderBy(t => t.Date).ToList();

            return Task.FromResult(result);

        }
    }
}
