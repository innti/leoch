/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesWareTakeOverbaseService与IMesWareTakeOverbaseService中编写
 */
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesWareTakeOverbaseService : ServiceBase<MesWareTakeOverbase, IMesWareTakeOverbaseRepository>
    , IMesWareTakeOverbaseService, IDependency
    {
        public MesWareTakeOverbaseService(IMesWareTakeOverbaseRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesWareTakeOverbaseService Instance
        {
            get { return AutofacContainerModule.GetService<IMesWareTakeOverbaseService>(); }

        }
        public Task<List<MesWareTakeOverbase>> SearchBoxNum(string code, string box)
        {

            var list = repository.FindQuery(e => true);

            if (box.Contains("箱"))
            {
                list = list.Where(t => t.CodeBM == code);
            }
            else
            {
                list = list.Where(t => t.CodeCode == code);
            }

            list = list.Where(t => t.IsDel == 0);
            var result = list.ToList();


            return Task.FromResult(result);


        }


    }
}
