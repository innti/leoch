/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesWareWorkbaseService与IMesWareWorkbaseService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Dapper;
using VOL.Core.DBManager;
using VOL.Core.Extensions.AutofacManager;
using VOL.Core.ManageUser;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesWareWorkbaseService : ServiceBase<MesWareWorkbase, IMesWareWorkbaseRepository>
    , IMesWareWorkbaseService, IDependency
    {
        public MesWareWorkbaseService(IMesWareWorkbaseRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesWareWorkbaseService Instance
        {
            get { return AutofacContainerModule.GetService<IMesWareWorkbaseService>(); }

        }
        public Task<List<MesWareWorkbase>> SearchWorkerNo(string code, string sort)
        {

            var list = repository.FindQuery(e => true);

            list = list.Where(t => t.Sort == sort);
            list = list.Where(t => t.WorkNo == code);

            var result = list.ToList();


            return Task.FromResult(result);


        }
        public bool InsertWareHouse(MesWareInputDetils model)
        {

            string formattableString = $"";
            if (model.Sort.Contains("箱"))
            {
                formattableString = $" insert into [MesWareInputDetils](  [Billno],[Sort] ,[WorkerNO],[SortType],[WareHouse],[WarePostion],[CodeBM],[Codecode],[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot] ,[Qty],[Unit],[Type],[Spec],[CodeClass],[WarePeople],[Effect],[Remake],[Status],[CreateTime],[CreateUserName])SELECT    '{model.Billno}' ,'{model.Sort}','{model.WorkerNO}',  '入库','{model.WareHouse}','{model.WarePostion}',[CodeBM],[CodeCode],[ProductNo] ,[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],[Unit],[Type] ,[Spec],[CodeClass] ,'{model.WarePeople}','{model.Effect}','{model.Remake}','0',GETDATE(),'{UserContext.Current.UserTrueName}' FROM [MesWareTakeOverbase] where CodeBM='{model.CodeBM}' and IsDel=0 ";

            }
            else
            {
                formattableString = $" insert into [MesWareInputDetils](  [Billno],[Sort] ,[WorkerNO],[SortType],[WareHouse],[WarePostion],[CodeBM],[Codecode],[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot] ,[Qty],[Unit],[Type],[Spec],[CodeClass],[WarePeople],[Effect],[Remake],[Status],[CreateTime],[CreateUserName])SELECT    '{model.Billno}' ,'{model.Sort}','{model.WorkerNO}',  '入库','{model.WareHouse}','{model.WarePostion}',[CodeBM],[CodeCode],[ProductNo] ,[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],[Unit],[Type] ,[Spec],[CodeClass] ,'{model.WarePeople}','{model.Effect}','{model.Remake}','0',GETDATE(),'{UserContext.Current.UserTrueName}' FROM [MesWareTakeOverbase] where CodeBM='{model.Codecode}' and IsDel=0 ";
            }
            bool flag = false;
            DBServerProvider.SqlDapper.BeginTransaction((ISqlDapper dapper) =>
                {
                    // string sql = "UPDATE App_Expert SET CreateDate=GETDATE()";
                    //return false;回滚事务
                    if (dapper.ExcuteNonQuery(formattableString, null) == 0)
                    {
                        return false;
                    }
                   

                    var UpdateWorkerSql = $"  update [GSMESserver].[dbo].[MesWareWorkbase] set PlaneQty1='{model.Qty}',PlaneQty2=PlaneQty,PlaneQty=PlaneQty-{model.Qty} where WorkNo='{model.WorkerNO}' and ProductLot='{model.ProductLot}' and ProductNo='{model.ProductNo}'";
                    if (dapper.ExcuteNonQuery(UpdateWorkerSql, null) == 0)
                    {
                        return false;
                    }
                    string InserWareSql = "";
                    var UpdateTakeOver = "";
                    if (model.Sort.Contains("箱"))
                    {
                        UpdateTakeOver = $"update MesWareTakeOverbase set isdel=1 where CodeBM='{model.Codecode}'";
                        InserWareSql = $" insert into [MesWareInputStock]([WareHouse] ,[WarePostion] ,[CodeBM],[Codecode],[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],[Status],[Type],[Spec],[CodeClass]) SELECT                          '{model.WareHouse}','{model.WarePostion}',[CodeBM],[CodeCode],[ProductNo] ,[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],'0',[Type],[Spec],[CodeClass]   FROM [MesWareTakeOverbase]    where CodeBM='{model.Codecode}'";
                    }
                    else
                    {
                        InserWareSql = $" insert into [MesWareInputStock]([WareHouse] ,[WarePostion] ,[CodeBM],[Codecode],[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],[Status],[Type],[Spec],[CodeClass]) SELECT                          '{model.WareHouse}','{model.WarePostion}',[CodeBM],[CodeCode],[ProductNo] ,[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],'0',[Type],[Spec],[CodeClass]   FROM [MesWareTakeOverbase]    where CodeCode='{model.Codecode}'";
                        UpdateTakeOver = $"update MesWareTakeOverbase set isdel=1 where CodeCode='{model.Codecode}' ";
                    }
                    if (dapper.ExcuteNonQuery(UpdateTakeOver, null) == 0)
                    {
                        return false;
                    }
                    if (dapper.ExcuteNonQuery(InserWareSql, null) == 0)
                    {
                        return false;
                    }
                    flag = true;
                    //返回true才会提交事务
                    return true;
                }, (Exception ex) =>
                {
                    //上面不需要处理异常，请在此处处理异常
                    //DBServerProvider.SqlDapper.xxx
                    Console.WriteLine(ex.Message);
                });

            // var num = repository.ExecuteSqlCommand(formattableString);
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public bool OutWareWareHouse(MesWareInputDetils model)
        {

            string formattableString = $"";
            if (model.Sort.Contains("箱"))
            {
                formattableString = $" insert into [MesWareInputDetils](  [Billno],[Sort] ,[WorkerNO],[SortType],[WareHouse],[WarePostion],[CodeBM],[Codecode],[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot] ,[Qty],[Unit],[Type],[Spec],[CodeClass],[WarePeople],[Effect],[Remake],[Status],[CreateTime],[CreateUserName])SELECT    '{model.Billno}' ,'{model.Sort}','{model.WorkerNO}',  '出库','{model.WareHouse}',WarePostion,[CodeBM],[CodeCode],[ProductNo] ,[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],'片',[Type] ,[Spec],[CodeClass] ,'{model.WarePeople}','{model.Effect}','{model.Remake}','0',GETDATE(),'{UserContext.Current.UserTrueName}' FROM [MesWareInputStock] where CodeBM='{model.CodeBM}' and IsDel=0 and WareHouse='{model.WareHouse}'";

            }
            else
            {
                formattableString = $" insert into [MesWareInputDetils](  [Billno],[Sort] ,[WorkerNO],[SortType],[WareHouse],[WarePostion],[CodeBM],[Codecode],[ProductNo],[ProductName],[ProductUnit],[ProductSpec],[ProductLot] ,[Qty],[Unit],[Type],[Spec],[CodeClass],[WarePeople],[Effect],[Remake],[Status],[CreateTime],[CreateUserName])SELECT    '{model.Billno}' ,'{model.Sort}','{model.WorkerNO}',  '出库','{model.WareHouse}',WarePostion,[CodeBM],[CodeCode],[ProductNo] ,[ProductName],[ProductUnit],[ProductSpec],[ProductLot],[Qty],'片',[Type] ,[Spec],[CodeClass] ,'{model.WarePeople}','{model.Effect}','{model.Remake}','0',GETDATE(),'{UserContext.Current.UserTrueName}' FROM [MesWareInputStock] where CodeBM='{model.Codecode}' and IsDel=0 and WareHouse='{model.WareHouse}' ";
            }
            bool flag = false;
            DBServerProvider.SqlDapper.BeginTransaction((ISqlDapper dapper) =>
            {
                // string sql = "UPDATE App_Expert SET CreateDate=GETDATE()";
                //return false;回滚事务
                if (dapper.ExcuteNonQuery(formattableString, null) == 0)
                {
                    return false;
                }


                var UpdateWorkerSql = $"  update [MesWareWorkbase] set PlaneQty1='{model.Qty}',PlaneQty2=PlaneQty,PlaneQty=PlaneQty-{model.Qty} where WorkNo='{model.WorkerNO}' and ProductLot='{model.ProductLot}' and ProductNo='{model.ProductNo}'";

                if (dapper.ExcuteNonQuery(UpdateWorkerSql, null) == 0)
                {
                    return false;
                }
                var UpdateTakeOver = "";
                if (model.Sort.Contains("箱"))
                {
                    UpdateTakeOver = $"update MesWareInputStock set status=1,qty = 0,qty2 = qty  where CodeBM='{model.Codecode}'";

                }
                else
                {
                    UpdateTakeOver = $"update MesWareInputStock set status=1,qty = 0,qty2 = qty  where Codecode='{model.Codecode}'";
                }
                if (dapper.ExcuteNonQuery(UpdateTakeOver, null) == 0)
                {
                    return false;
                }
               
                flag = true;
                //返回true才会提交事务
                return true;
            }, (Exception ex) =>
            {
                //上面不需要处理异常，请在此处处理异常
                //DBServerProvider.SqlDapper.xxx
                Console.WriteLine(ex.Message);
            });

            // var num = repository.ExecuteSqlCommand(formattableString);
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
    }
}
