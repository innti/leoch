/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下MesdeviceMaintainanceService与IMesdeviceMaintainanceService中编写
 */
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class MesdeviceMaintainanceService : ServiceBase<MesdeviceMaintainance, IMesdeviceMaintainanceRepository>
    , IMesdeviceMaintainanceService, IDependency
    {
        public MesdeviceMaintainanceService(IMesdeviceMaintainanceRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IMesdeviceMaintainanceService Instance
        {
            get { return AutofacContainerModule.GetService<IMesdeviceMaintainanceService>(); }
        }

        public Task<List<MesdeviceMaintainance>> ExcelChart()
        {
            var list = repository.FindQuery(e => true);

            var result = list.OrderByDescending(t => t.CreateTime).Take(10).ToList();

            return Task.FromResult(result);
        }

    }
}
