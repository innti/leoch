/*
 *所有关于MesInputWarehouse类的业务代码应在此处编写
*可使用repository.调用常用方法，获取EF/Dapper等信息
*如果需要事务请使用repository.DbContextBeginTransaction
*也可使用DBServerProvider.手动获取数据库相关信息
*用户信息、权限、角色等使用UserContext.Current操作
*MesInputWarehouseService对增、删、改查、导入、导出、审核业务代码扩展参照ServiceFunFilter
*/
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;
using System.Linq;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using VOL.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Vol.Mes.IRepositories;
using VOL.Core.Request;
using VOL.Core.ManageUser;
using System;
using VOL.Core;
using Vol.Mes.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Vol.Mes.Services
{
    public partial class MesInputWarehouseService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMesInputWarehouseRepository _repository;//访问数据库

        [ActivatorUtilitiesConstructor]
        public MesInputWarehouseService(
            IMesInputWarehouseRepository dbRepository,
            IHttpContextAccessor httpContextAccessor
            )
        : base(dbRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _repository = dbRepository;
            //多租户会用到这init代码，其他情况可以不用
            //base.Init(dbRepository);
        }
        /// <summary>
        /// 出库修改数据状态
        /// </summary>
        /// <param name="obj">List</param>
        /// <param name="CodeType">箱/盒</param>
        public void OutUpdate(AddOrUpdateMesInputWarehouseReq obj, string CodeType)
        {
            var user = UserContext.Current; 
            if (CodeType == "箱")
            {
                var data= repository.FindAsIQueryable(u => u.CodeBM == obj.CodeBM).ToListAsync();
                if (data != null)
                {
                    foreach (var item in data.Result)
                    {
                        item.WareStatus = obj.WareStatus;
                        //item.WareSort = obj.WareSort;
                        item.UpdateTime = DateTime.Now;
                        item.UpdateUserId = user.UserId.ToString();
                        item.UpdateUserName = user.UserTrueName;
                        repository.Update(item, true);
                    }
                  
                }
            }
            else
            {
                var data = repository.FindAsIQueryable(u => u.CodeCode == obj.CodeCode).FirstOrDefault();
                if (data != null)
                {
                    data.WareStatus = obj.WareStatus;
                    //data.WareSort = obj.WareSort;
                    data.UpdateTime = DateTime.Now;
                    data.UpdateUserId = user.UserId.ToString();
                    data.UpdateUserName = user.UserTrueName;
                    repository.Update(data, true);
                }
            }
        }
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="req"></param>

        public void AddCached(AddOrUpdateMesInputWarehouseReq req)
        {
            var obj = req.MapTo<MesInputWarehouse>();
             obj.CreateTime = DateTime.Now;
            var user = UserContext.Current;
            obj.CreateUserId = user.UserId.ToString();
            obj.CreateUserName = user.UserTrueName;
            MesInputWarehouseRepository.Instance.Add(obj, true);
        }
    }
}
