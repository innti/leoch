/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下Vlh_ComponentABService与IVlh_ComponentABService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class Vlh_ComponentABService : ServiceBase<Vlh_ComponentAB, IVlh_ComponentABRepository>
    , IVlh_ComponentABService, IDependency
    {
        public Vlh_ComponentABService(IVlh_ComponentABRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IVlh_ComponentABService Instance
        {
            get { return AutofacContainerModule.GetService<IVlh_ComponentABService>(); }
        }
        public Task<List<Vlh_ComponentAB>> SearchCharts(Vlh_ComponentAB model)
        {
            var list = repository.FindQuery(e => true);

            if (!string.IsNullOrEmpty(model.Name))
            {
                list = list.Where(t => t.Name == model.Name);
            }
            if (!string.IsNullOrEmpty(model.EquipmentName))
            {
                list = list.Where(t => t.EquipmentName == model.EquipmentName);
            }
            //  list = list.Where(t => t.Sort == "汇总");

            var date = model.ProcessCode.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);
            list = list.Where(t => t.CreationTime >= startdate);
            list = list.Where(t => t.CreationTime <= enddate);
            var result = list.OrderBy(t => t.CreationTime).ToList();

            return Task.FromResult(result);

        }
    }
}
