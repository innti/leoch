/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下Vlh_ComponentEVAService与IVlh_ComponentEVAService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class Vlh_ComponentEVAService : ServiceBase<Vlh_ComponentEVA, IVlh_ComponentEVARepository>
    , IVlh_ComponentEVAService, IDependency
    {
    public Vlh_ComponentEVAService(IVlh_ComponentEVARepository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static IVlh_ComponentEVAService Instance
    {
      get { return AutofacContainerModule.GetService<IVlh_ComponentEVAService>(); }
        }
        public Task<List<Vlh_ComponentEVA>> SearchCharts(Vlh_ComponentEVA model)
        {
            var list = repository.FindQuery(e => true);

            if (!string.IsNullOrEmpty(model.Name))
            {
                list = list.Where(t => t.Name == model.Name);
            }
            if (!string.IsNullOrEmpty(model.EquipmentName))
            {
                list = list.Where(t => t.EquipmentName == model.EquipmentName);
            }
            //  list = list.Where(t => t.Sort == "汇总");

            var date = model.ProcessCode.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);
            list = list.Where(t => t.CreationTime >= startdate);
            list = list.Where(t => t.CreationTime <= enddate);
            var result = list.OrderBy(t => t.CreationTime).ToList();

            return Task.FromResult(result);

        }
    }
 }
