/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下Vlh_ComponentProductService与IVlh_ComponentProductService中编写
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vol.Mes.IRepositories;
using Vol.Mes.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace Vol.Mes.Services
{
    public partial class Vlh_ComponentProductService : ServiceBase<Vlh_ComponentProduct, IVlh_ComponentProductRepository>
    , IVlh_ComponentProductService, IDependency
    {
        public Vlh_ComponentProductService(IVlh_ComponentProductRepository repository)
        : base(repository)
        {
            Init(repository);
        }
        public static IVlh_ComponentProductService Instance
        {
            get { return AutofacContainerModule.GetService<IVlh_ComponentProductService>(); }

        }
        public Task<List<Vlh_ComponentProduct>> SearchCharts(Vlh_ComponentProduct model)
        {
            var list = repository.FindQuery(e => true);


            var date = model.Backfield1.Split(',');
            var startdate = Convert.ToDateTime(date[0]);
            var enddate = Convert.ToDateTime(date[1]);
            list = list.Where(t => t.OperatorDate >= startdate);
            list = list.Where(t => t.OperatorDate <= enddate);
            var result = list.OrderBy(t => t.OperatorDate).ToList();

            return Task.FromResult(result);

        }
    }
}
